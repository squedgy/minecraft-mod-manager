module com.squedgy.mc.mod.manager.configuration {
    exports com.squedgy.mc.mod.manager.configuration.http;
    exports com.squedgy.mc.mod.manager.configuration;
    exports com.squedgy.mc.mod.manager.configuration.utils;

    requires org.slf4j;
    requires transitive com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.annotation;
    requires com.squedgy.mc.mod.manager.utils;
}