package com.squedgy.mc.mod.manager.configuration.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.utils.io.IO;

import java.io.IOException;
import java.util.List;

public class DownloadUriConfiguration {

    public static List<Modification> getUriReplacements() throws IOException {
        var config = ConfigPaths.getConfigPath();
        config = config.resolve("http")
                       .resolve("url-mods.json");
        var tr = new TypeReference<List<Modification>>() {};
        if(config.toFile()
                 .exists() &&
           config.toFile()
                 .isFile()) {
            return IO.objectMapper()
                     .readValue(config.toFile(), tr);
        }

        return IO.objectMapper()
                 .readValue(DownloadUriConfiguration.class.getResourceAsStream("url-mods.json"), tr);
    }

}
