package com.squedgy.mc.mod.manager.configuration;

import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class ClientToken {
    private static final Logger LOG = getLogger(ClientToken.class);
    private static String uuid;

    private static Path getTokenFile() {
        return ConfigPaths.getConfigPath()
                          .resolve("token.txt");
    }

    private static void createTokenFile() {
        var token = UUID.randomUUID();
        var tokenFile = getTokenFile();
        // if the file doesn't exist try creating it
        if(!tokenFile.toFile()
                     .exists()) {
            tokenFile.getParent()
                     .toFile()
                     .mkdirs();
            try {
                Files.write(tokenFile,
                            token.toString()
                                 .getBytes()
                );
                return;
            } catch(IOException e) {
                LOG.warn("Failed to make token file", e);
            }
        }

        // create a temp uuid
        if(uuid == null) uuid = token.toString();
    }

    public static String getClientToken() {
        LOG.info("retrieving client token");
        if(uuid == null) {
            LOG.info("this is the first time this session, attempting to retrieve");
            try {
                uuid = Files.readString(getTokenFile())
                            .replaceAll("[^A-Za-z0-9]*([A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}).*",
                                        "$1"
                            );
                LOG.info("Retrieved uuid: {}", uuid);
            } catch(IOException e) {
                LOG.warn("Failed to read existing client token", e);
                if(uuid == null) { // if we don't have a temp uuid try to create the file
                    createTokenFile();
                }
                if(uuid == null) return getClientToken();
            }
        }
        return uuid;
    }

}
