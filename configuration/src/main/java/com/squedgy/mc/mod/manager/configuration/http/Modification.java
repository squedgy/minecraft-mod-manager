package com.squedgy.mc.mod.manager.configuration.http;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.List;

public class Modification {
    public final List<Replacement> replacements;
    public final String add;

    @JsonCreator
    public Modification(List<Replacement> replacements, String add) {
        this.replacements = replacements;
        this.add = add;
    }

    public String getModifiedDownloadUri(String downloadUri) {
        String uri = downloadUri;
        if(replacements != null) {
            for(var r : replacements) {
                uri = uri.replace(r.replace, r.with);
            }
        }
        if(add != null) {
            uri += add;
        }

        return uri;
    }
}
