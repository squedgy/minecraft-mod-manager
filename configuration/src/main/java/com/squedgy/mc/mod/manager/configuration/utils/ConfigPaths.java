package com.squedgy.mc.mod.manager.configuration.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

public abstract class ConfigPaths {

    public static Path getConfigPath() {
        return getManagerDir().resolve("config");
    }

    public static Path getManagerDir() {
        return Paths.get(System.getProperty("user.home"), ".mc-mod-man")
                    .toAbsolutePath();
    }

    public static Path getVersionJsonFilePath(String minecraftVersion) {
        return getVersionJsonPath().resolve(minecraftVersion + ".json");
    }

    public static Path getVersionJsonPath() {
        return getManagerDir().resolve("versions");
    }

    public static Path getNativesDirectory(String instanceName) {
        return getInstanceDirectory(instanceName).resolve("natives");
    }

    public static Path getInstanceDirectory(String instanceName) {
        return getInstancesDirectory().resolve(instanceName);
    }

    public static Path getInstancesDirectory() {
        return getManagerDir().resolve("instances");
    }

    public static Path getInstanceConfigFile(String instanceName) {
        return getInstanceDirectory(instanceName).resolve("instance.json");
    }

    public static Path getAssetIndex(String assetId) {
        return getAssetDirectory().resolve("indexes")
                                  .resolve(assetId + ".json");
    }

    public static Path getAssetDirectory() {
        return getManagerDir().resolve("assets");
    }

    public static Path getModernAssetPath() {
        return getAssetDirectory().resolve("objects");
    }

    public static Path getLibraryPath() {
        return getManagerDir().resolve("libraries");
    }

    public static Path getJarsPath() {
        return getManagerDir().resolve("jars");
    }

    public static Path getResourcesDirectory(String instanceName) {
        return getInstanceMinecraftDirectory(instanceName).resolve("resources");
    }

    public static Path getInstanceMinecraftDirectory(String instanceName) {
        return getInstanceMinecraftDirectory(getInstanceDirectory(instanceName));
    }

    public static Path getInstanceMinecraftDirectory(Path instanceDirectory) {
        return instanceDirectory.resolve("minecraft");
    }

    public static Path getInstanceLogs(String instanceName) {
        return getManagerDir().resolve("logs")
                              .resolve(instanceName);
    }

    public static Path getLogConfiguration(String logFileId) {
        return getLogs().resolve(logFileId == null ? "pre-1.6.xml" : logFileId);
    }

    public static Path getLogs() {
        return getManagerDir().resolve("logs");
    }

    public static Path getMinecraftLogs() {
        return getLogs().resolve("minecraft-logs");
    }
}
