package com.squedgy.mc.mod.manager.configuration.http;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Replacement {
    public final String replace;
    public final String with;

    @JsonCreator
    public Replacement(String replace, String with) {
        this.replace = replace;
        this.with = with;
    }
}
