package com.squedgy.mc.mod.manager.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import org.slf4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Function;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static org.slf4j.LoggerFactory.getLogger;

public class LauncherOptions {
    public static final String FORGE_VERSION = "forge-version", MIN_MEMORY = "min-ram", MAX_MEMORY = "max-ram", DEFAULT_HEIGHT = "default-height", DEFAULT_WIDTH = "default-width", USERNAME = "username", JAVA_HOME = "java-home";
    private static final Logger LOG = getLogger(LauncherOptions.class);
    private static final Path OPTIONS_PATH = ConfigPaths.getConfigPath()
                                                        .resolve("launcher.json");
    private static JsonNode options;

    public static String getJavaExecutableString() {
        return getJavaBinString() + "java";
    }

    public static String getJavaBinString() {
        return getProperty(JAVA_HOME).map(j -> j.textValue() + "/bin/")
                                     .orElse("");
    }

    public static Optional<JsonNode> getProperty(String property) {
        ensureOptions();
        String[] args = property.split("\\.");
        JsonNode ret = options;
        for(String arg : args) {
            if(ret.has(arg)) ret = ret.get(arg);
            else return Optional.empty();
        }
        return Optional.of(ret);
    }

    private static void ensureOptions() {
        if(options == null) loadOptions();
    }

    public static void loadOptions() {
        try {
            options = objectMapper().readTree(new FileInputStream(OPTIONS_PATH.toFile()));
        } catch(IOException e) {
            try {
                options = objectMapper().readTree("{}");
            } catch(JsonProcessingException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static JsonNode getPropertyOrDefault(String property, JsonNode def) {
        Optional<JsonNode> prop = getProperty(property);
        return prop.orElse(def);
    }

    public static Optional<String> getStringProperty(String property) {
        return getMappedProperty(property, JsonNode::textValue);
    }

    public static <T> Optional<T> getMappedProperty(String property, Function<JsonNode, T> transform) {
        return getProperty(property).map(transform);
    }

    public static synchronized boolean setProperty(String property, Object value) {
        ensureOptions();
        String[] args = property.split("\\.");
        JsonNode currentNode = options;
        for(int i = 0; i < args.length - 1; i++) {
            String arg = args[i];
            if(currentNode.has(arg)) currentNode = currentNode.get(arg);
            else if(currentNode.isObject()) {
                ((ObjectNode) currentNode).putObject(arg);
                currentNode = currentNode.get(arg);
            } else {
                LOG.info("Failed to set property at depth {} for property {}. Is that property already a non-object value?", i, arg);
                return false;
            }
        }

        String finalArg = args[args.length - 1];
        if(currentNode.isObject()) {
            ((ObjectNode) currentNode).putPOJO(finalArg, value);
        } else {
            LOG.info(
                "Failed to set property at depth {} for child property {}. Is that property already a non-object value?",
                args.length - 1,
                finalArg
            );
            return false;
        }

        JsonNode currentOptions = options;

        try {
            new ObjectMapper().writeValue(OPTIONS_PATH.toFile(), currentOptions);
        } catch(IOException e) {
            LOG.error("Failed to save options.", e);
        }

        return true;
    }
}
