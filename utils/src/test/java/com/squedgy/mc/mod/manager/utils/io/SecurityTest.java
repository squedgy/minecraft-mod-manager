package com.squedgy.mc.mod.manager.utils.io;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class SecurityTest {

    @Test
    public void matching_bytes_resolve_to_the_same_sha1() throws IOException, NoSuchAlgorithmException {
        byte[] testBytes = new byte[]{12, 45, 64, 78, 15};
        byte[] first = Security.getSha1(new ByteArrayInputStream(testBytes));
        byte[] nonst = Security.getSha1(new ByteArrayInputStream(testBytes));

        assertArrayEquals(first, nonst);
    }

    @Test
    public void matching_values_resolve_to_the_same_sha1() throws IOException, NoSuchAlgorithmException {
        String fileContents = "this is a test string";
        byte[] first = Security.getSha1(new ByteArrayInputStream(fileContents.getBytes()));
        byte[] nonst = Security.getSha1(SecurityTest.class.getResource("test.txt"));

        assertArrayEquals(first, nonst);
    }

}
