package com.squedgy.mc.mod.manager.utils;

import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

public class FormattedRuntimeException extends RuntimeException {
    public FormattedRuntimeException(String message) {
        super(message);
    }

    public FormattedRuntimeException(final String message, final Object... args) {
        this(MessageFormatter.arrayFormat(message, args));
    }

    private FormattedRuntimeException(FormattingTuple tuple) {
        super(tuple.getMessage(), tuple.getThrowable());
    }

    public FormattedRuntimeException(String message, Exception ex) {
        super(message, ex);
    }
}
