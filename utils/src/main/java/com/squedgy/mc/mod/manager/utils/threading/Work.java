package com.squedgy.mc.mod.manager.utils.threading;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class Work {
    private static final Logger LOG = getLogger(Work.class);
    private static final ExecutorService WORKERS;

    public static class OrderedTask implements Runnable, Comparable<OrderedTask> {
        protected Runnable task, then;
        private int order;

        public OrderedTask(Runnable task, int order) {
            this(task, order, () -> {});
        }

        public OrderedTask(Runnable task, int order, Runnable then) {
            this.task = task;
            this.order = order;
            this.then = then;
        }

        public void run() {
            LOG.info("Starting task: {}", this.toString());
            task.run();
            then.run();
        }

        public int getOrder() { return order; }

        @Override
        public int compareTo(OrderedTask o) {
            return Integer.compare(getOrder(), o.getOrder());
        }

        @Override
        public boolean equals(Object o) {
            if(this == o) return true;
            if(o == null || getClass() != o.getClass()) return false;
            OrderedTask that = (OrderedTask) o;
            return order == that.order && Objects.equals(task, that.task) && Objects.equals(then, that.then);
        }

        @Override
        public int hashCode() {
            return Objects.hash(task, then, order);
        }

        @Override
        public String toString() {
            return "OrderedTask[" + order + "](" + task + ", " + then + ")";
        }
    }

    static {
        int availableProcessors = Runtime.getRuntime()
                                         .availableProcessors();
        WORKERS = new ThreadPoolExecutor(2, availableProcessors - 1, 5, TimeUnit.SECONDS, new PriorityBlockingQueue<>(200));
    }

    public static void addTask(OrderedTask task) {
        WORKERS.execute(task);
    }

    public static void addTasks(OrderedTask... tasks) {
        addTasks(List.of(tasks));
    }

    public static void addTasks(Collection<OrderedTask> tasks) {
        tasks.forEach(Work::addTask);
    }

    public static void finish() {
        WORKERS.shutdown();
    }
}
