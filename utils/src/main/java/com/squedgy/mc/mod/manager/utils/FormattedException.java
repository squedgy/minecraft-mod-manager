package com.squedgy.mc.mod.manager.utils;

import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

public class FormattedException extends Exception {
    public FormattedException(String message) {
        super(message);
    }

    public FormattedException(final String message, final Object... args) {
        this(MessageFormatter.arrayFormat(message, args));
    }

    private FormattedException(FormattingTuple tuple) {
        super(tuple.getMessage(), tuple.getThrowable());
    }

    public FormattedException(String message, Exception ex) {
        super(message, ex);
    }
}
