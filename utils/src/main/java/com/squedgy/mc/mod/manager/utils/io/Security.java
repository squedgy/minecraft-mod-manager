package com.squedgy.mc.mod.manager.utils.io;

import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static java.security.MessageDigest.getInstance;
import static org.slf4j.LoggerFactory.getLogger;

public abstract class Security {
    private static final Logger LOG = getLogger(Security.class);

    public static String forgeChecksum(Path path) throws IOException, NoSuchAlgorithmException {
        try(InputStream stream = Files.newInputStream(path)) {
            return forgeChecksum(stream);
        }
    }

    public static String forgeChecksum(InputStream stream) throws NoSuchAlgorithmException, IOException {
        try(DigestInputStream digest = new DigestInputStream(stream, getInstance("SHA-1"))) {
            while(digest.read() != -1) ;
            String hash = new BigInteger(
                1,
                digest.getMessageDigest()
                      .digest()
            ).toString(16);
            return forgePad(hash);
        }
    }

    private static String forgePad(String hash) {
        return (String.format("%040d", 0) + hash).substring(hash.length());
    }

    public static byte[] getSha1(Path path) throws IOException, NoSuchAlgorithmException {
        try(InputStream stream = Files.newInputStream(path)) {
            return getSha1(stream);
        }
    }

    public static byte[] getSha1(InputStream stream) throws IOException, NoSuchAlgorithmException {
        try(DigestInputStream digest = new DigestInputStream(stream, getInstance("SHA-1"))) {
            // Read the stream into the message digest
            while(digest.read() != -1) ;
            // get result
            return digest.getMessageDigest()
                         .digest();
        }
    }

    public static byte[] getSha1(URL resource) throws IOException, NoSuchAlgorithmException {
        try(InputStream stream = resource.openStream()) {
            return getSha1(stream);
        }
    }

    public static boolean shasEqual(byte[] one, byte[] two) {
        return Arrays.equals(one, two);
    }

}
