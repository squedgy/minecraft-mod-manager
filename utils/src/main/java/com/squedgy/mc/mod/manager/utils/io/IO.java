package com.squedgy.mc.mod.manager.utils.io;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.slf4j.LoggerFactory.getLogger;

public abstract class IO {
    private final static Logger LOG = getLogger(IO.class);

    public static void mergeJars(Path external, Path into) throws IOException {
        mergeJars(into, external, into);
    }

    public static void mergeJars(Path one, Path two, Path target) throws IOException {
        Path tempFile = Files.createTempFile(null, ".jar");

        try(JarOutputStream output = new JarOutputStream(new FileOutputStream(tempFile.toFile()));
            ZipFile zipOne = new ZipFile(one.toFile());
            ZipFile zipTwo = new ZipFile(two.toFile())
        ) {
            Set<String> entries = new HashSet<>();
            zipOne.stream()
                  .forEach(e -> entries.add(e.getName()));
            zipTwo.stream()
                  .forEach(e -> entries.add(e.getName()));
            for(String entry : entries) {
                var from = zipTwo.getEntry(entry);
                var base = zipOne.getEntry(entry);
                if(from != null) putEntry(output, zipTwo, from);
                else if(base != null) putEntry(output, zipOne, base);
                else throw new IOException("Unknown entry \"" + entry + "\"... how?");
            }
        }

        Files.copy(tempFile, target, REPLACE_EXISTING);
    }

    private static void putEntry(ZipOutputStream output, ZipFile file, ZipEntry entry) throws IOException {
        ZipEntry newEntry = new ZipEntry(entry.getName());
        output.putNextEntry(newEntry);
        file.getInputStream(entry)
            .transferTo(output);
    }

    public static void extractJar(Path jarFile, Path targetDir) throws UncheckedIOException {
        extractJar(jarFile, targetDir, Collections.emptyList());
    }

    public static void extractJar(Path jarFile, Path targetDir, List<String> excludes) throws UncheckedIOException {
        LOG.trace("Attempting to dump jar \"{}\"'s contents to \"{}\" excluding entries {}", jarFile, targetDir, excludes);
        try(JarFile file = new JarFile(jarFile.toFile())) {
            file.stream()
                .forEach(entry -> {
                    String name = entry.getName();
                    if(excludes.stream()
                               .anyMatch(exclude -> entry.getName()
                                                         .startsWith(exclude))) {
                        LOG.trace("excluding entry {}", name);
                        return;
                    }
                    try(InputStream target = file.getInputStream(entry)) {
                        var targetFile = targetDir.resolve(name);
                        var targetFileParent = targetFile.getParent()
                                                         .toFile();
                        if(entry.isDirectory()) {
                            if(!targetFile.toFile()
                                          .exists() &&
                               !targetFile.toFile()
                                          .mkdirs()) throw new IOException("Failed to create dir: " + targetFile);
                        } else if(targetFileParent.exists() || targetFileParent.mkdirs()) {
                            Files.copy(target, targetFile, REPLACE_EXISTING);
                        } else {
                            throw new IOException("Failed to create entry \"" + name + "\"'s parent dir \"" + targetFileParent + "\"");
                        }
                    } catch(IOException e) {
                        throw new UncheckedIOException(e);
                    }
                });
        } catch(IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void getJarEntry(Path jarFile, String entry, IOConsumer<InputStream> handle) throws IOException {
        try(JarFile file = new JarFile(jarFile.toFile())) {
            var zipEntry = file.getEntry(entry);
            if(zipEntry != null) {
                try(InputStream stream = file.getInputStream(zipEntry)) {
                    handle.accept(stream);
                    return;
                }
            } else if(LOG.isTraceEnabled()) {
                var entries = file.entries();
                while(entries.hasMoreElements()) LOG.error(
                    "Known entry: {}",
                    entries.nextElement()
                           .getName()
                );
            }
        }
        throw new FileNotFoundException("Failed to find a zip entry with name \"" + entry + "\" in jarFile: \"" + jarFile + "\"");
    }

    public static ObjectMapper objectMapper() {
        return new ObjectMapper().findAndRegisterModules()
                                 .setDefaultPropertyInclusion(JsonInclude.Include.NON_ABSENT);
    }

    public static void enforceParent(Path file) throws IOException {
        enforceDirectory(file.getParent());
    }

    public static void enforceDirectory(Path directory) throws IOException {
        File dir = directory.toFile();
        if(!dir.exists() && !dir.mkdirs() && !dir.exists())
            throw new IOException("Directory didn't exist and couldn't be made: " + directory);
    }

    public interface IOConsumer<T> {
        void accept(T item) throws IOException;
    }
}
