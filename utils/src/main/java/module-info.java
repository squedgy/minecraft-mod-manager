module com.squedgy.mc.mod.manager.utils {
    opens com.squedgy.mc.mod.manager.utils;
    exports com.squedgy.mc.mod.manager.utils;
    exports com.squedgy.mc.mod.manager.utils.io;
    exports com.squedgy.mc.mod.manager.utils.threading;

    requires transitive org.slf4j;
    requires transitive com.fasterxml.jackson.databind;
}