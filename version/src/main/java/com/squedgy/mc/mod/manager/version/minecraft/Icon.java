package com.squedgy.mc.mod.manager.version.minecraft;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.value.ObservableDoubleValue;
import javafx.scene.Node;

import java.io.IOException;

import static javafx.beans.binding.Bindings.createDoubleBinding;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "@class")
public abstract class Icon {

    protected Icon() {}

    @JsonIgnore
    public Node getDisplayableIcon(Node parent) throws IOException {
        return getDisplayableIcon(createDoubleBinding(() -> parent.maxWidth(-1)), createDoubleBinding(() -> parent.maxHeight(-1)));
    }

    @JsonIgnore
    public abstract Node getDisplayableIcon(DoubleBinding maxWidth, DoubleBinding maxHeight) throws IOException;

    @JsonIgnore
    public Node getDisplayableIcon(ReadOnlyDoubleProperty maxHeight) throws IOException {
        return getDisplayableIcon(createDoubleBinding(() -> Double.MAX_VALUE), createDoubleBinding(maxHeight::get, maxHeight));
    }

    @JsonIgnore
    public Node getDisplayableIcon(ReadOnlyDoubleProperty maxWidth, ReadOnlyDoubleProperty maxHeight) throws IOException {
        return getDisplayableIcon(createDoubleBinding(maxWidth::get, maxWidth), createDoubleBinding(maxHeight::get, maxHeight));
    }

    @JsonIgnore
    public abstract ObservableDoubleValue getWidth();

    @JsonIgnore
    public abstract ObservableDoubleValue getHeight();

    public abstract void setMaxHeight(ObservableDoubleValue maxHeight);

    public abstract void setMaxWidth(ObservableDoubleValue maxWidth);

    public abstract void setMinHeight(ObservableDoubleValue minHeight);

    public abstract void setMinWidth(ObservableDoubleValue minWidth);
}
