package com.squedgy.mc.mod.manager.version.library;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.http.Client;
import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.utils.io.Security;
import com.squedgy.mc.mod.manager.version.Downloads;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

public class Artifact {
    private static final Logger LOG = getLogger(Artifact.class);
    public final long size;
    protected final String path;
    protected final String sha1;
    protected final String url;
    protected final Optional<List<Artifact>> merge;

    public Artifact(String path, String url) {
        this(path, null, url, -1, Optional.empty());
    }

    @JsonCreator
    public Artifact(String path, String sha1, String url, long size, Optional<List<Artifact>> merge) {
        this.path = path;
        this.sha1 = sha1;
        this.url = url;
        this.size = size;
        this.merge = merge;
    }

    public Artifact(String path, String url, Optional<List<Artifact>> merge) {
        this(path, null, url, -1, merge);
    }

    @JsonIgnore
    public List<Artifact> getAll() {
        List<Artifact> arts = new LinkedList<>();
        arts.add(this);
        merge.map(a -> a.stream()
                        .flatMap(art -> art.getAll()
                                           .stream())
                        .collect(toList()))
             .ifPresent(arts::addAll);
        return arts;
    }

    @JsonIgnore
    public boolean isZip() {
        return url.endsWith(".zip");
    }

    public boolean doesNotExist() {
        Path location = ConfigPaths.getLibraryPath()
                                   .resolve(path);
        return !location.toFile()
                        .exists();
    }

    public String getPath() {
        return path;
    }

    public String getPath(String name) {
        if(path == null) return pathFromName(name);
        return path;
    }

    public static String pathFromName(String name) {
        String[] parts = name.split(":");
        String domain = parts[0].replace('.', '/'), id = parts[1].replace('.', '/'), version = parts[2], classifier = null;

        String pathPattern = "%1$s/%2$s/%3$s/%2$s-%3$s.jar";
        if(parts.length == 4) {
            pathPattern = "%1$s/%2$s/%3$s/%2$s-%3$s-%4$s.jar";
            classifier = parts[3];
        }

        return String.format(pathPattern, domain.replace('.', '/'), id, version, classifier);
    }

    public String getSha1() {
        return sha1;
    }

    public String getUrl() {
        return url;
    }

    public Optional<List<Artifact>> getMerge() {
        return merge;
    }

    /**
     * Downloads this artifact and places it at it's path within the library directory
     *
     * NOTE: this doesn't check if it already exists
     *
     * @throws UncheckedIOException if we can't find the algorithm {@code "SHA-1"}, the downloaded file doesn't match security checks, or there's an issue trying to download or create the file
     */
    public void download() {
        try {
            Path libraryLocation = ConfigPaths.getLibraryPath()
                                              .resolve(path);
            File parentDir = libraryLocation.getParent()
                                            .toFile();
            LOG.trace("Downloading artifact from {}", url);
            Path newJar = Client.download(url, ".jar");
            try(InputStream stream = new FileInputStream(newJar.toFile())) {
                if((sha1 != null && !Arrays.equals(sha1.getBytes(), Security.getSha1(stream))) &&
                   (size != -1 && Files.size(newJar) != size)) {
                    LOG.debug("Failing artifact's sha {} and size {}", sha1, size);
                    throw new SecurityException("Artifact doesn't match recorded sha or length: " + path);
                }
                if(parentDir.mkdirs() || parentDir.exists()) Files.copy(newJar, libraryLocation, StandardCopyOption.REPLACE_EXISTING);
                else throw new IOException("Failed to create artifact parent dir: " + parentDir);
            } catch(NoSuchAlgorithmException e) {
                LOG.error("", e);
                // delete the bad jar
                Files.delete(newJar);
                throw e;
            }
        } catch(InterruptedException | URISyntaxException | RequestException | IOException | NoSuchAlgorithmException e) {
            LOG.error("", e);
            throw new UncheckedIOException(new IOException("Failed to download artifact: " + path, e));
        }

    }

    /**
     * This determines whether the given object is an artifact who's fields
     * perfectly match this artifact's
     *
     * @param o
     */
    public boolean matches(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Artifact other = (Artifact) o;
        return Objects.equals(path, other.path) &&
               Objects.equals(url, other.url) &&
               Objects.equals(sha1, other.sha1) &&
               Objects.equals(size, other.size) &&
               Objects.equals(merge, other.merge);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }

    /**
     * Two artifacts are considered equal if they appear to reference the same local artifact.
     *
     * This is determined purely by their path.
     *
     * @param o
     */
    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        Artifact artifact = (Artifact) o;
        return Objects.equals(path, artifact.path);
    }

    @Override
    public String toString() {
        return "[" + url + '@' + path + "]";
    }

    public static Artifact fromDownloads(String name, Downloads.Download download) {
        return new Artifact(pathFromName(name), download.sha1, download.url, download.size, Optional.empty());
    }

    public static String nameFromPath(String path) {
        return nameFromPath(path, false);
    }

    public static String nameFromPath(String path, boolean withClassifier) {
        int lastSlash = path.lastIndexOf('/');
        String jar = path.substring(lastSlash);
        int firstDash = path.indexOf('-');
        String identifier = jar.substring(0, firstDash);
        String version = jar.substring(firstDash + 1);
        String namespace = path.substring(0, path.indexOf("/" + identifier.replace('.', '/') + "/"))
                               .replace('/', '.');

        if(withClassifier) {
            int lastDash = path.lastIndexOf('-');
            if(lastDash == firstDash)
                throw new IllegalArgumentException("Passed path \"" + path + "\" doesn't appear to have a classifier!");
            version = jar.substring(firstDash + 1, lastDash);
            String classifier = jar.substring(lastDash + 1);
            return namespace + ':' + identifier + ':' + version + ':' + classifier;
        }

        return namespace + ':' + identifier + ':' + version;
    }
}
