package com.squedgy.mc.mod.manager.version;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Logging {
    public final String argument, type;
    public final LogFile file;

    @JsonCreator
    public Logging(String argument, String type, LogFile file) {
        this.argument = argument;
        this.type = type;
        this.file = file;
    }

    @Override
    public String toString() {
        return "[" + "argument='" + argument + '\'' + ", type='" + type + '\'' + ", file=" + file + ']';
    }

    public static class LogFile {
        public final String id, sha1, url;
        public final long size;

        public LogFile(String id, String sha1, String url, long size) {
            this.id = id;
            this.sha1 = sha1;
            this.url = url;
            this.size = size;
        }

        @Override
        public String toString() {
            return "LogFile{" + "id='" + id + '\'' + ", sha1='" + sha1 + '\'' + ", url='" + url + '\'' + ", size=" + size + '}';
        }
    }
}
