package com.squedgy.mc.mod.manager.version;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.http.RequestException;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;

import static com.squedgy.mc.mod.manager.http.Client.enforceRequest;
import static com.squedgy.mc.mod.manager.http.Client.get;
import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static java.util.stream.Collectors.toList;

public class AssetIndex {
    public final String id, sha1, url;
    public final long size, totalSize;

    @JsonCreator
    public AssetIndex(String id, String sha1, String url, long size, long totalSize) {
        this.id = id;
        this.sha1 = sha1;
        this.url = url;
        this.size = size;
        this.totalSize = totalSize;
    }

    public void enforceAssetsIndexFile() {
        var indexFile = ConfigPaths.getAssetIndex(id);
        if(!indexFile.toFile()
                     .exists()) {
            var parent = indexFile.getParent()
                                  .toFile();
            if(!parent.exists() && !parent.mkdirs() && !parent.exists())
                throw new UncheckedIOException(new IOException("Failed to create parent directory for asset index file: " + parent));
            try {
                var request = get(url);
                var response = enforceRequest(request.build());
                Files.copy(response.body(), indexFile);
            } catch(RequestException | InterruptedException | IOException | URISyntaxException e) {
                throw new UncheckedIOException(new IOException("Failed to retrieve or save asset index", e));
            }
        }
    }

    @JsonIgnore
    public AssetsMap getAssetIndexFile() throws IOException {
        var file = ConfigPaths.getAssetIndex(id);
        return objectMapper().readValue(file.toFile(), AssetsMap.class);
    }

    @Override
    public String toString() {
        return "AssetIndex{" +
               "id='" +
               id +
               '\'' +
               ", sha1='" +
               sha1 +
               '\'' +
               ", url='" +
               url +
               '\'' +
               ", size=" +
               size +
               ", totalSize=" +
               totalSize +
               '}';
    }

    @JsonIgnoreProperties({"virtual"})
    public static final class AssetsMap {
        public Map<String, AssetNode> objects;
        public boolean map_to_resources;

        public List<AssetNode> getMissingAssets() {
            return objects.values()
                          .stream()
                          .filter(AssetNode::notExist)
                          .collect(toList());
        }
    }

    public static final class AssetNode {
        private static final String DEFAULT_ASSET_URL_PATTERN = "http://resources.download.minecraft.net/%s/%s";

        public String hash;
        public int size;

        public boolean notExist() {
            return !pathTo().toFile()
                            .exists();
        }

        public Path pathTo() {
            return ConfigPaths.getModernAssetPath()
                              .resolve(hash.substring(0, 2))
                              .resolve(hash);
        }

        public void download() {
            try {
                var path = pathTo();
                var parent = path.getParent()
                                 .toFile();
                if(!parent.exists() && !parent.mkdirs() && !parent.exists())
                    throw new IOException("Failed to create artifact's parent directory: " + parent);
                var request = get(String.format(DEFAULT_ASSET_URL_PATTERN, hash.substring(0, 2), hash));
                var response = enforceRequest(request.build());
                Files.copy(response.body(), path, StandardCopyOption.REPLACE_EXISTING);
            } catch(RequestException | InterruptedException | IOException | URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
