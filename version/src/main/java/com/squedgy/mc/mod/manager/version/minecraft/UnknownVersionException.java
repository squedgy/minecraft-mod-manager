package com.squedgy.mc.mod.manager.version.minecraft;

import com.squedgy.mc.mod.manager.utils.FormattedException;

public class UnknownVersionException extends FormattedException {
    public UnknownVersionException(String message) {
        super(message);
    }

    public UnknownVersionException(String message, Object... args) {
        super(message, args);
    }

    public UnknownVersionException(String message, Exception ex) {
        super(message, ex);
    }
}
