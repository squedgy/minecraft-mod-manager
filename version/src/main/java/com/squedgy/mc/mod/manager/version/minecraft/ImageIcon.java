package com.squedgy.mc.mod.manager.version.minecraft;

import com.fasterxml.jackson.annotation.JsonCreator;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableDoubleValue;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.slf4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;

import static org.slf4j.LoggerFactory.getLogger;

public final class ImageIcon extends Icon {
    private static final Logger LOG = getLogger(ImageIcon.class);

    public static ImageIcon DEFAULT_ICON;

    static {
        try {
            DEFAULT_ICON = new ImageIcon(Icon.class.getResource("minecraft-icon.png")
                                                   .toURI());
        } catch(URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public final URI image;
    private final ImageView displayableIcon;

    @JsonCreator
    public ImageIcon(URI image) {
        this.image = image;
        displayableIcon = new ImageView(new Image(image.toString()));
        displayableIcon.preserveRatioProperty()
                       .set(true);
    }

    @Override
    public Node getDisplayableIcon(DoubleBinding maxWidth, DoubleBinding maxHeight) {
        displayableIcon.fitWidthProperty()
                       .bind(maxWidth);
        displayableIcon.fitHeightProperty()
                       .bind(maxHeight);
        return displayableIcon;
    }

    @Override
    public ObservableDoubleValue getWidth() {
        return new SimpleDoubleProperty(displayableIcon.getFitWidth());
    }

    @Override
    public ObservableDoubleValue getHeight() {
        return new SimpleDoubleProperty(displayableIcon.getFitHeight());
    }

    @Override
    public void setMaxHeight(ObservableDoubleValue maxHeight) {
        displayableIcon.fitHeightProperty()
                       .bind(maxHeight);
    }

    @Override
    public void setMaxWidth(ObservableDoubleValue maxWidth) {
        displayableIcon.fitWidthProperty()
                       .bind(maxWidth);
    }

    @Override
    public void setMinHeight(ObservableDoubleValue minHeight) {

    }

    @Override
    public void setMinWidth(ObservableDoubleValue minWidth) {

    }
}
