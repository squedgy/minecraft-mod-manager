package com.squedgy.mc.mod.manager.version.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.squedgy.mc.mod.manager.version.Arguments;
import com.squedgy.mc.mod.manager.version.AssetIndex;
import com.squedgy.mc.mod.manager.version.Downloads;
import com.squedgy.mc.mod.manager.version.LoggingInfo;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftLibrary;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class Post13ClientJson extends ClientJson {
    public static final Logger LOG = getLogger(Post13ClientJson.class);

    @JsonCreator
    public Post13ClientJson(String id,
                            String mainClass,
                            String type,
                            String assets,
                            String releaseTime,
                            String time,
                            AssetIndex assetIndex,
                            Downloads downloads,
                            Arguments arguments,
                            List<MinecraftLibrary> libraries,
                            Optional<LoggingInfo> logging) {
        super(id,
              mainClass,
              type,
              assets,
              releaseTime,
              time,
              assetIndex,
              addDownloadLibraries(id, libraries, downloads),
              logging,
              arguments
        );
    }

    protected static List<Library> addDownloadLibraries(String version, List<MinecraftLibrary> libraries, Downloads downloads) {
        var allLibraries = new HashMap<String, Library>();

        libraries.forEach(library -> {
            if(!allLibraries.containsKey(library.name)) allLibraries.put(library.name, library);
            else {
                Library merged = Library.joining(library, Library.joining(library, allLibraries.get(library.name)));
                allLibraries.put(merged.name, merged);
            }
        });

        String clientName = "net.minecraft:client:" + version;
        String serverName = "net.minecraft:server:" + version;
        String clientMappingName = "net.minecraft:client-mappings:" + version;
        String serverMappingName = "net.minecraft:client-mappings:" + version;

        if(!allLibraries.containsKey(clientName) && downloads.client != null) {
            allLibraries.put(clientName, Library.of(clientName, downloads.client, true, false));
        }
        if(!allLibraries.containsKey(serverName) && downloads.server != null) {
            allLibraries.put(serverName, Library.of(serverName, downloads.server, false, true));
        }
        if(!allLibraries.containsKey(clientMappingName) && downloads.clientMappings != null) {
            allLibraries.put(clientMappingName, Library.of(clientMappingName, downloads.clientMappings, true, false));
        }
        if(!allLibraries.containsKey(serverMappingName) && downloads.serverMappings != null) {
            allLibraries.put(serverMappingName, Library.of(serverMappingName, downloads.serverMappings, false, true));
        }

        return new LinkedList<>(allLibraries.values());
    }

}
