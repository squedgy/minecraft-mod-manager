package com.squedgy.mc.mod.manager.version.library;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Objects;
import java.util.regex.Pattern;

import static com.squedgy.mc.mod.manager.version.Arguments.getOsName;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class OperatingSystem {

    public final OS name;
    public final Pattern version;

    @JsonCreator
    public OperatingSystem(OS name, Pattern version) {
        this.name = name;
        this.version = version;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, version);
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        OperatingSystem that = (OperatingSystem) o;
        return name == that.name && Objects.equals(version, that.version);
    }

    public enum OS {
        OSX, WINDOWS, LINUX;

        public static OS getCurrentOs() {
            return getFromName(getOsName());
        }

        @JsonCreator
        public static OS getFromName(String name) {
            for(OS os : values()) {
                if(os.name()
                     .equalsIgnoreCase(name)) {
                    return os;
                }
            }
            throw new IllegalArgumentException("Provided OS name (" + name + ") doesn't match known OS name.");
        }
    }
}
