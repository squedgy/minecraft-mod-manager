package com.squedgy.mc.mod.manager.version.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squedgy.mc.mod.manager.version.Arguments;
import com.squedgy.mc.mod.manager.version.AssetIndex;
import com.squedgy.mc.mod.manager.version.Downloads;
import com.squedgy.mc.mod.manager.version.LoggingInfo;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftLibrary;

import java.util.List;
import java.util.Optional;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pre13ClientJson extends Post13ClientJson {
    public Pre13ClientJson(String id,
                           String type,
                           String mainClass,
                           String assets,
                           String releaseTime,
                           String time,
                           AssetIndex assetIndex,
                           Downloads downloads,
                           String minecraftArguments,
                           List<MinecraftLibrary> libraries,
                           Optional<LoggingInfo> logging) {
        super(
            id,
            mainClass,
            type,
            assets,
            releaseTime,
            time,
            assetIndex,
            downloads,
            Arguments.fromMcArgs(minecraftArguments),
            libraries,
            logging
        );
    }
}
