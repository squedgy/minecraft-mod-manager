package com.squedgy.mc.mod.manager.version;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Optional;

public class Downloads {
    public final Download clientMappings, serverMappings, client, server, windowsServer;

    @JsonCreator
    public Downloads(Download client_mappings, Download server_mappings, Download client, Download server, Download windows_server) {
        this.clientMappings = client_mappings;
        this.serverMappings = server_mappings;
        this.client = client;
        this.server = server;
        this.windowsServer = windows_server;
    }

    @Override
    public String toString() {
        return "Downloads{" +
               "clientMappings=" +
               clientMappings +
               ", serverMappings=" +
               serverMappings +
               ", client=" +
               client +
               ", server=" +
               server +
               '}';
    }

    public static class Download {
        public final String sha1, url;
        public final Optional<String> merge;
        public final long size;

        public Download(String sha1, String url, long size, Optional<String> merge) {
            this.sha1 = sha1;
            this.url = url;
            this.size = size;
            this.merge = merge;
        }

        @Override
        public String toString() {
            return "Download{" + "sha1='" + sha1 + '\'' + ", url='" + url + '\'' + ", merge='" + merge + '\'' + ", size=" + size + '}';
        }
    }
}
