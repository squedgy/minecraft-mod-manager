package com.squedgy.mc.mod.manager.version;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Optional;

public final class LoggingInfo {
    public final Optional<Logging> client;

    @JsonCreator
    public LoggingInfo(Optional<Logging> client) {this.client = client;}

    @Override
    public String toString() {
        return "[" + "client=" + client + ']';
    }
}
