package com.squedgy.mc.mod.manager.version.library;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Objects;
import java.util.Optional;

public class LibraryRule {
    public final Action action;
    public final Optional<OperatingSystem> os;

    @JsonCreator
    public LibraryRule(Action action, Optional<OperatingSystem> os) {
        this.action = action;
        this.os = os;
    }

    public boolean matches(boolean client) {
        boolean act = action == Action.ALLOW;
        boolean isOs = os.map(os -> os.name == OperatingSystem.OS.getCurrentOs())
                         .orElse(true);
        return act == isOs;
    }

    @Override
    public int hashCode() {
        return Objects.hash(action, os);
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        LibraryRule that = (LibraryRule) o;
        return Objects.equals(action, that.action) && Objects.equals(os, that.os);
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch(JsonProcessingException e) {
            return "Failed to write " + getClass().getSimpleName() + " as json";
        }
    }

    public enum Action {
        ALLOW, DISALLOW;

        @JsonCreator
        public static Action fromName(String action) {
            for(Action a : values()) {
                if(a.name()
                    .equalsIgnoreCase(action)) return a;
            }

            throw new IllegalArgumentException("There's no action with name \"" + action + "\"");
        }
    }
}
