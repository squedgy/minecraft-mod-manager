package com.squedgy.mc.mod.manager.version.settings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface InstanceSettings {
    /**
     * Various Instance Options that are available to this instance
     * @return
     */
    Class<? extends InstanceOptions>[] settings();
    /**
     * If this value is it's default value it defaults to the first item in settings
     * @return
     */
    Class<? extends InstanceTitle> title() default InstanceTitle.class;
}
