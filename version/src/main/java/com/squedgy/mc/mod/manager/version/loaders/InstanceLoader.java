package com.squedgy.mc.mod.manager.version.loaders;

import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.utils.FormattedException;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static org.slf4j.LoggerFactory.getLogger;

public abstract class InstanceLoader {
    private static Logger LOG = getLogger(InstanceLoader.class);

    public static void saveInstance(MinecraftInstance instance) throws FormattedException {
        var dir = ConfigPaths.getInstanceDirectory(instance.getName())
                             .toFile();
        if(!dir.exists() && !dir.mkdirs() && !dir.exists())
            throw new FormattedException("Failed to create instance {}'s directory", instance.getName());
        try {
            var instanceConfig = ConfigPaths.getInstanceConfigFile(instance.getName());
            objectMapper().writeValue(instanceConfig.toFile(), instance);
        } catch(IOException e) {
            throw new FormattedException("Failed to create instance {}'s configuration file", instance.getName(), e);
        }
    }

    public static Set<MinecraftInstance> getInstances() {
        var instancesDir = ConfigPaths.getInstancesDirectory();
        var dir = instancesDir.toFile();
        var names = new HashSet<MinecraftInstance>();
        LOG.debug("instance dir {}", instancesDir);

        if(dir.exists() && dir.isDirectory()) {
            for(var potentialInstance : dir.listFiles()) {
                if(potentialInstance.isDirectory()) {
                    try {
                        var instanceJson = getInstanceJson(potentialInstance.getName());
                        if(instanceJson.exists()) {
                            LOG.trace("Adding instance {}", potentialInstance.getName());
                            names.add(objectMapper().readValue(instanceJson, MinecraftInstance.class));
                        }
                    } catch(IOException | IllegalArgumentException e) {
                        LOG.warn(
                            "Directory within \"{}\" contained directory \"{}\" with an incorrect instance.json",
                            instancesDir,
                            potentialInstance.getName(),
                            e
                        );
                    }
                }
            }
        }

        return names;
    }

    private static File getInstanceJson(String instanceName) {
        var instanceConfig = ConfigPaths.getInstanceConfigFile(instanceName)
                                        .toFile();
        if(instanceConfig.exists()) {
            return instanceConfig;
        }
        throw new IllegalArgumentException("Received bad instance directory " + instanceConfig.getAbsolutePath());
    }

    public static MinecraftInstance getInstanceByName(String instanceName) throws IllegalArgumentException, IOException {
        var instanceDir = ConfigPaths.getInstanceDirectory(instanceName)
                                     .toFile();

        if(instanceDir.exists()) {
            if(instanceDir.isDirectory()) {
                return objectMapper().readValue(getInstanceJson(instanceName), MinecraftInstance.class);
            }
        }

        throw new IllegalArgumentException("Instance \"" + instanceName + "\" doesn't exist");
    }

}
