package com.squedgy.mc.mod.manager.version.library;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.squedgy.mc.mod.manager.http.Client;
import com.squedgy.mc.mod.manager.http.RequestException;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static com.squedgy.mc.mod.manager.http.Client.head;
import static java.util.stream.Collectors.toUnmodifiableMap;
import static org.slf4j.LoggerFactory.getLogger;

public class LibraryDownloads {
    private static final Logger LOG = getLogger(LibraryDownloads.class);
    private static final String DEFAULT_BASE_URL = "https://libraries.minecraft.net/";

    protected final Optional<Artifact> artifact;
    protected final Optional<Map<String, Artifact>> classifiers;

    @JsonCreator
    public LibraryDownloads(Optional<Artifact> artifact, Optional<Map<String, Artifact>> classifiers) {
        this.artifact = artifact;
        this.classifiers = classifiers.map(e -> e.entrySet()
                                                 .stream()
                                                 .collect(toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    public Optional<Artifact> getArtifact() {
        return artifact;
    }

    public Optional<Map<String, Artifact>> getClassifiers() {
        return classifiers;
    }

    @Override
    public int hashCode() {
        return Objects.hash(artifact, classifiers);
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        LibraryDownloads downloads = (LibraryDownloads) o;
        return Objects.equals(artifact, downloads.artifact) && Objects.equals(classifiers, downloads.classifiers);
    }

    @Override
    public String toString() {
        return "[art{" + artifact + "}, classifiers{" + classifiers + "}]";
    }

    /**
     * @param name the artifact's dependency name (EX. {@code "net.minecraft:client:1.0"})
     *
     * @throws RuntimeException if the enforceRequest called to double check if an artifact exists throws an IOException, URISyntaxException, or InterruptedException
     */
    public static LibraryDownloads of(String name) {
        return of(name, DEFAULT_BASE_URL);
    }

    /**
     * @param name    the artifact's dependency name (EX. {@code "net.minecraft:client:1.0"})
     * @param baseUrl the base url to append the artifact's name and classifier path to
     *
     * @throws RuntimeException if the enforceRequest called to double check if an artifact exists throws an IOException, URISyntaxException, or InterruptedException
     */
    public static LibraryDownloads of(String name, String baseUrl) {
        return of(name, baseUrl, Map.of());
    }

    /**
     * @param name    the artifact's dependency name (EX. {@code "net.minecraft:client:1.0"})
     * @param baseUrl the base url to append the artifact's name and classifier path to
     * @param natives a map of operating system -> artifact modifier for various natives
     *
     * @throws RuntimeException if the enforceRequest called to double check if an artifact exists throws an IOException, URISyntaxException, or InterruptedException
     */
    public static LibraryDownloads of(String name, String baseUrl, Map<OperatingSystem.OS, String> natives) {
        Optional<Map<String, Artifact>> classifiers = Optional.empty();
        if(natives.size() > 0) {
            classifiers = Optional.of(natives.values()
                                             .stream()
                                             .map(s -> new AbstractMap.SimpleEntry<>(s, artifactOf(name + ':' + s, baseUrl)))
                                             .filter(e -> Objects.nonNull(e.getValue()))
                                             .collect(toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue)));
        }
        return new LibraryDownloads(Optional.ofNullable(artifactOf(name, baseUrl)), classifiers);
    }

    private static Artifact artifactOf(String name, String baseUrl) {
        String path = Artifact.pathFromName(name);
        String url = baseUrl + (baseUrl.endsWith("/") ? "" : '/') + path;
        HttpRequest request = head(url).build();
        try {
            Client.enforceRequest(request);
        } catch(RequestException e) {
            LOG.warn("Failed to find resource {}.", request, e);
            return null;
        } catch(InterruptedException | IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }

        return new Artifact(path, null, url, -1, Optional.empty());
    }

    /**
     * @param name    the artifact's dependency name (EX. {@code "net.minecraft:client:1.0"})
     * @param natives a map of operating system -> artifact modifier for various natives
     *
     * @throws RuntimeException if the enforceRequest called to double check if an artifact exists throws an IOException, URISyntaxException, or InterruptedException
     */
    public static LibraryDownloads of(String name, Map<OperatingSystem.OS, String> natives) {
        return of(name, DEFAULT_BASE_URL, natives);
    }

}
