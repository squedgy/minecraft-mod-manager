package com.squedgy.mc.mod.manager.version.minecraft;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.squedgy.mc.mod.manager.http.RequestException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.squedgy.mc.mod.manager.http.Client.enforceRequest;
import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static java.time.format.DateTimeFormatter.ofPattern;

public class VersionManifest {
    private static VersionManifest manifest;

    public final Latest latest;
    public final List<Version> versions;

    @JsonCreator
    public VersionManifest(Latest latest, List<Version> versions) {
        this.latest = latest;
        this.versions = versions;
    }

    public static VersionManifest getVersionManifest() throws InterruptedException, IOException, RequestException, URISyntaxException {
        if(manifest != null) return manifest;
        return refreshManifest();
    }

    public static VersionManifest refreshManifest() throws InterruptedException, URISyntaxException, RequestException, IOException {
        String versionManifestUrl = "https://launchermeta.mojang.com/mc/game/version_manifest.json";
        HttpRequest request = HttpRequest.newBuilder()
                                         .uri(URI.create(versionManifestUrl))
                                         .build();

        InputStream response = enforceRequest(request).body();
        return objectMapper().readValue(response, VersionManifest.class);
    }

    public static class Latest {
        public final String release, snapshot;

        public Latest(String release, String snapshot) {
            this.release = release;
            this.snapshot = snapshot;
        }
    }

    /**
     * The getters are used for formatting in the case of FXML property factories
     * to be guaranteed the actual value, just use the fields
     */
    public static class Version {
        public static final DateTimeFormatter GENERAL_DATE_TIME_FORMAT = ofPattern("uuuu/MM/dd HH:mm:ss");
        public final String id, type, url;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd'T'HH:mm:ssZZZZZ") public final ZonedDateTime time, releaseTime;

        @JsonCreator
        public Version(String id, String type, String url, ZonedDateTime time, ZonedDateTime releaseTime) {
            this.id = id;
            this.type = type;
            this.url = url;
            this.time = time;
            this.releaseTime = releaseTime;
        }

        public String getId() {
            return id;
        }

        public String getType() {
            return type;
        }

        public String getUrl() {
            return url;
        }

        @JsonIgnore
        public String getTime() {
            return time.toLocalDateTime()
                       .format(GENERAL_DATE_TIME_FORMAT);
        }

        @JsonIgnore
        public String getReleaseTime() {
            return releaseTime.toLocalDateTime()
                              .format(GENERAL_DATE_TIME_FORMAT);
        }
    }
}
