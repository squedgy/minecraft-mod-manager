package com.squedgy.mc.mod.manager.version.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.utils.io.IO;
import com.squedgy.mc.mod.manager.version.Downloads;
import com.squedgy.mc.mod.manager.version.library.Artifact;
import com.squedgy.mc.mod.manager.version.library.LibraryDownloads;
import com.squedgy.mc.mod.manager.version.library.LibraryRule;
import com.squedgy.mc.mod.manager.version.library.OperatingSystem;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftLibrary;
import org.slf4j.Logger;

import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.squedgy.mc.mod.manager.version.library.OperatingSystem.OS.getCurrentOs;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Implementations of this class should NEVER have instance fields and should do everything in their power to conform to this class's specification
 * this is an attempt at making (de)serialization easier for generic implementations of instances.
 *
 * If you need more control you should look at implementing your own {@link com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance instance implementation}
 * as that class (de)serializes into it's subtypes.
 */
@JsonInclude(JsonInclude.Include.NON_ABSENT)
@JsonPropertyOrder({"name", "downloads", "natives", "rules", "extract"})
public class Library {
    private static final Logger LOG = getLogger(Library.class);

    protected final String name;
    protected final Optional<Map<MinecraftLibrary.ExtractionAction, List<String>>> extract;
    protected final Optional<Map<OperatingSystem.OS, String>> natives;
    protected final List<LibraryRule> rules;
    protected final LibraryDownloads downloads;
    protected final boolean forClient;
    protected final boolean forServer;

    public Library(String name, LibraryDownloads downloads, boolean forClient, boolean forServer) {
        this(name, downloads, Optional.empty(), Optional.empty(), Collections.emptyList(), forClient, forServer);
    }

    @JsonCreator
    public Library(String name,
                   LibraryDownloads downloads,
                   Optional<Map<MinecraftLibrary.ExtractionAction, List<String>>> extract,
                   Optional<Map<OperatingSystem.OS, String>> natives,
                   List<LibraryRule> rules,
                   boolean forClient,
                   boolean forServer) {
        this.name = name;
        this.extract = extract;
        this.natives = natives;
        this.rules = rules;
        this.downloads = downloads;
        this.forClient = forClient;
        this.forServer = forServer;
    }

    public final boolean isNeeded(boolean client) {
        return (client ? forClient : forServer) &&
               rules.stream()
                    .allMatch(rule -> rule.matches(client));
    }

    @JsonIgnore
    public final List<Artifact> getMissingArtifacts() {
        return getArtifacts().stream()
                             .filter(Artifact::doesNotExist)
                             .collect(Collectors.toList());
    }

    @JsonIgnore
    public final List<Artifact> getArtifacts() {
        List<Artifact> artifacts = new LinkedList<>();
        downloads.getArtifact()
                 .map(Artifact::getAll)
                 .ifPresent(artifacts::addAll);
        natives.flatMap(natives -> downloads.getClassifiers()
                                            .map(c -> {
                                                String classifier = natives.get(getCurrentOs());
                                                if(classifier != null) return c.get(classifier);
                                                return null;
                                            })
                                            .map(Artifact::getAll))
               .ifPresent(artifacts::addAll);

        return artifacts;
    }

    public final void extractNatives(Path nativesDirectory) {
        natives.ifPresent(natives -> {
            OperatingSystem.OS os = getCurrentOs();
            LOG.trace("{}'s Natives os: {}", name, os);
            var classifier = natives.get(os);
            LOG.trace("  Natives classifier: {}", classifier);
            LOG.trace("  classifiers: {}", downloads.getClassifiers());
            if(classifier != null) {
                downloads.getClassifiers()
                         .ifPresent(classifiers -> {
                             Artifact artifact = classifiers.get(classifier);
                             if(artifact != null) {
                                 String path = artifact.getPath();
                                 Path libraryLocation = ConfigPaths.getLibraryPath()
                                                                   .resolve(path);
                                 extract.ifPresentOrElse(extract -> IO.extractJar(libraryLocation,
                                                                                  nativesDirectory,
                                                                                  extract.get(ExtractionAction.EXCLUDE)
                                                         ),
                                                         () -> IO.extractJar(libraryLocation, nativesDirectory)
                                 );

                             }
                         });
            }
        });
    }

    public String getName() {
        return name;
    }

    public Optional<Map<ExtractionAction, List<String>>> getExtract() {
        return extract;
    }

    public Optional<Map<OperatingSystem.OS, String>> getNatives() {
        return natives;
    }

    public List<LibraryRule> getRules() {
        return rules;
    }

    public LibraryDownloads getDownloads() {
        return downloads;
    }

    public boolean isForClient() {
        return forClient;
    }

    public boolean isForServer() {
        return forServer;
    }

    @Override
    public final int hashCode() {
        return name.hashCode();
    }

    @Override
    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    public final boolean equals(Object o) {
        if(this == o) return true;
        if(o == null) return false;
        Library that = (Library) o;
        return name.equals(that.name);
    }

    @Override
    public String toString() {
        return "[" + name + ": " + natives + " || " + downloads + "]";
    }

    public static Library of(String name, Downloads.Download download, boolean forClient, boolean forServer) {
        Artifact art = Artifact.fromDownloads(name, download);
        LibraryDownloads downloads = new LibraryDownloads(Optional.of(art), Optional.empty());

        return new Library(name, downloads, Optional.empty(), Optional.empty(), List.of(), forClient, forServer);
    }

    public static Library joining(Library a, Library b) {
        if(!a.name.equals(b.name)) throw new IllegalArgumentException("Passed libraries were not equal!");

        var libraryDownloads = new LibraryDownloads(a.downloads.getArtifact()
                                                               .or(() -> b.downloads.getArtifact()),
                                                    a.downloads.getClassifiers()
                                                               .or(() -> b.downloads.getClassifiers())
        );
        var r1 = a.rules;
        var r2 = b.rules;
        var rules = r1.size() > r2.size() ? r1 : r2;
        return new Library(a.name, libraryDownloads, a.extract.or(() -> b.extract), a.natives.or(() -> b.natives), rules, true, true);
    }

    public enum ExtractionAction {
        EXCLUDE;

        @JsonCreator
        public static ExtractionAction fromName(String name) {
            for(ExtractionAction action : values()) {
                if(action.name()
                         .equalsIgnoreCase(name)) return action;
            }
            throw new IllegalArgumentException("Failed to find an extraction action matching name \"" + name + "\"");
        }
    }
}
