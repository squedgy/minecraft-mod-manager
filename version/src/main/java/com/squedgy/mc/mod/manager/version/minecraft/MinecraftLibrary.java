package com.squedgy.mc.mod.manager.version.minecraft;

import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.library.LibraryDownloads;
import com.squedgy.mc.mod.manager.version.library.LibraryRule;
import com.squedgy.mc.mod.manager.version.library.OperatingSystem;
import org.slf4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

public class MinecraftLibrary extends Library {
    private static Logger LOG = getLogger(MinecraftLibrary.class);

    public MinecraftLibrary(String name,
                            LibraryDownloads downloads,
                            Optional<Map<ExtractionAction, List<String>>> extract,
                            Optional<Map<OperatingSystem.OS, String>> natives,
                            Optional<List<LibraryRule>> rules) {
        super(name, downloads, extract, natives, rules.orElse(new LinkedList<>()), true, true);
    }
}
