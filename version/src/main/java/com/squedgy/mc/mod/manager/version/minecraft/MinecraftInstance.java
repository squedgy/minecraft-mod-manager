package com.squedgy.mc.mod.manager.version.minecraft;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.squedgy.mc.mod.manager.auth.format.AuthenticationResponse;
import com.squedgy.mc.mod.manager.configuration.LauncherOptions;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.http.Client;
import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.utils.io.Security;
import com.squedgy.mc.mod.manager.version.json.ClientJson;
import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.json.Post13ClientJson;
import com.squedgy.mc.mod.manager.version.json.Pre13ClientJson;
import com.squedgy.mc.mod.manager.version.library.Artifact;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpRequest;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.squedgy.mc.mod.manager.configuration.LauncherOptions.DEFAULT_HEIGHT;
import static com.squedgy.mc.mod.manager.configuration.LauncherOptions.DEFAULT_WIDTH;
import static com.squedgy.mc.mod.manager.configuration.LauncherOptions.FORGE_VERSION;
import static com.squedgy.mc.mod.manager.configuration.LauncherOptions.MAX_MEMORY;
import static com.squedgy.mc.mod.manager.configuration.LauncherOptions.MIN_MEMORY;
import static com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths.getInstanceDirectory;
import static com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths.getInstanceMinecraftDirectory;
import static com.squedgy.mc.mod.manager.http.Client.enforceRequest;
import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static com.squedgy.mc.mod.manager.version.minecraft.VersionManifest.getVersionManifest;
import static org.slf4j.LoggerFactory.getLogger;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "@class")
@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class MinecraftInstance {
    protected static final String LAUNCHER_JAR_NAME = "launcher.jar";
    private static final Logger LOG = getLogger(MinecraftInstance.class);
    public final ClientJson versionInfo;
    public final Icon icon;
    public String minecraftVersion;
    private String name;

    public MinecraftInstance(String name, String minecraftVersion, Icon icon) throws
            UnknownVersionException,
            InterruptedException,
            URISyntaxException,
            RequestException,
            IOException {
        this(name, minecraftVersion, getMinecraftVersionJsonFile(minecraftVersion), icon);
    }

    @JsonCreator
    public MinecraftInstance(String name, String minecraftVersion, ClientJson versionInfo, Icon icon) {
        setName(name);
        setMinecraftVersion(minecraftVersion);
        if (versionInfo == null) throw new IllegalArgumentException("Version info cannot be null!");
        this.versionInfo = versionInfo;
        if (icon == null) icon = ImageIcon.DEFAULT_ICON;
        this.icon = icon;
    }

    public static ClientJson getMinecraftVersionJsonFile(String minecraftVersion) throws
            UnknownVersionException,
            InterruptedException,
            URISyntaxException,
            RequestException,
            IOException {
        var clientJson = ConfigPaths.getVersionJsonFilePath(minecraftVersion);
        if (clientJson.toFile()
                      .exists()) {
            LOG.trace("Found existing client json for version {} at {}", minecraftVersion, clientJson);
            return objectMapper().readValue(clientJson.toFile(), ClientJson.class);
        }
        var info = downloadMinecraftVersionJsonFile(minecraftVersion);
        var parent = clientJson.getParent()
                               .toFile();
        if (!parent.exists() && !parent.mkdirs() && !parent.exists())
            throw new IOException("Failed to created version json's parent directory: " + parent);
        try {
            objectMapper().writeValue(clientJson.toFile(), info);
        } catch (IOException e) {

            if (clientJson.toFile()
                          .exists()) clientJson.toFile()
                                               .delete();
            throw e;
        }
        return info;
    }

    protected void setMinecraftVersion(String minecraftVersion) {
        if (minecraftVersion == null || minecraftVersion.isEmpty())
            throw new IllegalArgumentException("Passed minecraft version cannot be empty!");
        this.minecraftVersion = minecraftVersion;
    }

    private static ClientJson downloadMinecraftVersionJsonFile(String minecraftVersion) throws
            UnknownVersionException,
            InterruptedException,
            URISyntaxException,
            RequestException,
            IOException {

        VersionManifest manifest;
        try {
            manifest = getVersionManifest();
            LOG.trace("Retrieved the version manifest.");
        } catch (IOException | InterruptedException | URISyntaxException ex) {
            throw new IOException("Failed to retrieve the version manifest.", ex);
        }
        VersionManifest.Version v = manifest.versions.stream()
                                                     .filter(f -> f.id.equals(minecraftVersion))
                                                     .findFirst()
                                                     .orElseThrow(() -> new UnknownVersionException(
                                                             "Failed to find minecraft version: " +
                                                                     minecraftVersion));

        LOG.trace("Retrieving version json file.");
        HttpRequest fileRequest = HttpRequest.newBuilder()
                                             .uri(URI.create(v.url))
                                             .build();

        Class<? extends Post13ClientJson> parseTo = isPost13VersionJson(v) ? Post13ClientJson.class : Pre13ClientJson.class;
        LOG.trace("Retrieving version json for version {} into {}", minecraftVersion, parseTo.getSimpleName());

        Path temp = Files.createTempFile(null, ".json");
        Files.copy(enforceRequest(fileRequest).body(), temp, StandardCopyOption.REPLACE_EXISTING);

        var ret = objectMapper().readValue(temp.toFile(), parseTo);

        LOG.trace("TEMP JSON {}", temp);

        ret.libraries.forEach(library -> LOG.trace("{} contains: {}", minecraftVersion, library));
        return ret;
    }

    private static boolean isPost13VersionJson(VersionManifest.Version version) {
        // This is the release time of the snapshot that introduced the post13 version json.
        // I don't think this should cause any issues, but it could ¯\_(ツ)_/¯
        return version.releaseTime.compareTo(ZonedDateTime.parse("2017-10-25T14:43:50+00:00")) >= 0;
    }

    public final void start(AuthenticationResponse auth) throws IOException, NoSuchAlgorithmException {
        LOG.info("Attempting to start minecraft instance {}", getName());
        retrieveNatives();
        enforceDirectory();
        enforceLauncherJar();
        List<String> command = new LinkedList<>();
        command.add(LauncherOptions.getJavaExecutableString());
        command.add("-jar");
        command.add(getLauncherJar().toString());
        command.add(versionInfo.buildLaunchCommand());
        getDefaultEnvironmentVariables(auth).forEach((k, v) -> command.add("--env=" + k + '=' + v));
        getExtraEnvironmentVariables(auth).forEach((k, v) -> command.add("--env=" + k + "=" + v));
        command.add(ConfigPaths.getResourcesDirectory(getName())
                               .toString());
        command.add(ConfigPaths.getNativesDirectory(getName())
                               .toString());

        Path instanceLogs = ConfigPaths.getInstanceLogs(getName());
        Path latestLog = instanceLogs.resolve("latest.log");
        if (!instanceLogs.toFile()
                         .exists() &&
                !instanceLogs.toFile()
                             .mkdirs()) throw new IOException("Failed to created log directory: " + instanceLogs);
        if (latestLog.toFile()
                     .exists()) Files.delete(latestLog);
        if (!latestLog.toFile()
                      .createNewFile()) throw new IOException("Failed to create log file: " + latestLog);

        ProcessBuilder.Redirect redirection = ProcessBuilder.Redirect.appendTo(latestLog.toFile());

        new ProcessBuilder().command(command)
                            .redirectOutput(redirection)
                            .redirectErrorStream(true)
                            .start();
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("Instance cannot have empty name!");
        this.name = name;
    }

    public void retrieveNatives() throws UncheckedIOException {
        var path = ConfigPaths.getNativesDirectory(getName());
        versionInfo.libraries.forEach(library -> library.extractNatives(path));
    }

    public final void enforceDirectory() throws IOException {
        File dir = getInstanceDir().toFile();
        File mcDir = getInstanceMinecraftDirectory(getName()).toFile();
        if (!dir.exists() && !dir.mkdirs() && !dir.exists())
            throw new IOException("Failed to create instances directory \"" + dir + "\"");
        if (!mcDir.exists() && !mcDir.mkdirs() && !mcDir.exists())
            throw new IOException("Failed to create instance's minecraft directory \"" + mcDir + "\"");
    }

    private void enforceLauncherJar() throws IOException, NoSuchAlgorithmException {
        Path launcherJar = getLauncherJar();
        URL newestUrl = MinecraftInstance.class.getResource("resources/" + LAUNCHER_JAR_NAME);
        LOG.trace("Newest launcher jar url: {}", newestUrl);
        if (!launcherJar.toFile()
                        .exists()) {
            if (!launcherJar.getParent()
                            .toFile()
                            .exists() &&
                    !launcherJar.getParent()
                                .toFile()
                                .mkdirs()) {
                throw new IOException("Failed to create launcher jar's parent directory: " + launcherJar.getParent());
            }
            Files.copy(newestUrl.openStream(), launcherJar);
        } else {
            byte[] actualSha = Security.getSha1(newestUrl);
            byte[] liveSha = Security.getSha1(launcherJar);
            LOG.trace("ACTUAL: {}", actualSha);
            LOG.trace("LIVING: {}", liveSha);
            if (!Security.shasEqual(actualSha, liveSha)) {
                LOG.info("Sha for current and new launcher jar didn't match, recreating.");
                Files.copy(newestUrl.openStream(), launcherJar, StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }

    private Path getLauncherJar() {
        return ConfigPaths.getJarsPath()
                          .resolve(LAUNCHER_JAR_NAME);
    }

    private Map<String, String> getDefaultEnvironmentVariables(AuthenticationResponse auth) {
        var forgeVersion = LauncherOptions.getStringProperty(FORGE_VERSION);
        var minMem = LauncherOptions.getStringProperty(MIN_MEMORY);
        var maxMem = LauncherOptions.getStringProperty(MAX_MEMORY);
        var defaultWidth = LauncherOptions.getStringProperty(DEFAULT_WIDTH);
        var defaultHeight = LauncherOptions.getStringProperty(DEFAULT_HEIGHT);
        var logPath = ConfigPaths.getLogConfiguration(versionInfo.logging.flatMap(e -> e.client.map(c -> c.file.id))
                                                                         .orElse(null))
                                 .toString();

        return Map.ofEntries(
                entry("auth_uuid", auth.selectedProfile.id),
                entry("auth_access_token", auth.accessToken),
                entry("auth_player_name", auth.selectedProfile.name.replace(" ", "\\ ")),
                entry("auth_session", auth.accessToken),
                entry(
                        "game_directory",
                        (ConfigPaths.getInstanceMinecraftDirectory(getName())
                                    .toString())
                ),
                entry("version_name", minecraftVersion),
                entry("forge_version", forgeVersion.orElse("")),
                entry("def_width", defaultWidth.orElse("1024")),
                entry("def_height", defaultHeight.orElse("768")),
                entry("max_mem", maxMem.orElse("1024m")),
                entry("min_mem", minMem.orElse("768m")),
                entry("user_type", "mojang"),
                entry("version_type", versionInfo.type),
                entry(
                        "assets_root",
                        (ConfigPaths.getAssetDirectory()
                                    .toString())
                ),
                entry(
                        "game_assets",
                        (ConfigPaths.getAssetDirectory()
                                    .toString())
                ),
                entry("assets_index_name", versionInfo.assets),
                entry(
                        "natives_directory",
                        (ConfigPaths.getNativesDirectory(getName())
                                    .toString())
                ),
                entry("log_path", logPath),
                entry("instance_name", (getName())),
                entry(
                        "logging_dir",
                        (ConfigPaths.getMinecraftLogs()
                                    .toString())
                ),
                entry("classpath", (versionInfo.getClassPath(this)))
        );
    }

    protected Map<String, String> getExtraEnvironmentVariables(AuthenticationResponse auth) {
        return Map.of();
    }

    @JsonIgnore
    protected final Path getInstanceDir() {
        return getInstanceDirectory(this.getName());
    }

    protected static <K, V> Map.Entry<K, V> entry(K key, V value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    @JsonIgnore
    public String getVersion() {
        return minecraftVersion;
    }

    /**
     * If there are any tasks to do after downloading and installing missing artifacts/assets return
     * them as Runnables here.
     * <p>
     * Good examples are:
     * <ul>
     *     <li>Merging artifacts</li>
     *     <li>Post artifact install actions (Forge processors)</li>
     *     <li>Installing further non-marked deps (Forge installer jars)</li>
     *     <li>etc.</li>
     * </ul>
     *
     * @return actions that require artifacts/assets to be downloaded before they can run
     */
    public List<Runnable> afterDownloads() {
        return List.of();
    }

    @JsonIgnore
    public List<Artifact> getMissingArtifacts() {
        return versionInfo.libraries.stream()
                                    .map(Library::getMissingArtifacts)
                                    .flatMap(Collection::stream)
                                    .collect(Collectors.toList());
    }

    public void enforceLogging() {
        LOG.info("Enforcing logging");
        if (versionInfo.logging.flatMap(c -> c.client)
                               .isPresent()) {
            var logFile = ConfigPaths.getLogConfiguration(versionInfo.logging.get().client.map(v -> v.file.id)
                                                                                          .orElse(null));
            if (!logFile.toFile()
                        .exists()) {
                try {
                    var request = Client.get(versionInfo.logging.get().client.map(c -> c.file.url)
                                                                             .orElse(null));
                    var response = enforceRequest(request.build());
                    var body = new String(response.body()
                                                  .readAllBytes());
                    var minecraftLogs = "${sys:squedgy.logging.dir}";
                    body = body.replace(
                            "filePattern=\"logs/",
                            "filePattern=\"" + minecraftLogs + "/${sys:squedgy.instance.name:-mc-mod-man}-"
                    )
                               .replace(
                                       "fileName=\"logs/latest.log",
                                       "fileName=\"" + minecraftLogs + "/${sys:squedgy.instance.name:-mc-mod-man}-latest.log"
                               )
                               .replace(
                                       "<LegacyXMLLayout />",
                                       "<PatternLayout pattern=\"[%d{HH:mm:ss}] [%-20t/%5level]: %msg%n\" />"
                               );
                    Files.writeString(logFile, body);
                } catch (InterruptedException | IOException | URISyntaxException | RequestException e) {
                    LOG.error("Failed to download log configuration file.", e);
                    throw new RuntimeException(e);
                }
            }
        } else {
            var logFile = ConfigPaths.getLogConfiguration(null);
            if (!logFile.toFile()
                        .exists()) {
                try {
                    var file = MinecraftInstance.class.getResource("log4j2.xml")
                                                      .openStream();
                    Files.copy(file, logFile, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    LOG.error("Failed to read resource: log4j2.xml", e);
                    throw new RuntimeException(e);
                }
            }
        }
        LOG.info("Done enforcing logging");
    }

    public List<Path> instanceClassPath(boolean client) {
        return Collections.emptyList();
    }

    @Override
    public String toString() {
        return name + "@MC" + minecraftVersion;
    }
}
