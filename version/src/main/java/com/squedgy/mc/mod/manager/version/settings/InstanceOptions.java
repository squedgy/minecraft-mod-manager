package com.squedgy.mc.mod.manager.version.settings;

import javafx.scene.Node;

public interface InstanceOptions {

    /**
     * If this is NOT the instances primary display, how should it's sub-heading be labeled
     * @return
     */
    String getSubTitle();

    /**
     * Provides the caller with any body of the side bar for it's instances implementations. This should provide access to any
     * settings update modals/screens that would be available for an instance using these options
     * @return
     */
    Node getBody();
}
