package com.squedgy.mc.mod.manager.version;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.TextNode;
import com.squedgy.mc.mod.manager.configuration.LauncherOptions;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.List.of;
import static org.slf4j.LoggerFactory.getLogger;

public final class Arguments {
    private static Logger LOG = getLogger(Arguments.class);
    private final ArrayNode game;
    private final ArrayNode jvm;

    @JsonCreator
    public Arguments(ArrayNode game, ArrayNode jvm) {
        this.game = game;
        this.jvm = jvm;
    }

    public ArrayNode getGame() {
        return game;
    }

    public ArrayNode getJvm() {
        return jvm;
    }

    public Collection<String> parseGameArgs() {
        return getArgsForArray(game);
    }

    private Collection<String> getArgsForArray(ArrayNode root) {
        if(root == null) return of();
        LOG.info("SIZE: {}\n\t{}", root.size(), root);
        ArrayList<String> args = new ArrayList<>(root.size());
        for(int i = 0; i < root.size(); i++) {
            JsonNode child = root.get(i);

            if(child.isObject()) {
                ArrayNode rules = (ArrayNode) child.get("rules");
                AtomicBoolean followsRules = new AtomicBoolean(true);
                rules.forEach(rule -> {
                    if(!followsRules.get()) return;
                    String action = rule.get("action")
                                        .textValue();
                    boolean allow = action.equals("allow");
                    boolean matches = true;
                    if(rule.has("features")) {
                        Iterator<String> fields = rule.get("features")
                                                      .fieldNames();
                        while(matches && fields.hasNext()) {
                            String field = fields.next();
                            JsonNode feature = rule.get("features")
                                                   .get(field);
                            matches = feature.equals(LauncherOptions.getPropertyOrDefault("features." + field, BooleanNode.FALSE));
                        }
                    }
                    if(matches && rule.has("os")) {
                        String requestedOs = tryGetValue(rule, JsonNode::textValue, "os", "name");
                        String requestedArch = tryGetValue(rule, JsonNode::textValue, "os", "arch");
                        String versionPattern = tryGetValue(rule, JsonNode::textValue, "os", "version");

                        String os = getOsName();
                        String arch = System.getProperty("os.arch");
                        String version = getOsVersion();

                        matches = ((requestedOs == null || requestedOs.equals(os)) &&
                                   (versionPattern == null || version.matches(versionPattern)) ||
                                   (requestedArch == null || requestedArch.equals(arch)));
                    }
                    followsRules.set(allow == matches); // if((!allow && matches) || (allow && !matches)) followsRules.set(false);
                });
            } else if(child.isTextual()) {
                args.add(child.textValue());
            }
        }

        return args.stream()
                   .filter(Objects::nonNull)
                   .collect(Collectors.toUnmodifiableList());
    }

    public static <V> V tryGetValue(JsonNode from, Function<JsonNode, V> value, String... keys) {
        JsonNode currentNode = from;
        for(String key : keys) {
            if(currentNode.has(key)) {
                currentNode = currentNode.get(key);
            } else {
                return null;
            }
        }
        return value.apply(currentNode);
    }

    public static String getOsName() {
        String osName = System.getProperty("os.name")
                              .toLowerCase();

        if(osName.contains("win")) return "windows";
        if(osName.contains("mac")) return "osx";
        if(osName.contains("nux")) return "linux";
        return null;
    }

    public static String getOsVersion() {
        return System.getProperty("os.version");
    }

    public Collection<String> parseJvmArgs() {
        return getArgsForArray(jvm);
    }

    public static Arguments joining(Arguments one, Arguments two) {
        if(one == null) return two;
        if(two == null) return one;

        ArrayNode game = JsonNodeFactory.instance.arrayNode();
        ArrayNode jvm = JsonNodeFactory.instance.arrayNode();
        if(one.game != null) game.addAll(one.game);
        if(two.game != null) game.addAll(two.game);
        if(one.jvm != null) jvm.addAll(one.jvm);
        if(two.jvm != null) jvm.addAll(two.jvm);
        return new Arguments(game, jvm);
    }

    public static Arguments fromMcArgs(String minecraftArguments) {
        return new Arguments(toArrayNode(minecraftArguments), defaultJvmArgs());
    }

    private static ArrayNode toArrayNode(String mcArgs) {
        ArrayNode node = JsonNodeFactory.instance.arrayNode();
        for(String arg : mcArgs.split(" ")) node.add(new TextNode(arg));
        return node;
    }

    private static ArrayNode defaultJvmArgs() {
        ArrayNode node = JsonNodeFactory.instance.arrayNode();
        List.of("-Djava.library.path=$natives_directory", "-Xms$min_mem", "-Xmx$max_mem", "-cp", "$classpath")
            .stream()
            .map(TextNode::new)
            .forEach(node::add);
        return node;
    }
}
