package com.squedgy.mc.mod.manager.version.settings;

import javafx.scene.Node;

public interface InstanceTitle {
    /**
     * Provides a Node to display as a MinecraftInstance's settings panel title
     * @return
     */
    Node getTitle();
}
