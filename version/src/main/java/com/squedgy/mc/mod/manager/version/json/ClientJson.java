package com.squedgy.mc.mod.manager.version.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.squedgy.mc.mod.manager.configuration.LauncherOptions;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.version.Arguments;
import com.squedgy.mc.mod.manager.version.AssetIndex;
import com.squedgy.mc.mod.manager.version.LoggingInfo;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import org.slf4j.Logger;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@JsonInclude(content = JsonInclude.Include.NON_NULL)
public class ClientJson {
    private static final Logger LOG = getLogger(ClientJson.class);

    public final String assets, id, mainClass, type;
    public final AssetIndex assetIndex;
    public final Collection<Library> libraries;
    public final String releaseTime, time;
    public final Optional<LoggingInfo> logging;
    public final Arguments arguments;

    @JsonCreator
    public ClientJson(String id,
                      String mainClass,
                      String type,
                      String assets,
                      String releaseTime,
                      String time,
                      AssetIndex assetIndex,
                      Collection<Library> libraries,
                      Optional<LoggingInfo> logging,
                      Arguments arguments) {
        LOG.debug("LOGGING: {}", logging);
        this.assets = assets;
        this.id = id;
        this.mainClass = mainClass;
        this.type = type;
        this.assetIndex = assetIndex;
        this.libraries = libraries;
        this.releaseTime = releaseTime;
        this.time = time;
        this.logging = logging;
        this.arguments = arguments;
    }

    @JsonIgnore
    public final String getJarId() {
        return id;
    }

    @JsonIgnore
    public final String getClassPath(MinecraftInstance parent) {
        var baseLibraryPath = ConfigPaths.getLibraryPath()
                                         .toString();
        List<String> libraryPaths = new LinkedList<>();

        libraries.stream()
                 .filter(l -> l.isNeeded(true))
                 .forEach(library -> {
                     var artifacts = library.getArtifacts()
                                            .stream()
                                            .filter(Objects::nonNull)
                                            .map(artifact -> baseLibraryPath + File.separatorChar + artifact.getPath())
                                            .reduce((a, b) -> a + File.pathSeparator + b)
                                            .orElse("");

                     libraryPaths.add(artifacts);
                 });

        parent.instanceClassPath(true)
              .forEach(p -> libraryPaths.add(p.toString()));

        return libraryPaths.stream()
                           .reduce((a, b) -> a + File.pathSeparator + b)
                           .orElse("")
                           .replace(" ", "\\ ");
    }

    public String buildLaunchCommand() {

        StringBuilder builder = new StringBuilder(LauncherOptions.getJavaExecutableString()
                                                                 .replace(" ", "\\ ") + " ");

        getRequiredJvmArgs().forEach(arg -> {
            LOG.trace("Adding required Jvm arg {}", arg);
            builder.append(quote(arg))
                   .append(' ');
        });
        arguments.parseJvmArgs()
                 .forEach(arg -> {
                     LOG.trace("Adding jvm arg {}", arg);
                     builder.append(quote(arg))
                            .append(' ');
                 });
        LOG.trace("Adding main class {}", quote(mainClass));
        builder.append(quote(mainClass))
               .append(' ');
        arguments.parseGameArgs()
                 .forEach(arg -> {
                     LOG.trace("Adding game arg {}", arg);
                     builder.append(quote(arg))
                            .append(' ');
                 });

        return builder.toString();
    }

    @JsonIgnore
    public final List<String> getRequiredJvmArgs() {
        return List.of(
            ("-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump"),
            ("-Dlog4j.configurationFile=$log_path"),
            ("-Dsquedgy.instance.name=$instance_name"),
            ("-Dsquedgy.logging.dir=$logging_dir")
        );
    }

    private static String quote(String string) {
        return '"' + string.replace("\"", "\\\"") + '"';
    }

}
