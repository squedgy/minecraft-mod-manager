package com.squedgy.mc.mod.manager.version.mods;

import java.util.Arrays;
import java.util.Objects;

public abstract class ModVersions {

    /**
     * Kind of a fuzzy match, matches on the major and minor versions, not the patch version
     *
     * Ex. 1.12 == 1.12.2 | 1.12.2 == 1.12.2 | 1.12 != 1.13
     *
     * @param minecraftVersion
     * @param modMcVersion
     *
     * @return if the versions match
     */
    public static boolean matchesVersion(String minecraftVersion, String modMcVersion) {
        Integer[] mcv = getVersions(minecraftVersion), mmcv = getVersions(modMcVersion);

        // 1.12 should equal 1.12.2 (in most cases you can use them, so *shrug*)
        return Objects.equals(mcv[0], mmcv[0]) && Objects.equals(mcv[1], mmcv[1]);
    }

    public static Integer[] getVersions(String modVersion) {
        return Arrays.stream(modVersion.split("\\."))
                     .map(Integer::parseInt)
                     .toArray(Integer[]::new);
    }

    /**
     * An absolute match. matches on the major, minor and patch versions
     *
     * Ex. 1.12 != 1.12.2 | 1.12.2 == 1.12.2 | 1.12 != 1.13
     *
     * @param minecraftVersion
     * @param modMcVersion
     *
     * @return if the versions match
     */
    public static boolean absoluteMatchesVersion(String minecraftVersion, String modMcVersion) {
        Integer[] mcv = getVersions(minecraftVersion), mmcv = getVersions(modMcVersion);

        return Objects.equals(mcv[0], mmcv[0]) &&
               Objects.equals(mcv[1], mmcv[1]) &&
               (mcv.length != 3 || mmcv.length != 3 || Objects.equals(mcv[2], mmcv[2]));
    }

}
