package com.squedgy.mc.mod.manager.version.mods;

import com.squedgy.mc.mod.manager.configuration.http.DownloadUriConfiguration;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.stream.Stream;

public class ModMetadata {
    private final String fileName, formattedName;
    private final Collection<String> mcVersions;
    private final URI downloadURI;

    public ModMetadata(String fileName, String formattedName, URI downloadURI, Collection<String> mcVersions) {
        this.fileName = fileName;
        this.formattedName = formattedName;
        this.downloadURI = downloadURI;
        this.mcVersions = mcVersions;
    }

    /**
     * This function should return the file name that the jar file retrieved had
     *
     * @return a string
     */
    public String getFileName() {

        return fileName;
    }

    public ModMetadata withFileName(String fileName) {
        return new ModMetadata(fileName, formattedName, downloadURI, mcVersions);
    }

    public ModMetadata withDownloadUri(URI downloadURI) {
        return new ModMetadata(fileName, formattedName, downloadURI, mcVersions);
    }

    /**
     * Any string of minecraft versions which this mod is known to be used with.
     *
     * @return string
     */
    public Collection<String> getEffectiveMinecraftVersions() {
        return mcVersions;
    }

    public ModMetadata withEffectiveVersions(Collection<String> mcVersions) {
        return new ModMetadata(fileName, formattedName, downloadURI, mcVersions);
    }

    /**
     * This should be the formatted name the retriever understands
     *
     * @return a string
     */
    public String getFormattedName() {
        return formattedName;
    }

    public ModMetadata withFormattedName(String formattedName) {
        return new ModMetadata(fileName, formattedName, downloadURI, mcVersions);
    }

    public String toString() {
        return String.format("%s(%s)<%s> [%s]", formattedName, fileName, mcVersions, downloadURI);
    }

    public static Stream<String> getModDownloadUris(ModMetadata metadata) throws IOException {
        return DownloadUriConfiguration.getUriReplacements()
                                       .stream()
                                       .map(replacement -> replacement.getModifiedDownloadUri(metadata.getDownloadUri()
                                                                                                      .toString()));
    }

    /**
     * The URI from which this file was retrieved from
     *
     * @return a URI
     */
    public URI getDownloadUri() {
        return downloadURI;
    }
}
