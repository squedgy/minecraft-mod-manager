module com.squedgy.mc.mod.manager.version {
    exports com.squedgy.mc.mod.manager.version.mods;
    exports com.squedgy.mc.mod.manager.version.minecraft;
    exports com.squedgy.mc.mod.manager.version.loaders;
    exports com.squedgy.mc.mod.manager.version;
    exports com.squedgy.mc.mod.manager.version.json;
    exports com.squedgy.mc.mod.manager.version.library;

    opens com.squedgy.mc.mod.manager.version.minecraft to com.fasterxml.jackson.databind;
    opens com.squedgy.mc.mod.manager.version.loaders to com.fasterxml.jackson.databind;
    opens com.squedgy.mc.mod.manager.version.library to com.fasterxml.jackson.databind;
    opens com.squedgy.mc.mod.manager.version.json to com.fasterxml.jackson.databind;
    opens com.squedgy.mc.mod.manager.version to com.fasterxml.jackson.databind;

    requires org.slf4j;
    requires com.squedgy.mc.mod.manager.configuration;
    requires com.squedgy.mc.mod.manager.http;
    requires transitive com.squedgy.mc.mod.manager.utils;
    requires transitive com.squedgy.mc.mod.manager.auth;
    requires transitive javafx.graphics;
}