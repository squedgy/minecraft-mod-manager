package com.squedgy.mc.mod.manager.version.json;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;

public class NewPost13ClientJsonTest {

    @Test
    public void parses_pre_minecraft_13_client_json() throws IOException {
        Pre13ClientJson file = objectMapper().readValue(getClass().getResourceAsStream("json.json"), Pre13ClientJson.class);
    }

    @Test
    public void parses_post_minecraft_13_client_json() throws IOException {
        Post13ClientJson file = objectMapper().readValue(getClass().getResourceAsStream("new-json.json"), Post13ClientJson.class);
    }

}
