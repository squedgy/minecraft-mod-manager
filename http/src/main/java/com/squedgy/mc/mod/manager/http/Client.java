package com.squedgy.mc.mod.manager.http;

import com.squedgy.mc.mod.manager.utils.FormattedRuntimeException;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static java.net.http.HttpRequest.BodyPublishers.noBody;
import static org.slf4j.LoggerFactory.getLogger;

public abstract class Client {
    private static final Logger LOG = getLogger(Client.class);

    /**
     * Downloads the result of a request to a temp file using {@link #enforceRequest(HttpRequest, int...)}
     *
     * @param url                the url to download
     * @param postfix            the file's extension
     * @param validResponseCodes response codes you're expecting
     *
     * @return the path to downloaded the temporary file
     *
     * @throws IOException          If there's an IO exception while sending or retrieving
     * @throws InterruptedException If the request is interrupted
     * @throws URISyntaxException   If a redirect fails to build a valid URI
     * @throws RequestException     if the requested resource returns a status code that wasn't expected
     */
    public static Path download(String url, String postfix, int... validResponseCodes) throws
                                                                                       InterruptedException,
                                                                                       URISyntaxException,
                                                                                       RequestException,
                                                                                       IOException {
        return download(get(url).build(), postfix, validResponseCodes);
    }

    /**
     * {@link #download(String, String, int...)} uses this method. see it's description for more details
     *
     * @param request            the url to download
     * @param postfix            the file's extension
     * @param validResponseCodes response codes you're expecting
     *
     * @return the path to the downloaded temporary file
     *
     * @throws IOException          If there's an IO exception while sending or retrieving
     * @throws InterruptedException If the request is interrupted
     * @throws URISyntaxException   If a redirect fails to build a valid URI
     * @throws RequestException     if the requested resource returns a status code that wasn't expected
     */
    public static Path download(HttpRequest request, String postfix, int... validResponseCodes) throws
                                                                                                IOException,
                                                                                                InterruptedException,
                                                                                                URISyntaxException,
                                                                                                RequestException {
        Path tempFile = Files.createTempFile(null, postfix);
        HttpResponse<InputStream> response = enforceRequest(request, validResponseCodes);
        try(InputStream stream = response.body()) {
            Files.copy(stream, tempFile, StandardCopyOption.REPLACE_EXISTING);
        }
        return tempFile;
    }

    /**
     * Created a Request builder with the given string as the request uri.
     * uses {@link URI#create(String)} and {@link #getDefaultHttpRequestBuilder(URI)}
     *
     * @param uri a uri
     *
     * @return an HttpRequest Builder
     *
     * @throws NullPointerException     if {@code uri} is {@code null}
     * @throws IllegalArgumentException if {@code uri} is not RFC&nbsp;2396 compliant
     */
    public static HttpRequest.Builder get(String uri) {
        try {
            return get(URI.create(uri));
        } catch(IllegalArgumentException ex) {
            throw new FormattedRuntimeException("Given uri \"{}\" was ill formatted", uri, ex);
        }
    }

    /**
     * If no valid response codes are passed it's assumed you want a 200
     *
     * this method uses {@link #sendRequest(HttpRequest)} under the hood and contains all caveats that come with that.
     *
     * @param request            the request to send
     * @param validResponseCodes all status codes you will handle
     *
     * @return the result of
     *
     * @throws IOException          If there's an IO exception while sending or retrieving
     * @throws InterruptedException If the request is interrupted
     * @throws URISyntaxException   If a redirect fails to build a valid URI
     * @throws RequestException     if the requested resource returns a status code that wasn't expected
     */
    public static HttpResponse<InputStream> enforceRequest(HttpRequest request, int... validResponseCodes) throws
                                                                                                           RequestException,
                                                                                                           InterruptedException,
                                                                                                           IOException,
                                                                                                           URISyntaxException {
        int[] allowedCodes = validResponseCodes.length == 0 ? new int[]{200} : validResponseCodes;
        var response = sendRequest(request);
        int status = response.statusCode();
        for(int validCode : allowedCodes) if(validCode == status) return response;
        throw new RequestException("Received a {} status code for request [{}] and only {} are expected", status, request, allowedCodes);
    }

    /**
     * Created a Request builder with the given uri as the request uri
     *
     * @param uri a uri
     *
     * @return an HttpRequest Builder
     */
    public static HttpRequest.Builder get(URI uri) {
        return getDefaultHttpRequestBuilder(uri).GET();
    }

    /**
     * Send an http request and follows redirects, should handle excess cases of double encoding
     *
     * I'm looking at you pam's harvest craft...
     *
     * @param request an Http request
     *
     * @return the response
     *
     * @throws IOException          If there's an IO exception while sending or retrieving
     * @throws InterruptedException If the request is interrupted
     * @throws URISyntaxException   If a redirect fails to build a valid URI
     */
    public static HttpResponse<InputStream> sendRequest(HttpRequest request) throws IOException, InterruptedException, URISyntaxException {
        HttpClient client = getClient();
        LOG.trace("Sending request {}", request);
        HttpResponse<InputStream> response = client.send(request, HttpResponse.BodyHandlers.ofInputStream());
        while(response.statusCode() >= 300 && response.statusCode() <= 399) {
            HttpRequest r = getHttpRequest(
                request,
                buildURI(response.headers()
                                 .map()
                                 .get("Location")
                                 .get(0))
            );
            LOG.trace("Request returned redirect, following to {}", r);
            response = client.send(r, HttpResponse.BodyHandlers.ofInputStream());
        }

        LOG.trace("status: {}", response.statusCode());
        return response;
    }

    private static HttpRequest.Builder getDefaultHttpRequestBuilder(URI uri) {
        return HttpRequest.newBuilder()
                          .uri(uri)
                          .headers("Accept", "*/*", "Accept-Language", "en-US,en;q=0.5", "DNT", "1", "Upgrade-Insecure-Requests", "1");
    }

    private static HttpClient getClient() {
        return HttpClient.newBuilder()
                         .followRedirects(HttpClient.Redirect.NEVER)
                         .build();
    }

    private static HttpRequest getHttpRequest(HttpRequest basedOn, URI uri) {
        var builder = getDefaultHttpRequestBuilder(uri).method(
            basedOn.method(),
            basedOn.bodyPublisher()
                   .orElse(noBody())
        );

        basedOn.headers()
               .map()
               .forEach((k, v) -> v.forEach(header -> builder.header(k, header)));

        return builder.build();
    }

    private static URI buildURI(String url) throws URISyntaxException {
        int i = url.indexOf("://");
        String scheme = url.substring(0, i);
        int schemeOffset = i + "://".length();
        int slash = url.indexOf('/', schemeOffset);
        String host = url.substring(schemeOffset, slash);
        String path = url.substring(slash);
        // Things like Pam's Harvest Craft don't work without the following...
        // The first one encodes
        URI intermediate = new URI(scheme, host, path, null);
        // the second fixes any encoding encoded item issues
        String inbetween = intermediate.toString()
                                       .replaceAll("%25([2-9A-Fa-f]{2})", "%$1");
        URI ret = new URI(inbetween);
        // Trace the result for safety
        LOG.trace("built url \"{}\"-> URI \"{}\" -> String \"{}\" to \"{}\"", url, intermediate, inbetween, ret);
        return ret;
    }

    /**
     * {@link #get(String)} as this method is exactly that, but for HEAD requests.
     *
     * @param uri a string representing a URI
     *
     * @return an HttpRequest Builder that's ready to be build
     *
     * @throws NullPointerException     if {@code uri} is {@code null}
     * @throws IllegalArgumentException if {@code uri} is not RFC&nbsp;2396 compliant
     */
    public static HttpRequest.Builder head(String uri) {
        return head(URI.create(uri));
    }

    /**
     * {@link #get(URI)} except returns a HEAD request
     *
     * @param uri a URI to request
     *
     * @return an HttpRequest builder with certain default defined
     */
    public static HttpRequest.Builder head(URI uri) {
        return getDefaultHttpRequestBuilder(uri).method("HEAD", noBody());
    }

}
