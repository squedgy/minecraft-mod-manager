package com.squedgy.mc.mod.manager.http;

import com.squedgy.mc.mod.manager.utils.FormattedException;

public class RequestException extends FormattedException {

    public RequestException(String message) {
        super(message);
    }

    public RequestException(String message, Object... args) {
        super(message, args);
    }

    public RequestException(String message, Exception ex) {
        super(message, ex);
    }
}
