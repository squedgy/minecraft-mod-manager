module com.squedgy.mc.mod.manager.http {
    exports com.squedgy.mc.mod.manager.http;

    requires java.base;
    requires transitive java.net.http;
    requires org.slf4j;
    requires transitive com.fasterxml.jackson.databind;
    requires jdk.jsobject;
    requires jdk.crypto.ec;
    requires com.squedgy.mc.mod.manager.configuration;
    requires com.squedgy.mc.mod.manager.utils;
}