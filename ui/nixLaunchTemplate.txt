#!/bin/sh
DIR="\${0%/*}"
# -Dlogback.configurationFile="\$DIR/logback.xml"
"\$DIR/java" --add-modules ALL-MODULE-PATH ${jvmArgs} -p "\$DIR/../app" -m ${moduleName}/${mainClassName} ${args} "\$@"