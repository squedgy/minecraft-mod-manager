package com.squedgy.mc.mod.manager.ui.controller;

import com.squedgy.mc.mod.manager.auth.format.AuthenticationResponse;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.ui.App;
import com.squedgy.mc.mod.manager.ui.element.ThemedElement;
import com.squedgy.mc.mod.manager.utils.threading.Work;
import com.squedgy.mc.mod.manager.utils.threading.Work.OrderedTask;
import com.squedgy.mc.mod.manager.version.AssetIndex;
import com.squedgy.mc.mod.manager.version.json.ClientJson;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;
import static org.slf4j.LoggerFactory.getLogger;

public class LaunchMinecraft extends ThemedElement {
    private static final Logger LOG = getLogger(LaunchMinecraft.class);
    private final MinecraftInstance toLaunch;
    private final AuthenticationResponse authentication;
    private final AssetIndex.AssetsMap assetsMap;

    @FXML private ProgressBar progress;
    @FXML private Label label;

    private final List<Throwable> failures = new LinkedList<>();
    private final long totalSize;
    private volatile long finished = 0;

    public LaunchMinecraft(AuthenticationResponse auth, MinecraftInstance toLaunch) throws IOException {
        super(LaunchMinecraft.class.getResource("launch-minecraft.fxml"));
        this.authentication = auth;
        this.toLaunch = toLaunch;
        ClientJson clientJson = toLaunch.versionInfo;
        clientJson.assetIndex.enforceAssetsIndexFile();
        assetsMap = clientJson.assetIndex.getAssetIndexFile();
        Collection<OrderedTask> assetRequests = toLaunch.getMissingArtifacts()
                                .stream()
                                .map(a -> new LaunchRunnable(a::download))
                                .collect(toCollection(LinkedList::new));
        assetsMap.getMissingAssets()
                 .forEach(a -> assetRequests.add(new LaunchRunnable(a::download)));

        assetRequests.add(new LaunchRunnable(toLaunch::enforceLogging, 1));

        totalSize = assetRequests.size();
        progress.setProgress(0);
        Work.addTasks(assetRequests);

        Work.addTask(new LaunchRunnable(this::afterWork, 3));
    }

    private Collection<LaunchRunnable> getAfterDownloads() {
        return toLaunch.afterDownloads()
                       .stream()
                       .map(a -> new LaunchRunnable(a, 2))
                       .collect(Collectors.toList());
    }

    private void afterWork() {
        LOG.info("Started afterWork waiting for {} >= {}", finished, totalSize);
        while(finished < totalSize) {
            try {
                TimeUnit.MILLISECONDS.sleep(250);
            } catch(InterruptedException e) {
                LOG.error("Interrupted while waiting for start tasks", e);
            }
        }
        LOG.info("Finished all assets/artifacts, starting after-work for real");
        if(assetsMap.map_to_resources) {
            LOG.info("Mapping assets");
            assetsMap.objects.forEach((k, asset) -> {
                Path to = ConfigPaths.getResourcesDirectory(toLaunch.getName())
                                     .resolve(k);
                File parent = to.getParent()
                                .toFile();
                try {
                    if(!parent.exists() && !parent.mkdirs() && !parent.exists())
                        throw new IOException("Failed to create resource's parent directory: " + parent);
                    Files.copy(asset.pathTo(),
                               ConfigPaths.getResourcesDirectory(toLaunch.getName())
                                          .resolve(k),
                               StandardCopyOption.REPLACE_EXISTING);
                } catch(IOException e) {
                    throw new UncheckedIOException(e);
                }
            });
        }

        if(failures.size() > 0) {
            LOG.info("Failed to run all asset requests");
            onFail();
        } else {
            try {
                LOG.info("Starting minecraft");
                toLaunch.start(authentication);
                Platform.runLater(App.currentModal::close);
            } catch (IOException | NoSuchAlgorithmException e) {
                LOG.error("Failed to start minecraft", e);
                failures.add(e);
                onFail();
            }
        }
    }

    private void updateProgress() {
        finished++;
        LOG.info("Updating progress: {} out of {}", finished, totalSize);
        if(totalSize == 0) {
            progress.setProgress(1);
        } else {
            var p = finished / ((double) totalSize);
            Platform.runLater(() -> progress.setProgress(p));

        }
    }

    private void fail(Throwable t) {
        this.failures.add(t);
    }

    private void onFail() {
        failures.forEach(failure -> LOG.error("Error while retrieving resources", failure));
        // TODO: Use the (currently) non-existent error modal to display the thrown error(s)
        Platform.runLater(App::closeExistingModal);
    }

    private class LaunchRunnable extends OrderedTask {
        private LaunchRunnable(Runnable run) {
            this(run, 0);
        }

        private LaunchRunnable(Runnable run, int order) {
            super(run, order, LaunchMinecraft.this::updateProgress);
        }

        @Override
        public void run() {
            try {
                this.task.run();
            } catch (Exception e) {
                LaunchMinecraft.this.fail(e);
            }
            this.then.run();
        }
    }
}
