package com.squedgy.mc.mod.manager.ui.controller;

import com.squedgy.mc.mod.manager.ui.App;
import com.squedgy.mc.mod.manager.ui.element.InstanceCreatorSelector;
import com.squedgy.mc.mod.manager.ui.element.ThemedElement;
import com.squedgy.mc.mod.manager.ui.instance.InstanceCreator;
import com.squedgy.mc.mod.manager.utils.FormattedException;
import com.squedgy.mc.mod.manager.version.loaders.InstanceLoader;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicReference;

import static org.slf4j.LoggerFactory.getLogger;

public class InstanceModal extends ThemedElement {
    private static final Logger LOG = getLogger(InstanceModal.class);

    private final AtomicReference<Optional<InstanceCreatorSelector>> currentCreator = new AtomicReference<>(Optional.empty());
    private final ServiceLoader<InstanceCreator> CREATORS = ServiceLoader.load(InstanceCreator.class);
    private final AtomicReference<Node> selected = new AtomicReference<>(null);

    @FXML public VBox root;
    @FXML public CreatorHandler creator;
    @FXML public VBox creators;
    @FXML public ScrollPane scrolly;

    public InstanceModal() throws IOException {
        super(InstanceModal.class.getResource("instance-modal.fxml"));

        ThemedElement.getTheme()
                     .enforceStyleClass(root);
        LOG.debug("Instance modals style classes: {}", root.getStyleClass());
        CREATORS.stream()
                .map(ServiceLoader.Provider::get)
                .forEach(creator -> {
                    try {
                        var selector = new InstanceCreatorSelector(creator, scrolly);
                        var root = selector.root;
                        root.onMouseClickedProperty()
                            .setValue((e) -> {
                                currentCreator.get()
                                              .ifPresent(InstanceCreatorSelector::deselect);
                                selector.select();
                                currentCreator.set(Optional.of(selector));
                                this.creator.setCreator(selector.creator);
                                this.creator.getChildren()
                                            .setAll(selector.creator);
                                this.setSelected(root);
                            });
                        creators.getChildren()
                                .add(selector.root);
                    } catch(IOException e) {
                        LOG.error("Failed to create a selector for creator {}", creator.getTitle(), e);
                    }
                });
    }

    private void setSelected(Node node) {
        var last = selected.get();
        if(last != null) {
            last.getStyleClass()
                .remove("selected");
        }
        node.getStyleClass()
            .add("selected");
        selected.set(node);
    }

    public void createInstance(Event e) {
        currentCreator.get()
                      .ifPresent(creator -> {
                          try {
                              InstanceLoader.saveInstance(creator.creator.onCreate());
                              App.refreshInstances();
                              if(App.currentModal != null) {
                                  App.currentModal.close();
                              }
                          } catch(FormattedException | IllegalArgumentException ex) {
                              LOG.error("", ex);
                          }
                      });
    }
}
