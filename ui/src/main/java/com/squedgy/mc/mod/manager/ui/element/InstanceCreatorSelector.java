package com.squedgy.mc.mod.manager.ui.element;

import com.squedgy.mc.mod.manager.ui.App;
import com.squedgy.mc.mod.manager.ui.instance.InstanceCreator;
import com.squedgy.mc.mod.manager.version.minecraft.Icon;
import javafx.beans.binding.DoubleBinding;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;

import java.io.IOException;

import static javafx.beans.binding.Bindings.createDoubleBinding;
import static org.slf4j.LoggerFactory.getLogger;

public class InstanceCreatorSelector {
    private static final Logger LOG = getLogger(InstanceCreatorSelector.class);
    public final InstanceCreator creator;

    @FXML public HBox root;
    @FXML public Label title;

    public InstanceCreatorSelector(InstanceCreator creator, ScrollPane node) throws IOException {
        this.creator = creator;
        try {
            var loader = new FXMLLoader(getClass().getResource("instance-selector.fxml"));
            loader.setController(this);
            loader.load();
        } catch(IOException e) {
            LOG.error("Failed to create Instance creator selector", e);
            throw (e);
        }
        title.setText(creator.getTitle());

        DoubleBinding prefHeight = root.heightProperty()
                                       .subtract(4);

        Icon icon = creator.getIcon();
        Node imageNode = icon.getDisplayableIcon(prefHeight, prefHeight);
        root.getChildren()
            .add(imageNode);

        var boundWidth = createDoubleBinding(() -> node.widthProperty()
                                                       .subtract(icon.getWidth()
                                                                     .get())
                                                       .subtract(10)
                                                       .get(), node.widthProperty(), icon.getWidth());
        title.prefWidthProperty()
             .bind(boundWidth);

        root.getStyleClass()
            .addListener(App.getBindStyleClass(root, imageNode));
    }

    public void select() {
        var styleClass = root.getStyleClass();
        if(!styleClass.contains("selected")) {
            styleClass.add("selected");
        }
    }

    public void deselect() {
        var styleClass = root.getStyleClass();
        while(styleClass.contains("selected")) styleClass.remove("selected");
    }

    public Node getRoot() {
        return root;
    }
}