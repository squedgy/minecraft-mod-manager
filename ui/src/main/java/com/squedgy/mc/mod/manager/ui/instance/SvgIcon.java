package com.squedgy.mc.mod.manager.ui.instance;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.squedgy.mc.mod.manager.ui.element.CustomWebView;
import com.squedgy.mc.mod.manager.version.minecraft.Icon;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.value.ObservableDoubleValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import org.slf4j.Logger;

import java.io.IOException;

import static org.slf4j.LoggerFactory.getLogger;

public class SvgIcon extends Icon {
    private static final Logger LOG = getLogger(SvgIcon.class);

    public final String svg;
    private final CustomWebView displayableIcon;

    @JsonCreator
    public SvgIcon(String svg) throws IOException {
        if(!svg.startsWith("<svg") || !svg.endsWith("</svg>") || svg.matches("href=|<(a|link|script|button|input|textarea) "))
            throw new IllegalArgumentException(
                "Svg must start with '<svg' and end with </svg>. It also cannot contain an a, link, script, button, input, or textarea element/");
        this.svg = svg;
        displayableIcon = new CustomWebView();
        displayableIcon.setContent(svg);
        displayableIcon.alignmentProperty()
                       .set(Pos.CENTER);
    }

    @Override
    public Node getDisplayableIcon(DoubleBinding maxWidth, DoubleBinding maxHeight) throws IOException {
        maxWidth(maxWidth);
        prefWidth(maxWidth);
        maxHeight(maxHeight);
        prefHeight(maxHeight);
        return displayableIcon;
    }

    @Override
    public ObservableDoubleValue getWidth() {
        return displayableIcon.widthProperty();
    }

    @Override
    public ObservableDoubleValue getHeight() {
        return displayableIcon.heightProperty();
    }

    @Override
    public void setMaxHeight(ObservableDoubleValue maxHeight) {
        displayableIcon.maxHeightProperty()
                       .bind(maxHeight);
    }

    @Override
    public void setMaxWidth(ObservableDoubleValue maxWidth) {
        displayableIcon.maxWidthProperty()
                       .bind(maxWidth);
    }

    @Override
    public void setMinHeight(ObservableDoubleValue minHeight) {
        displayableIcon.minHeightProperty()
                       .bind(minHeight);
    }

    @Override
    public void setMinWidth(ObservableDoubleValue minWidth) {
        displayableIcon.minWidthProperty()
                       .bind(minWidth);
    }

    public void maxWidth(ObservableDoubleValue maxWidth) {
        displayableIcon.maxWidthProperty()
                       .bind(maxWidth);
    }

    public void prefWidth(ObservableDoubleValue prefWidth) {
        displayableIcon.prefWidthProperty()
                       .bind(prefWidth);
    }

    public void maxHeight(ObservableDoubleValue maxHeight) {
        displayableIcon.maxHeightProperty()
                       .bind(maxHeight);
    }

    public void prefHeight(ObservableDoubleValue prefHeight) {
        displayableIcon.prefHeightProperty()
                       .bind(prefHeight);
    }
}
