package com.squedgy.mc.mod.manager.ui.element;

import com.squedgy.mc.mod.manager.configuration.LauncherOptions;
import com.squedgy.mc.mod.manager.ui.Theme;
import com.squedgy.mc.mod.manager.ui.theme.DarkTheme;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;

public class ThemedElement extends VBox {

    public static List<Theme> themes = null;

    public ThemedElement() {
        super();
    }

    public ThemedElement(double spacing) {
        super(spacing);
    }

    public ThemedElement(Node... children) {
        super(children);
    }

    public ThemedElement(double spacing, Node... children) {
        super(spacing, children);
    }

    public ThemedElement(URL fxml) throws IOException {
        this(new FXMLLoader(fxml));
    }

    public ThemedElement(FXMLLoader loader) throws IOException {
        loader.setController(this);
        loader.setRoot(this);
        loader.load();
        getTheme().enforceStyleClass(this)
                  .enforceStyleSheet(this);
    }

    public static Theme getTheme() {
        var themeName = oneOf(() -> LauncherOptions.getStringProperty("theme")
                                                   .orElse(null),
                              () -> System.getProperty("com.squedgy.theme"),
                              () -> System.getenv("SQUEDGY_THEME")
        );

        return getThemes().stream()
                          .filter(t -> Objects.equals(t.getName(), themeName))
                          .findFirst()
                          .orElse(new DarkTheme());
    }

    @SafeVarargs
    private static String oneOf(Supplier<String>... providers) {
        for(var theme : providers) {
            String value = theme.get();
            if(value != null && !value.isBlank()) return value;
        }

        return null;
    }

    public static List<Theme> getThemes() {
        if(themes == null) themes = ServiceLoader.load(Theme.class)
                                                 .stream()
                                                 .map(ServiceLoader.Provider::get)
                                                 .collect(toList());
        return themes;
    }
}
