package com.squedgy.mc.mod.manager.ui.instance;

import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.utils.FormattedException;
import com.squedgy.mc.mod.manager.utils.io.IO;
import com.squedgy.mc.mod.manager.version.minecraft.Icon;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static org.slf4j.LoggerFactory.getLogger;

public abstract class InstanceCreator extends VBox {
    private static Logger LOG = getLogger(InstanceCreator.class);

    protected InstanceCreator(URL fxmlFile) {
        FXMLLoader loader = new FXMLLoader(fxmlFile);
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
            this.maxHeightProperty()
                .set(Double.MAX_VALUE);
        } catch(IOException e) {
            LOG.error("Failed to load instance creator {}.", getClass().getSimpleName(), e);
            throw new UncheckedIOException(e);
        }
    }

    public final MinecraftInstance onCreate() throws FormattedException {
        MinecraftInstance instance = create();
        afterCreateImplementation(instance);

        return instance;
    }

    protected abstract MinecraftInstance create() throws FormattedException;

    protected final void afterCreateImplementation(MinecraftInstance instance) {
        var dir = ConfigPaths.getInstanceDirectory(instance.getName());
        try {
            IO.enforceDirectory(dir);
            var configurationFile = ConfigPaths.getInstanceConfigFile(instance.getName());
            objectMapper().writeValue(configurationFile.toFile(), instance);
        } catch(IOException e) {
            LOG.error("Failed to write instance {}'s configuration file", instance.getName(), e);
        }
    }

    public abstract String getTitle();

    /**
     * What icon should this instance use?
     * defaults to the creator's icon
     *
     * @return an instance icon
     */
    public Icon getInstanceIcon() {
        return getIcon();
    }

    public abstract Icon getIcon();
}
