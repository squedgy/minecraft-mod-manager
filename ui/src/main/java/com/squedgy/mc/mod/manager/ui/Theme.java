package com.squedgy.mc.mod.manager.ui;

import com.squedgy.mc.mod.manager.ui.element.ThemedElement;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.effect.BlendMode;
import org.slf4j.Logger;

import java.io.InputStream;
import java.net.URL;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class Theme {
    private static final Logger LOG = getLogger(Theme.class);

    public final Theme enforceStyleClass(Node node) {
        return enforceStyleClass(node.getStyleClass());
    }

    public final Theme enforceStyleClass(ObservableList<String> styleClasses) {
        LOG.debug("Enforctin {} in {}", getThemeStyleClass(), styleClasses);
        for(Theme theme : ThemedElement.getThemes())
            while(styleClasses.contains(theme.getThemeStyleClass())) styleClasses.remove(theme.getThemeStyleClass());
        styleClasses.add(getThemeStyleClass());
        return this;
    }

    public String getThemeStyleClass() {
        return getName();
    }

    public abstract String getName();

    public final Theme enforceStyleSheet(Parent node) {
        return enforceStyleSheet(node.getStylesheets());
    }

    public final Theme enforceStyleSheet(ObservableList<String> styleSheets) {
        LOG.info("Enforcing {} in {}", getThemeCss(), styleSheets);
        for(Theme theme : ThemedElement.getThemes()) {
            String styleSheet = theme.getThemeCss()
                                     .toString();
            while(styleSheets.contains(styleSheet)) styleSheets.remove(styleSheet);
        }
        styleSheets.add(getThemeCss().toString());
        return this;
    }

    public abstract URL getThemeCss();

    public abstract BlendMode getBlendMode();

    public abstract InputStream getWebViewCss();

    public String getClassName() {
        return getName();
    }
}
