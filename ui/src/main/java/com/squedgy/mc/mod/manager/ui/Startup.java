package com.squedgy.mc.mod.manager.ui;

import com.squedgy.mc.mod.manager.utils.threading.Work;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.scenicview.ScenicView;
import org.slf4j.Logger;

import java.io.IOException;

import static org.slf4j.LoggerFactory.getLogger;

public class Startup extends Application {
    private static Logger LOG = getLogger(Startup.class);

    @Override
    public void start(Stage stage) throws IOException {
        stage.setTitle("Minecraft Mod Manager");
        try {
            LOG.info("Starting app instance");
            var root = new App(); //new App();
            root.prefWidthProperty()
                .bind(stage.widthProperty());
            root.prefHeightProperty()
                .bind(stage.heightProperty());
            stage.minHeightProperty()
                 .bind(root.minHeightProperty()
                           .add(30)); //.bind(root.minHeightProperty());
            stage.minWidthProperty()
                 .bind(root.minWidthProperty()); //.bind(root.minWidthProperty());
            Scene scene = new Scene(root, root.minWidth(-1), root.minHeight(-1));
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
            App.mainStage = stage;
            var defaultOnCloseRequest = stage.onCloseRequestProperty().get();
            LOG.info("Default on close request: {}", defaultOnCloseRequest);
            stage.setOnCloseRequest(e -> {
                Work.finish();
                if(defaultOnCloseRequest != null) defaultOnCloseRequest.handle(e);
            });
        } catch(IOException e) {
            LOG.error("Failed to create app's component", e);
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        Font.loadFont(Startup.class.getResourceAsStream("FiraCode-Regular.otf"), 14);
        System.setProperty("sun.java2d.opengl", "true");

        launch(args);
    }

    public static void showDetailsOf(Scene scene) {
        ScenicView.show(scene);
    }

    public static void showDetailsOf(Parent parent) {
        ScenicView.show(parent);
    }
}
