package com.squedgy.mc.mod.manager.ui.element;

import com.squedgy.mc.mod.manager.forge.Forge;
import javafx.application.Platform;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static javafx.beans.binding.Bindings.createDoubleBinding;
import static org.slf4j.LoggerFactory.getLogger;

public class ForgeVersionManager extends VBox {
    private static final Logger LOG = getLogger(ForgeVersionManager.class);

    private final List<Forge.DisplayForgeVersion> forgeVersions = new LinkedList<>();
    private final SimpleStringProperty mcVersion = new SimpleStringProperty(null);
    private final LoadingElement loader = new LoadingElement();
    private Thread retriever;
    @FXML private TableView<Forge.DisplayForgeVersion> version;
    @FXML private HBox label;
    @FXML private TableColumn versionColumn;

    public ForgeVersionManager() throws IOException {
        super();
        var loader = new FXMLLoader(getClass().getResource("forge-version.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
            DoubleBinding height = createDoubleBinding(() -> this.heightProperty()
                                                                 .subtract(label.heightProperty()
                                                                                .subtract(10))
                                                                 .get(), this.heightProperty(), label.heightProperty());
            version.prefHeightProperty()
                   .bind(height);
            this.loader.prefHeightProperty()
                       .bind(height);
            this.loader.prefWidthProperty()
                       .bind(widthProperty().subtract(label.heightProperty()));
            label.getChildren()
                 .remove(this.loader);
            versionColumn.cellValueFactoryProperty()
                         .set((javafx.util.Callback) (a) -> mcVersion);
            updateMinecraftVersion(MinecraftVersionManager.getLatestMcVersion());
        } catch(IOException e) {
            LOG.error("Failed to initialize forge version manager");
            throw e;
        }
    }

    public void updateMinecraftVersion(String newMinecaftVersion) {
        LOG.info("newMinecraftVersion: {}", newMinecaftVersion);
        if(newMinecaftVersion == null) {
            mcVersion.set(null);
            version.setItems(FXCollections.emptyObservableList());
        } else if(!newMinecaftVersion.equals(mcVersion.get())) {
            if(retriever != null && retriever.isAlive()) {
                retriever.interrupt();
            }
            retriever = new Thread(null, () -> {
                forgeVersions.clear();
                try {
                    var newVersions = Forge.getForgeVersions(newMinecaftVersion);
                    forgeVersions.addAll(newVersions);
                } catch(IOException e) {
                    LOG.error("Failed to pull forge versions for mc {}", newMinecaftVersion, e);
                } finally {
                    updateTableView();
                    updateLoader(false);
                    mcVersion.set(newMinecaftVersion);
                }

            }, newMinecaftVersion + "-4-forge");
            retriever.start();
            updateLoader(true);
        }
    }

    private void updateTableView() {
        Platform.runLater(() -> version.setItems(FXCollections.observableList(new ArrayList<>(forgeVersions))));
    }

    private void updateLoader(boolean loading) {
        Platform.runLater(() -> {
            if(loading) {
                getChildren().setAll(label, loader);
            } else {
                getChildren().setAll(label, version);
            }
        });
    }

    public Forge.DisplayForgeVersion getForgeVersion() {
        return version.getSelectionModel()
                      .getSelectedItem();
    }

}
