package com.squedgy.mc.mod.manager.ui.element;

import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.version.minecraft.VersionManifest;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.function.Consumer;

import static com.squedgy.mc.mod.manager.version.minecraft.VersionManifest.getVersionManifest;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

public class MinecraftVersionManager extends VBox {
    private static final Logger LOG = getLogger(MinecraftVersionManager.class);
    private static VersionManifest versionManifest;

    private enum ReleaseType {
        RELEASE(true),
        SNAPSHOT(true),
        ALPHA,
        BETA,
        OLD_ALPHA,
        EXPERIMENTS
        ;

        private boolean active;
        ReleaseType(boolean active) { this.active = active; }

        ReleaseType() { this(false); }

        public static boolean isActive(String type) {
            for(ReleaseType r : values()) if(r.name().equalsIgnoreCase(type)) return r.active;

            LOG.error("Release type {} was received, but there is no indication of this type.", type);
            return false;
        }
    }

    static { refreshVersionManifest(); }

    @FXML public TableView<VersionManifest.Version> version;

    private Consumer<VersionManifest.Version> handleSelectionUpdate = (e) -> {};

    public MinecraftVersionManager() throws RuntimeException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("minecraft-version.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            if(versionManifest == null) {
                versionManifest = getVersionManifest();
            }
            loader.load();
            version.setItems(FXCollections.observableList(versionManifest.versions.stream()
                                                                                  .filter(v -> v.type != null)
                                                                                  .filter(v -> ReleaseType.isActive(v.type))
                                                                                  .collect(toList())));
            version.getSelectionModel()
                   .select(versionManifest.versions.stream()
                                                   .filter(v -> v.id.equals(versionManifest.latest.release))
                                                   .findFirst()
                                                   .get());

            version.setOnMousePressed(e -> {
                if(e.getButton() == MouseButton.PRIMARY && (e.isShiftDown() || e.isControlDown())) {
                    e.consume();
                }
                handleSelectionUpdate.accept(version.getSelectionModel()
                                                    .getSelectedItem());
                LOG.trace(
                    "Selection: {}",
                    version.getSelectionModel()
                           .getSelectedItem()
                );
            });
        } catch(InterruptedException | IOException | URISyntaxException | RequestException e) {
            throw new RuntimeException(e);
        }
    }

    public void setHandleSelectionUpdate(Consumer<VersionManifest.Version> onUpdate) {
        if(onUpdate != null) this.handleSelectionUpdate = onUpdate;
    }

    public String zonedDateTimeToString(ZonedDateTime dateTime) {
        return dateTime.toLocalDate()
                       .toString();
    }

    public VersionManifest.Version getVersion() {
        return version.getSelectionModel()
                      .getSelectedItem();
    }

    public static String getLatestMcVersion() {
        if(versionManifest == null) {
            refreshVersionManifest();
        }

        return versionManifest.latest.release;
    }

    public static void refreshVersionManifest() {
        try {
            versionManifest = getVersionManifest();
        } catch(IOException | URISyntaxException | InterruptedException | RequestException e) {
            LOG.error("Failed to load version manifest", e);
        }
    }

}
