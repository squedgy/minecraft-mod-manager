package com.squedgy.mc.mod.manager.ui.element;

import com.squedgy.mc.mod.manager.ui.App;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class InstanceIcon {
    public final MinecraftInstance instance;
    private final Node image;
    @FXML public VBox parent;
    @FXML public Label name;

    public InstanceIcon(MinecraftInstance instance) throws IOException {
        this.instance = instance;
        var loader = new FXMLLoader(getClass().getResource("instance-icon.fxml"));
        loader.setController(this);
        loader.load();
        image = instance.icon.getDisplayableIcon(new SimpleDoubleProperty(60), new SimpleDoubleProperty(60));
        instance.icon.setMaxHeight(new SimpleDoubleProperty(60));
        instance.icon.setMaxWidth(new SimpleDoubleProperty(60));
        instance.icon.setMinHeight(new SimpleDoubleProperty(60));
        instance.icon.setMinWidth(new SimpleDoubleProperty(60));
        image.getStyleClass()
             .addAll(parent.getStyleClass());
        parent.getChildren()
              .add(0, image);

        name.setText(instance.getName());
        parent.getStyleClass()
              .addListener(App.getBindStyleClass(parent, image));
    }

}
