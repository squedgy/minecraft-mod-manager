package com.squedgy.mc.mod.manager.ui.theme;

import com.squedgy.mc.mod.manager.ui.Theme;
import javafx.scene.effect.BlendMode;

import java.io.InputStream;
import java.net.URL;

public final class LightTheme extends Theme {
    @Override
    public URL getThemeCss() {
        return getClass().getResource("light.css");
    }

    @Override
    public BlendMode getBlendMode() {
        return null;
    }

    @Override
    public InputStream getWebViewCss() {
        return LightTheme.class.getResourceAsStream("light-web.css");
    }

    @Override
    public String getName() {
        return "light";
    }
}
