package com.squedgy.mc.mod.manager.ui;

import com.squedgy.mc.mod.manager.ui.controller.AuthenticateModal;
import com.squedgy.mc.mod.manager.ui.controller.InstanceModal;
import com.squedgy.mc.mod.manager.ui.controller.LaunchMinecraft;
import com.squedgy.mc.mod.manager.ui.element.InstanceIcon;
import com.squedgy.mc.mod.manager.ui.element.ThemedElement;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import static com.squedgy.mc.mod.manager.version.loaders.InstanceLoader.getInstances;
import static java.util.Optional.empty;
import static org.slf4j.LoggerFactory.getLogger;

public class App extends ThemedElement {
    private static final Logger LOG = getLogger(App.class);

    public static Stage mainStage;
    public static Stage currentModal;
    public static App instance;
    private static Set<MinecraftInstance> minecraftInstances = getInstances();
    @FXML public VBox root;
    @FXML public HBox instancesContainer;
    @FXML public FlowPane instances;
    @FXML public Menu instanceMenu;
    private Optional<InstanceIcon> selected = empty();

    public App() throws IOException {
        super(App.class.getResource("app.fxml"));
        instance = this;
        buildInstanceIcons();
    }

    private void buildInstanceIcons() {
        var instance = new ArrayList<VBox>();
        LOG.info("Minecraft Instances: {}", minecraftInstances);
        for(MinecraftInstance minecraftInstance : minecraftInstances) {
            InstanceIcon instanceIcon;
            try {
                instanceIcon = new InstanceIcon(minecraftInstance);
                instanceIcon.parent.onMouseClickedProperty()
                                   .set(e -> {
                                       this.selected.ifPresent(selected -> selected.parent.getStyleClass()
                                                                                          .remove("selected"));
                                       this.selected = Optional.of(instanceIcon);
                                       instanceIcon.parent.getStyleClass()
                                                          .add("selected");
                                       if(e.getClickCount() == 2) {
                                           try {
                                               var modal = new AuthenticateModal();
                                               var result = modal.authenticate();
                                               LaunchMinecraft launcher = new LaunchMinecraft(result, instanceIcon.instance);
                                               newImportantModal("Starting " + instanceIcon.instance.getName(), launcher);
                                           } catch(Exception ioException) {
                                               LOG.error("Failed to start minecraft", ioException);
                                           }
                                       }
                                   });
                instance.add(instanceIcon.parent);
            } catch(IOException e) {
                LOG.error("Failed to initialize instance {}", minecraftInstance.getName(), e);
            }
        }
        LOG.info("Instances: {}", instance);
        this.instances.getChildren()
                      .setAll(instance);
    }

    private void newImportantModal(String title, Parent node) {
        Scene scene = new Scene(node, node.minWidth(-1) + 20, node.minHeight(-1) + 20);
        effectAndShowCurrentModal(title, node, scene, Modality.APPLICATION_MODAL);
    }

    private static void effectAndShowCurrentModal(String title, Parent node, Scene scene, Modality modality) {
        if(currentModal != null && currentModal.isShowing()) currentModal.close();
        currentModal = new Stage();
        currentModal.setTitle(title);
        currentModal.setScene(scene);
        currentModal.setAlwaysOnTop(false);
        currentModal.centerOnScreen();
        currentModal.initModality(modality);
        currentModal.initOwner(mainStage);
        tryBindProperties(node, currentModal);
        currentModal.show();
    }

    private static void tryBindProperties(Node node, Stage target) {
        tryBind(
            node,
            "maxWidthProperty",
            node.maxWidth(-1),
            v -> target.maxWidthProperty()
                       .bind(v)
        );
        tryBind(
            node,
            "maxHeightProperty",
            node.maxHeight(-1),
            v -> target.maxHeightProperty()
                       .bind(v)
        );
        tryBind(
            node,
            "minWidthProperty",
            node.minWidth(-1),
            v -> target.minWidthProperty()
                       .bind(v)
        );
        tryBind(
            node,
            "minHeightProperty",
            node.minHeight(-1),
            v -> target.minHeightProperty()
                       .bind(v)
        );
    }

    private static void tryBind(Node node, String method, Double otherwise, Consumer<ObservableValue<? extends Number>> set) {
        ObservableValue<? extends Number> value = new SimpleDoubleProperty(otherwise);
        try {
            Method propMeth = node.getClass()
                                  .getMethod(method);
            ObservableValue<? extends Number> saved = (ObservableValue<? extends Number>) propMeth.invoke(node);
            if(saved.getValue()
                    .doubleValue() > 0) {
                value = saved;
            }
        } catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOG.trace(
                "Failed to find node {}'s {}}",
                node.getClass()
                    .getSimpleName(),
                method
            );
        }
        set.accept(value);
    }

    public void openInstanceModal(Event event) {
        if(currentModal != null && currentModal.isShowing()) {
            currentModal.close();
        }
        event.consume();
        LOG.info("Showing create instance modal");
        currentModal = new Stage();
        try {
            VBox parent = new InstanceModal();
            newModal("Create", parent);
            //			Startup.showDetailsOf(currentModal.getScene(), null);
        } catch(IOException e) {
            LOG.error("Failed to open new instance modal.", e);
        }
    }

    public static void newModal(String title, Parent node) {
        Scene scene = new Scene(node, node.minWidth(-1) + 20, node.minHeight(-1) + 20);
        effectAndShowCurrentModal(title, node, scene, Modality.WINDOW_MODAL);
    }

    public void closeMenu(Event event) {
        instanceMenu.hide();
    }

    public static boolean closeExistingModal() {
        if(currentModal != null && currentModal.isShowing()) {
            currentModal.close();
            return true;
        }
        return false;
    }

    public static ListChangeListener<String> getBindStyleClass(Node parent, Node child) {
        return (newStyles) -> {
            while(newStyles.next()) {
                List<String> classes = child.getStyleClass();
                if(newStyles.wasRemoved()) {
                    newStyles.getRemoved()
                             .forEach(removed -> {
                                 while(classes.contains(removed)) classes.remove(removed);
                             });
                }
                if(newStyles.wasAdded()) classes.addAll(newStyles.getAddedSubList());
            }
        };
    }

    public static void refreshInstances() {
        minecraftInstances = getInstances();
        instance.buildInstanceIcons();
    }

}
