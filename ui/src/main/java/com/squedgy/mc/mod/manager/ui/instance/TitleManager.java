package com.squedgy.mc.mod.manager.ui.instance;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.io.UncheckedIOException;

public class TitleManager extends VBox {

    @FXML public TextField instanceName;

    public TitleManager() throws UncheckedIOException {
        var loader = new FXMLLoader(getClass().getResource("title.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch(IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String getInstanceName() {
        return instanceName.getText();
    }

}
