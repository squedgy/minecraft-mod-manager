package com.squedgy.mc.mod.manager.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

public class MinecraftInstanceForm {
    private static final Logger LOG = getLogger(MinecraftInstanceForm.class);

    @FXML private TextField instanceName;

}
