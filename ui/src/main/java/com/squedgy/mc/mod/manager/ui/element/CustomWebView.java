package com.squedgy.mc.mod.manager.ui.element;

import com.squedgy.mc.mod.manager.ui.App;
import com.squedgy.mc.mod.manager.ui.Theme;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import org.slf4j.Logger;

import java.io.IOException;

import static com.squedgy.mc.mod.manager.ui.element.ThemedElement.getTheme;
import static org.slf4j.LoggerFactory.getLogger;

public class CustomWebView extends VBox {
    private static final Logger LOG = getLogger(CustomWebView.class);

    @FXML private WebView webView;
    private String content;

    public CustomWebView() throws IOException {
        var loader = new FXMLLoader(getClass().getResource("custom-web-view.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch(IOException e) {
            LOG.error("Failed to initialized CustomWebView", e);
            throw e;
        }
        Parent parent = getParent();
        if(parent != null) this.getStyleClass()
                               .addAll(parent.getStyleClass());

        getStyleClass().addListener(App.getBindStyleClass(this, webView));
        webView.getStyleClass()
               .addListener((ListChangeListener<String>) (change) -> Platform.runLater(() -> setContent(content)));
        //        webView.getEngine()
        //               .getLoadWorker()
        //               .stateProperty()
        //               .addListener((obs, old, neu) -> {
        //                   if(neu == Worker.State.SUCCEEDED) {
        //                       Document doc = webView.getEngine()
        //                                             .getDocument();
        //                       try(ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
        //                           try {
        //                               Transformer transformer = TransformerFactory.newInstance()
        //                                                                           .newTransformer();
        //                               transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        //                               transformer.setOutputProperty(OutputKeys.METHOD, "html");
        //                               transformer.setOutputProperty(OutputKeys.INDENT, "no");
        //                               transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        ////                               transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        //                               transformer.transform(new DOMSource(doc), new StreamResult(stream));
        //                               LOG.trace("New web view content:\n{}", stream.toString());
        //                           } catch(TransformerException e) {
        //                               LOG.error("Failed to create transformer", e);
        //                           }
        //                       } catch(IOException e) {
        //                           LOG.error("Failed to create output stream");
        //                       }
        //                   }
        //               });

        webView.prefWidthProperty()
               .bind(this.widthProperty());
        webView.prefHeightProperty()
               .bind(this.heightProperty());
    }

    public void setContent(String content) {
        Theme theme = getTheme();
        String baseContent = getDefaultContentBase();
        String extraStyle = "";
        try {
            extraStyle = "<style>" +
                         new String(theme.getWebViewCss()
                                         .readAllBytes()) +
                         "</style>";
        } catch(Exception e) {
            LOG.warn("Failed to find styles for theme {}, using default", getTheme().getName(), e);
        }
        webView.setBlendMode(theme.getBlendMode());

        this.content = content;
        webView.getEngine()
               .loadContent("<!DOCTYPE html><html class='" +
                            String.join(" ", getStyleClass()) +
                            "'><head>" +
                            baseContent +
                            extraStyle +
                            "</head><body>" +
                            wrapContent(content) +
                            "</body></html>");
    }

    private String getDefaultContentBase() {
        return "<script>" +
               "window.onload = function() {var tags = document.getElementsByTagName('iframe');" +
               "for(var i = tags.length-1; i >= 0; i--){tags[i].parentNode.removeChild(tags[i]);}" +
               "var links = document.querySelectorAll(\"[href]\");" +
               "for(var link of links) {link.href = undefined;}}" +
               "</script>" +
               "<style>" +
               "html, body {min-height: 100vh; min-width: 100%; overflow: hidden; margin: 0; padding: 0;}" +
               "#main-grid {display: flex; justify-content: center; align-items: center; min-height: 100vh;}" +
               "#content {max-width: 100%; max-height: 100%;}" +
               "</style>";
    }

    private String wrapContent(String content) {
        return "<div id='main-grid'><div id='content'>" + content + "</div></div>";
    }

    public void load(String url) {
        this.content = null;
        webView.setBlendMode(getTheme().getBlendMode());
        webView.getEngine()
               .load(url);
    }

}
