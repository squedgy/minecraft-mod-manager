package com.squedgy.mc.mod.manager.ui.controller;

import com.squedgy.mc.mod.manager.ui.instance.InstanceCreator;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class CreatorHandler extends VBox {

    private InstanceCreator creator;

    public CreatorHandler() throws IOException {
        FXMLLoader loader = new FXMLLoader(CreatorHandler.class.getResource("creator.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        loader.load();
        this.maxHeightProperty()
            .set(Double.MAX_VALUE);
        this.prefHeightProperty()
            .set(Double.MAX_VALUE);
    }

    public void setCreator(InstanceCreator creator) {
        this.getChildren()
            .setAll(creator);
        creator.prefHeightProperty()
               .bind(this.heightProperty());
        this.autosize();
    }

}
