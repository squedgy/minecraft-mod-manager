package com.squedgy.mc.mod.manager.ui.instance;

import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.forge.Forge;
import com.squedgy.mc.mod.manager.forge.Post13ForgeClientJson;
import com.squedgy.mc.mod.manager.forge.Pre13ForgeClientJson;
import com.squedgy.mc.mod.manager.forge.install.ForgeInstance;
import com.squedgy.mc.mod.manager.forge.install.InstallProfile;
import com.squedgy.mc.mod.manager.forge.install.NewForgeInstallProfile;
import com.squedgy.mc.mod.manager.forge.install.OldForgeInstallProfile;
import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.ui.element.ForgeVersionManager;
import com.squedgy.mc.mod.manager.ui.element.MinecraftVersionManager;
import com.squedgy.mc.mod.manager.utils.FormattedException;
import com.squedgy.mc.mod.manager.utils.io.IO;
import com.squedgy.mc.mod.manager.version.json.ClientJson;
import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.library.Artifact;
import com.squedgy.mc.mod.manager.version.library.LibraryDownloads;
import com.squedgy.mc.mod.manager.version.minecraft.Icon;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import com.squedgy.mc.mod.manager.version.minecraft.UnknownVersionException;
import javafx.beans.binding.DoubleBinding;
import javafx.fxml.FXML;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static org.slf4j.LoggerFactory.getLogger;

public class ForgeInstanceCreator extends InstanceCreator {
    public static final Icon BASE_FORGE_ICON;
    private static final Logger LOG = getLogger(ForgeInstanceCreator.class);

    static {
        try {
            BASE_FORGE_ICON = new SvgIcon(
                "<svg viewBox=\"-2.5 0 100 50\" height='auto' width='100%'><path d='M 47.5 2 l -17.5 0 l 0 4 l -30 -1 s 10,15 29,15 s 5,30 -5,28 l 23.5 0 M 47.5 2 l 17.5 0 l 0 4 l 30 -1 s -10,15 -29,15 s -5,30 5,28 l -23.5 0' stroke=\"#114\" fill=\"#114\" /></svg>");
        } catch(IOException e) {
            LOG.error("Failed to create forge icon", e);
            throw new UncheckedIOException(e);
        }
    }

    @FXML private TitleManager title;
    @FXML private MinecraftVersionManager mcVersion;
    @FXML private ForgeVersionManager forgeVersion;

    public ForgeInstanceCreator() {
        super(ForgeInstanceCreator.class.getResource("forge-creator.fxml"));
        mcVersion.setHandleSelectionUpdate(version -> forgeVersion.updateMinecraftVersion(version.id));
        DoubleBinding halfHeight = this.heightProperty()
                                       .subtract(title.heightProperty())
                                       .subtract(10);
        forgeVersion.prefHeightProperty()
                    .bind(halfHeight);
        mcVersion.prefHeightProperty()
                 .bind(halfHeight);
    }

    @Override
    public MinecraftInstance create() throws FormattedException {
        String minecraftVersion = mcVersion.getVersion().id;
        Forge.DisplayForgeVersion displayForgeVersion = this.forgeVersion.getForgeVersion();
        AtomicReference<ClientJson> versionInfo = new AtomicReference<>(null);
        AtomicReference<InstallProfile> installProfile = new AtomicReference<>(null);
        Forge.ForgeVersion forgeVersion = null;
        try {
            forgeVersion = displayForgeVersion.toForgeVersion();
            if(forgeVersion.installer.isPresent()) {
                Artifact installer = forgeVersion.installer.get();
                if(installer.doesNotExist()) installer.download();

                Path installerLocation = ConfigPaths.getLibraryPath()
                                                    .resolve(installer.getPath());
                try {
                    IO.getJarEntry(installerLocation, "install_profile.json", profile -> {
                        installProfile.set(objectMapper().readValue(profile, NewForgeInstallProfile.class));
                    });
                    IO.getJarEntry(installerLocation, installProfile.get().json.substring(1), version -> {
                        versionInfo.set(objectMapper().readValue(version, Post13ForgeClientJson.class));
                    });
                } catch(IOException | UncheckedIOException | NullPointerException e) {
                    LOG.trace("Failed to get new forge instance profile", e);
                    if(installProfile.get() == null || installProfile.get().minecraft == null) {
                        IO.getJarEntry(installerLocation, "install_profile.json", stream -> {
                            OldForgeInstallProfile profile = objectMapper().readValue(stream, OldForgeInstallProfile.class);
                            installProfile.set(profile);
                            versionInfo.set(profile.retrieveVersionInfo());
                        });
                    } else {
                        IO.getJarEntry(installerLocation, installProfile.get().json.substring(1), version -> {
                            versionInfo.set(objectMapper().readValue(version, Pre13ForgeClientJson.class));
                        });
                    }
                }
            } else {
                installProfile.set(new NewForgeInstallProfile(Map.of(),
                                                              0,
                                                              forgeVersion.version,
                                                              null,
                                                              "net.minecraftforge:forge:" + forgeVersion.version,
                                                              minecraftVersion,
                                                              List.of(),
                                                              List.of()
                ));
                versionInfo.set(Pre13ForgeClientJson.of(forgeVersion, minecraftVersion));
            }
        } catch(IOException | RequestException | URISyntaxException | InterruptedException | UnknownVersionException e) {
            throw new FormattedException("Failed to get version json for forge version {}", forgeVersion, e);
        }

        return new ForgeInstance(this.title.getInstanceName(),
                                 mcVersion.getVersion().id,
                                 processLibraries(installProfile.get(), minecraftVersion, forgeVersion),
                                 processLibraries(versionInfo.get(), minecraftVersion, forgeVersion),
                                 forgeVersion,
                                 getInstanceIcon()
        );
    }

    @Override
    public String getTitle() {
        return "Minecraft Forge";
    }

    @Override
    public Icon getIcon() {
        return BASE_FORGE_ICON;
    }

    private static InstallProfile processLibraries(InstallProfile base, String minecraftVersion, Forge.ForgeVersion forgeVersion) {
        return new InstallProfile(base.data,
                                  base.spec,
                                  base.version,
                                  base.json,
                                  base.path,
                                  base.minecraft,
                                  processLibraries(base.libraries, minecraftVersion, forgeVersion),
                                  base.processors
        );
    }

    private static ClientJson processLibraries(ClientJson base, String minecraftVersion, Forge.ForgeVersion forgeVersion) {
        return new ClientJson(base.id,
                              base.mainClass,
                              base.type,
                              base.assets,
                              base.releaseTime,
                              base.time,
                              base.assetIndex,
                              processLibraries(base.libraries, minecraftVersion, forgeVersion),
                              base.logging,
                              base.arguments
        );
    }

    private static Collection<Library> processLibraries(Collection<Library> libraries,
                                                        String minecraftVersion,
                                                        Forge.ForgeVersion forgeVersion) {
        if(libraries.stream()
                    .anyMatch(l -> l.getName()
                                    .startsWith("net.minecraft:client:"))) LOG.info("Found minecraft client in libraries\n  {}", libraries);
        else LOG.info("Found no minecraft client in libraries\n  {}", libraries);

        String forgeKey = "net.minecraftforge:forge:" + minecraftVersion + '-' + forgeVersion.version;

        Function<Library, Library> ident = Function.identity();
        BinaryOperator<Library> join = (a, b) -> {
            var r1 = a.getRules();
            var r2 = b.getRules();
            var rules = r1.size() > r2.size() ? r1 : r2;
            var downloads = new LibraryDownloads(a.getDownloads()
                                                  .getArtifact()
                                                  .or(() -> b.getDownloads()
                                                             .getArtifact()),
                                                 a.getDownloads()
                                                  .getClassifiers()
                                                  .or(() -> b.getDownloads()
                                                             .getClassifiers())
            );

            return new Library(a.getName(),
                               downloads,
                               a.getExtract()
                                .or(() -> b.getExtract()),
                               a.getNatives()
                                .or(() -> b.getNatives()),
                               rules,
                               a.isForClient(),
                               a.isForServer()
            );
        };
        Map<String, Library> allLibraries = libraries.stream()
                                                     .collect(Collectors.toMap(l -> l.getName(), ident, join, HashMap::new));

        List<String> matchingKeys = allLibraries.keySet()
                                                .stream()
                                                .filter(f -> f.startsWith(forgeKey))
                                                .collect(Collectors.toList());
        matchingKeys.forEach(key -> {
            Library forge = allLibraries.get(key);
            LOG.info("Found forge library: {}", forge);
            if(forge.getDownloads()
                    .getArtifact()
                    .map(a -> a.getUrl()
                               .isBlank())
                    .orElse(false)) {
                var newDownloads = new LibraryDownloads(Optional.of(forgeVersion.client),
                                                        forge.getDownloads()
                                                             .getClassifiers()
                );
                allLibraries.put(key, new Library(forge.getName(),
                                                  newDownloads,
                                                  forge.getExtract(),
                                                  forge.getNatives(),
                                                  forge.getRules(),
                                                  forge.isForClient(),
                                                  forge.isForServer()
                ));
            }
        });

        return allLibraries.values();
    }
}
