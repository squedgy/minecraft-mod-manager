package com.squedgy.mc.mod.manager.ui.instance;

import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.ui.element.MinecraftVersionManager;
import com.squedgy.mc.mod.manager.utils.FormattedException;
import com.squedgy.mc.mod.manager.version.minecraft.Icon;
import com.squedgy.mc.mod.manager.version.minecraft.ImageIcon;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import com.squedgy.mc.mod.manager.version.minecraft.UnknownVersionException;
import javafx.fxml.FXML;

import java.io.IOException;
import java.net.URISyntaxException;

public class MinecraftInstanceCreator extends InstanceCreator {

    @FXML protected TitleManager title;
    @FXML protected MinecraftVersionManager minecraftVersion;

    public MinecraftInstanceCreator() {
        super(MinecraftInstanceCreator.class.getResource("minecraft-creator.fxml"));
    }

    @Override
    public MinecraftInstance create() throws FormattedException {
        try {
            return new MinecraftInstance(this.title.getInstanceName(), this.minecraftVersion.getVersion().id, getInstanceIcon());
        } catch(UnknownVersionException | InterruptedException | URISyntaxException | RequestException | IOException e) {
            throw new FormattedException("Failed to create a minecraft instance for version: {}", minecraftVersion.getVersion().id, e);
        }
    }

    @Override
    public String getTitle() {
        return "Minecraft";
    }

    @Override
    public Icon getIcon() {
        return ImageIcon.DEFAULT_ICON;
    }

}
