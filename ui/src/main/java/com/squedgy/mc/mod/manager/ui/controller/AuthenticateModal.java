package com.squedgy.mc.mod.manager.ui.controller;

import com.squedgy.mc.mod.manager.auth.Authenticate;
import com.squedgy.mc.mod.manager.auth.AuthenticationError;
import com.squedgy.mc.mod.manager.auth.UserToken;
import com.squedgy.mc.mod.manager.auth.format.AuthenticationResponse;
import com.squedgy.mc.mod.manager.configuration.LauncherOptions;
import com.squedgy.mc.mod.manager.ui.element.ThemedElement;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.slf4j.LoggerFactory.getLogger;

public class AuthenticateModal extends ThemedElement {

    private static final Logger LOG = getLogger(AuthenticateModal.class);

    @FXML private TextField email;
    @FXML private PasswordField password;

    private Stage stage;
    private AuthenticationResponse authenticationResponse;

    public AuthenticateModal() throws IOException {
        super(AuthenticateModal.class.getResource("authenticate-modal.fxml"));
    }

    public AuthenticationResponse authenticate() {
        var response = new AtomicReference<Optional<AuthenticationResponse>>(Optional.empty());
        LauncherOptions.getStringProperty(LauncherOptions.USERNAME)
                       .ifPresent(username -> {
                           LOG.info("Found common username {}", username);
                           var token = UserToken.getUserToken(username);
                           try {
                               if(token != null) {
                                   LOG.info("Validating existing token");
                                   if(!Authenticate.checkToken(token.accessToken)) {
                                       LOG.info("Existing client token wasn't valid, attempting refresh");
                                       response.set(Optional.of(Authenticate.refresh(token)));
                                   } else {
                                       response.set(Optional.of(token));
                                   }
                               }
                           } catch(IOException | URISyntaxException | InterruptedException | AuthenticationError e) {
                               LOG.warn("Failed to get an existing token for user {}", username);
                           }
                       });

        var auth = response.get();
        if(auth.isPresent()) return auth.get();

        stage = new Stage();
        var scene = new Scene(this, minWidthProperty().get(), minHeightProperty().get());
        stage.setScene(scene);
        stage.setAlwaysOnTop(true);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Authenticate");
        stage.showAndWait();

        return authenticationResponse;
    }

    public void submit() {
        try {
            authenticationResponse = Authenticate.authenticateUser(email.getText(), password.getText());
            stage.close();
        } catch(IOException | URISyntaxException | InterruptedException | AuthenticationError e) {
            LOG.warn("Failed to authenticate user {}", email.getText(), e);
            // ErrorModal.popup(e);
        }
    }

}
