package com.squedgy.mc.mod.manager.ui.theme;

import com.squedgy.mc.mod.manager.ui.Theme;
import javafx.scene.effect.BlendMode;

import java.io.InputStream;
import java.net.URL;

public class DarkTheme extends Theme {
    @Override
    public URL getThemeCss() {
        return getClass().getResource("dark.css");
    }

    @Override
    public BlendMode getBlendMode() {
        return null;
    }

    @Override
    public InputStream getWebViewCss() {
        return getClass().getResourceAsStream("web-view-dark.css");
    }

    @Override
    public String getName() {
        return "dark";
    }
}
