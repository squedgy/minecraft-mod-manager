package com.squedgy.mc.mod.manager.ui.element;

import com.squedgy.mc.mod.manager.ui.instance.SvgIcon;
import com.squedgy.mc.mod.manager.version.minecraft.Icon;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;

import static org.slf4j.LoggerFactory.getLogger;

public class LoadingElement extends Pane {
    private static final Logger LOG = getLogger(LoadingElement.class);
    private static final Icon LOADING_SVG;

    static {
        try {
            LOADING_SVG = new SvgIcon(
                "<svg style='max-height: 100%; max-width: 100%; display: block;' width='100%' height='100%' viewBox='0 0 100 100'><path d='M 50 10 a 40 40 0 1 0 0,80' fill='transparent' stroke-width='7'><animateTransform attributeName='transform' attributeType='XML' dur='1s' from='0 50 50' repeatCount='indefinite' to='360 50 50' type='rotate'></animateTransform><animate attributeName='stroke' attributeType='XML' dur='6' repeatCount='indefinite' values='#228;#288;#228'></animate></path><path d='M 50 20  a 30 30 0 1 0 0,60' fill='transparent' stroke-width='7'><animateTransform attributeName='transform' attributeType='XML' dur='1.5s' from='0 50 50' repeatCount='indefinite' to='360 50 50' type='rotate'></animateTransform><animate attributeName='stroke' attributeType='XML' dur='6' repeatCount='indefinite' values='#228;#288;#228'></animate></path><path d='M 50 30 a 20 20 0 1 0 0,40'><animateTransform attributeName='transform' attributeType='XML' dur='3s' from='0 50 50' repeatCount='indefinite' to='360 50 50' type='rotate'></animateTransform><animate attributeName='fill' attributeType='XML' dur='6' repeatCount='indefinite' values='#228;#288;#228'></animate></path></svg>");
        } catch(IOException e) {
            LOG.error("Failed to create loading svg", e);
            throw new UncheckedIOException(e);
        }
    }

    @FXML private Node view;

    public LoadingElement() throws IOException {
        var loader = new FXMLLoader(getClass().getResource("loading-element.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
            getChildren().setAll(LOADING_SVG.getDisplayableIcon(prefWidthProperty(), prefHeightProperty()));
        } catch(IOException e) {
            LOG.error("Failed to initialize loading element", e);
            throw e;
        }
        LOADING_SVG.setMaxWidth(maxWidthProperty());
        LOADING_SVG.setMaxHeight(maxHeightProperty());
    }
}
