module com.squedgy.mc.mod.manager.ui {

    requires org.slf4j;
    requires ch.qos.logback.classic;
    requires javafx.fxml;
    requires javafx.media;
    requires javafx.web;
    requires java.naming;
    requires org.scenicview.scenicview;

    requires com.squedgy.mc.mod.manager.http;
    requires com.squedgy.mc.mod.manager.auth;
    requires com.squedgy.mc.mod.manager.version;
    requires com.squedgy.mc.mod.manager.configuration;
    requires com.squedgy.mc.mod.manager.forge;

    opens com.squedgy.mc.mod.manager.ui to javafx.graphics, javafx.fxml;
    opens com.squedgy.mc.mod.manager.ui.controller to javafx.graphics, javafx.fxml;
    opens com.squedgy.mc.mod.manager.ui.element to javafx.graphics, javafx.fxml;
    opens com.squedgy.mc.mod.manager.ui.instance to javafx.graphics, javafx.fxml, com.fasterxml.jackson.databind;

    uses com.squedgy.mc.mod.manager.ui.instance.InstanceCreator;
    uses com.squedgy.mc.mod.manager.ui.Theme;
    provides com.squedgy.mc.mod.manager.ui.instance.InstanceCreator with com.squedgy.mc.mod.manager.ui.instance.MinecraftInstanceCreator, com.squedgy.mc.mod.manager.ui.instance.ForgeInstanceCreator;
    provides com.squedgy.mc.mod.manager.ui.Theme with com.squedgy.mc.mod.manager.ui.theme.DarkTheme, com.squedgy.mc.mod.manager.ui.theme.LightTheme;
}