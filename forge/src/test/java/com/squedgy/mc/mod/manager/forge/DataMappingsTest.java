package com.squedgy.mc.mod.manager.forge;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DataMappingsTest {

    private final Map<String, Object> values = Map.of("test", new TestClass(), "thisIsAString", "Hello There");

    @Test
    public void value_is_parsed() throws InvocationTargetException, IllegalAccessException {
        Mapping currentMapping = new Mapping("{test}", "{test}");

        assertEquals(values.get("test")
                           .toString(), currentMapping.parseValue(true, values));
        assertEquals(values.get("test")
                           .toString(), currentMapping.parseValue(false, values));
    }

    @Test
    public void retrieves_value_on_given_value() throws InvocationTargetException, IllegalAccessException {
        Mapping currentMapping = new Mapping("{test.getThing}", "{test.otherThing}");

        assertEquals("1234", currentMapping.parseValue(true, values));
        assertEquals("I am a string", currentMapping.parseValue(false, values));
    }

    @Test
    public void accesses_fields_on_retrieved_fields() throws InvocationTargetException, IllegalAccessException {
        Mapping currentMapping = new Mapping("{test.testclass.testclass.getThing}", "{test.testclass.testclass.otherThing}");

        assertEquals("1234", currentMapping.parseValue(true, values));
        assertEquals("I am a string", currentMapping.parseValue(false, values));
    }

    @Test
    public void cant_access_private_arguments() {
        Mapping currentMapping = new Mapping("{test.testclass.testclass.dontUseMe}", "{test.dontUseMe}");

        assertThrows(IllegalArgumentException.class, () -> currentMapping.parseValue(true, values));
        assertThrows(IllegalArgumentException.class, () -> currentMapping.parseValue(false, values));
    }

    @Test
    public void parses_multiple_values_into_result() throws InvocationTargetException, IllegalAccessException {
        Mapping currentMapping = new Mapping("{test.testclass.getThing}/someotherstuff/{thisIsAString}", null);

        assertEquals("1234/someotherstuff/Hello There", currentMapping.parseValue(true, values));
    }

    private static class TestClass {
        public String otherThing = "I am a string";
        private int dontUseMe = 123;

        public int getThing() {
            return 1234;
        }

        public TestClass testclass() {
            return this;
        }
    }

}
