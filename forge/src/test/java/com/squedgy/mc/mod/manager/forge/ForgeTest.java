package com.squedgy.mc.mod.manager.forge;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;

import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

public class ForgeTest {
    @Test
    public void parses_a_minecraft_forge_version_page() throws IOException {
        Jsoup.parse(getClass().getResourceAsStream("test.html"), "UTF-8", "https://files.minecraftforge.net");
    }

    @Test
    public void display_forge_versions_are_parsed() throws IOException, URISyntaxException, InterruptedException {
        Document d = Jsoup.parse(getClass().getResourceAsStream("test.html"), "UTF-8", "https://files.minecraftforge.net");
        var versions = Forge.parseForgeDocument("1.12", d);

        var list = (of(
            new Forge.DisplayForgeVersion("1.12", "14.21.1.2443", LocalDateTime.of(2017, 8, 1, 21, 42)),
            new Forge.DisplayForgeVersion("1.12", "14.21.1.2442", LocalDateTime.of(2017, 8, 1, 21, 32))
        ));

        assertIterableEquals(list, versions);
        assertEquals(
            "https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.12-14.21.1.2443/forge-1.12-14.21.1.2443-installer.jar",
            versions.get(0)
                    .toForgeVersion().installer.get()
                                               .getUrl()
        );
    }
}
