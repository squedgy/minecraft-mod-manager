package com.squedgy.mc.mod.manager.forge;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CurseForgeRetrieverTest {

    @Test
    public void successfully_retrieves_mod_metadata_for_version() {
        assertNotNull(CurseForgeRetriever.retrieve("astral-sorcery", "1.12.2"));
    }

    @Test
    public void successfully_finds_specific_mod_version() {
        assertNotNull(CurseForgeRetriever.retrieve("astral-sorcery", 2716590, "1.12.2"));
    }

    @Test
    public void successfully_retrieves_a_mod_that_deals_with_double_encoding() {
        assertNotNull(CurseForgeRetriever.retrieve("pams-harvestcraft", "1.12.2"));
    }
}
