package com.squedgy.mc.mod.manager.forge;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;

public class ForgeClientJsonTest {

    @Test
    public void parses_pre_13_forge_version_json() throws IOException {
        Pre13ForgeClientJson file = objectMapper().readValue(getClass().getResourceAsStream("forge.json"), Pre13ForgeClientJson.class);
    }

    @Test
    public void parses_post_13_forge_version_json() throws IOException {
        Post13ForgeClientJson file = objectMapper().readValue(getClass().getResourceAsStream("new-forge.json"),
                                                              Post13ForgeClientJson.class
        );
    }
}
