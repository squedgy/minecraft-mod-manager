package com.squedgy.mc.mod.manager.forge.install;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.forge.Forge;
import com.squedgy.mc.mod.manager.utils.FormattedRuntimeException;
import com.squedgy.mc.mod.manager.utils.io.IO;
import com.squedgy.mc.mod.manager.version.json.ClientJson;
import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.library.Artifact;
import com.squedgy.mc.mod.manager.version.minecraft.Icon;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import org.slf4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import static com.squedgy.mc.mod.manager.version.library.Artifact.pathFromName;
import static org.slf4j.LoggerFactory.getLogger;

public class ForgeInstance extends MinecraftInstance {
    // http://files.minecraftforge.net/fmllibs/fml_libs.zip pre 1.5.x libs that you prolly can't find
    // http://files.minecraftforge.net/fmllibs/fml_libs15.zip 1.5.x libs that you prolly cant' find
    private static final Logger LOG = getLogger(ForgeInstance.class);

    public final InstallProfile installProfile;
    public final Forge.ForgeVersion forgeVersion;

    @JsonCreator
    public ForgeInstance(String name,
                         String minecraftVersion,
                         InstallProfile installProfile,
                         ClientJson versionInfo,
                         Forge.ForgeVersion forgeVersion,
                         Icon icon) {
        super(name, minecraftVersion, versionInfo, icon);
        this.installProfile = installProfile;
        this.forgeVersion = forgeVersion;
    }

    @Override
    public List<Artifact> getMissingArtifacts() {
        List<Artifact> base = new LinkedList<>(super.getMissingArtifacts());

        installProfile.libraries.forEach(l -> base.addAll(l.getMissingArtifacts()));

        forgeVersion.installer.ifPresent(installer -> {
            if(installer.doesNotExist()) base.add(installer);
        });

        return base;
    }

    @Override
    public List<Path> instanceClassPath(boolean client) {
        List<Path> base = new LinkedList<>(super.instanceClassPath(client));
        //        if(installProfile != null) {
        //            try {
        //                base.addAll(installProfile.getOutputs(this, client));
        //            } catch(IOException e) {
        //                throw new FormattedRuntimeException("Failed to get install profile's outputs.", e);
        //            }
        //        }
        Path attempt = ConfigPaths.getLibraryPath()
                                  .resolve(getRelativeInstallerPath());
        if(attempt.toFile()
                  .exists()) base.add(attempt);

        return base;
    }

    @Override
    public List<Runnable> afterDownloads() {
        List<Runnable> threads = new LinkedList<>(super.afterDownloads());

        versionInfo.libraries.stream()
                             .map(Library::getArtifacts)
                             .flatMap(List::stream)
                             .forEach(artifact -> {
                                 Path libraryPath = ConfigPaths.getLibraryPath();
                                 Path parentJar = libraryPath.resolve(artifact.getPath());
                                 artifact.getMerge()
                                         .ifPresent(merge -> threads.add(() -> merge.forEach(art -> {
                                             Path childPath = libraryPath.resolve(art.getPath());
                                             try {
                                                 IO.mergeJars(childPath, parentJar);
                                             } catch(IOException e) {
                                                 throw new UncheckedIOException(e);
                                             }
                                         })));
                             });
        threads.add(() -> {
            if(installProfile != null) {
                try {
                    installProfile.process(this, true);
                } catch(IOException e) {
                    throw new FormattedRuntimeException("Failed to process install profile {}", installProfile.version, e);
                }
            }
        });
        threads.add((() -> {
            Path installer = ConfigPaths.getLibraryPath()
                                        .resolve(pathFromName("net.minecraftforge:forge:" + getVersion() + ":installer"));
            if(installer.toFile().exists()) {
                String pathTo = getRelativeInstallerPath();
                String forgeLauncher = "maven/net/minecraftforge/forge/" + getVersion() + "/forge-" + getVersion() + ".jar";
                try {
                    IO.getJarEntry(installer, forgeLauncher, launcherStream -> {
                        Path target = ConfigPaths.getLibraryPath()
                                                 .resolve(pathTo);
                        File parent = target.getParent()
                                            .toFile();
                        if(parent.mkdirs() || parent.exists()) Files.copy(
                            launcherStream,
                            ConfigPaths.getLibraryPath()
                                       .resolve(pathTo)
                        );
                        else throw new IOException("Failed to create launcher's parent dir: " + parent);
                    });
                } catch(FileNotFoundException e) {
                    LOG.warn("Found no forge launcher in the installer at \"{}\"", installer);
                } catch(IOException e) {
                    throw new FormattedRuntimeException("Failed to copy/find forge launcher");
                }
            } else {
                LOG.warn("No forge launch");
            }
        }));

        return threads;
    }

    private String getRelativeInstallerPath() {
        return pathFromName("net.minecraftforge:forge-launcher:" + getVersion());
    }

    @Override
    public String getVersion() {
        return minecraftVersion + "-" + forgeVersion.version;
    }

    @Override
    public String toString() {
        return getName() + "@" + forgeVersion + "@MC" + minecraftVersion;
    }
}
