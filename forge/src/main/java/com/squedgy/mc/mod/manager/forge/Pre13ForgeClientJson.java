package com.squedgy.mc.mod.manager.forge;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squedgy.mc.mod.manager.forge.install.OldForgeLibrary;
import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.version.Arguments;
import com.squedgy.mc.mod.manager.version.AssetIndex;
import com.squedgy.mc.mod.manager.version.LoggingInfo;
import com.squedgy.mc.mod.manager.version.json.ClientJson;
import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.library.Artifact;
import com.squedgy.mc.mod.manager.version.library.LibraryDownloads;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import com.squedgy.mc.mod.manager.version.minecraft.UnknownVersionException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.squedgy.mc.mod.manager.version.library.Artifact.pathFromName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pre13ForgeClientJson extends ClientJson {

    public Pre13ForgeClientJson(String id,
                                String mainClass,
                                String type,
                                String assets,
                                String releaseTime,
                                String time,
                                AssetIndex assetIndex,
                                Collection<Library> libraries,
                                Optional<LoggingInfo> logging,
                                Arguments arguments) {
        super(id, mainClass, type, assets, releaseTime, time, assetIndex, libraries, logging, arguments);
    }

    protected static String getMinecraftVersion(Optional<String> inheritsFrom, String id) {
        if(inheritsFrom.isPresent()) return inheritsFrom.get();
        if(id.contains("-")) return id.substring(0, id.indexOf('-'));

        // this is an assumption as 1.5.2 has it in the id, and 1.6.2 (and from what I saw following versions) have the <mcVersion>-[fF]orge...
        return "1.6.1";
    }

    protected static Forge.ForgeVersion getForgeVersion(String minecraftVersion, String id) throws
                                                                                            InterruptedException,
                                                                                            IOException,
                                                                                            URISyntaxException,
                                                                                            UnknownVersionException {
        if(id.toLowerCase()
             .startsWith("forge")) return Forge.getForgeVersion(minecraftVersion, id.substring("forge".length()));
        String[] parts = id.split("-");
        if(parts.length == 2) return getForgeVersion(minecraftVersion, parts[1]);
        if(parts.length == 3) return Forge.getForgeVersion(minecraftVersion, parts[2]);

        throw new UnknownVersionException("Unknown forge version: {}", id);
    }

    protected static Collection<Library> mergeLibraries(Forge.ForgeVersion forgeVersion,
                                                        Collection<Library> minecraftLibraries,
                                                        List<OldForgeLibrary> libraries) {
        // this will contain libraries will all relevant information as compared to forge's info, but only for actual mc deps
        var mcLibraries = minecraftLibraries.stream()
                                            .collect(Collectors.toMap(l -> l.getName(),
                                                                      Function.identity(),
                                                                      Pre13ForgeClientJson::mergeLibraries,
                                                                      HashMap::new
                                            ));
        libraries.forEach(l -> {
            if(l.givenUrl.isPresent()) {
                if(mcLibraries.containsKey(l.getName())) {
                    mcLibraries.put(l.getName(), mergeLibraries(l, mcLibraries.get(l.getName())));
                } else {
                    mcLibraries.put(l.getName(), l);
                }
            }
        });

        String forgeName = "net.minecraftforge:forge:" + forgeVersion.version;
        String clientName = forgeVersion.client.getPath();

        Artifact clientArtifact = forgeVersion.client;
        LibraryDownloads clientDownloads = new LibraryDownloads(Optional.of(clientArtifact), Optional.empty());
        boolean forServer = forgeVersion.server.map(s -> s.equals(forgeVersion.client))
                                               .orElse(false);
        Library client = new Library(clientName, clientDownloads, true, forServer);

        mcLibraries.remove(forgeName);
        mcLibraries.put(clientName, client);

        if(!forServer && forgeVersion.server.isPresent()) {
            String serverName = forgeVersion.server.get()
                                                   .getPath();
            LibraryDownloads serverDownloads = new LibraryDownloads(forgeVersion.server, Optional.empty());
            Library server = new Library(serverName, serverDownloads, false, true);
            mcLibraries.put(serverName, server);
        }

        return mcLibraries.values();
    }

    @JsonCreator
    public static Pre13ForgeClientJson of(String id,
                                          String mainClass,
                                          String type,
                                          String time,
                                          String releaseTime,
                                          String minecraftArguments,
                                          Optional<String> inheritsFrom,
                                          List<OldForgeLibrary> libraries,
                                          Optional<LoggingInfo> logging) throws
                                                                         UnknownVersionException,
                                                                         InterruptedException,
                                                                         IOException,
                                                                         RequestException,
                                                                         URISyntaxException {
        String minecraftVersion = getMinecraftVersion(inheritsFrom, id);
        ClientJson mcVersion = MinecraftInstance.getMinecraftVersionJsonFile(minecraftVersion);
        Forge.ForgeVersion forgeVersion = getForgeVersion(minecraftVersion, id);
        String assets = mcVersion.assets;
        AssetIndex assetIndex = mcVersion.assetIndex;
        var totalLibs = mergeLibraries(forgeVersion, mcVersion.libraries, libraries);

        return new Pre13ForgeClientJson(id,
                                        mainClass,
                                        type,
                                        assets,
                                        releaseTime,
                                        time,
                                        assetIndex,
                                        totalLibs,
                                        mcVersion.logging.or(() -> logging),
                                        Arguments.fromMcArgs(minecraftArguments)
        );
    }

    public static Pre13ForgeClientJson of(Forge.ForgeVersion forgeVersion, String minecraftVersion) throws
                                                                                                    InterruptedException,
                                                                                                    URISyntaxException,
                                                                                                    RequestException,
                                                                                                    IOException,
                                                                                                    UnknownVersionException {
        ClientJson mcVersion = MinecraftInstance.getMinecraftVersionJsonFile(minecraftVersion);
        String id = minecraftVersion + "-forge" + minecraftVersion + "-" + forgeVersion.version;
        String mainClass = mcVersion.mainClass;
        String type = mcVersion.type;
        String assets = mcVersion.assets;
        String time = mcVersion.time;
        // 1960-01-01T00:00:00-0700
        String releaseTime = forgeVersion.releaseTime.atZone(ZoneId.systemDefault())
                                                     .format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssZZZZZ"));
        AssetIndex assetIndex = mcVersion.assetIndex;
        var libraries = mcVersion.libraries;
        if(forgeVersion.requiresMerge(true)) {
            String clientName = "net.minecraft:client:" + id;
            String serverName = "net.minecraft:server:" + id;
            Library clientBase = libraries.stream()
                                          .filter(l -> l.getName()
                                                        .startsWith("net.minecraft:client:"))
                                          .findAny()
                                          .orElseThrow(() -> new UnknownVersionException(
                                              "Couldn't find a client jar for minecraft version: " + minecraftVersion));
            Library serverBase = libraries.stream()
                                          .filter(l -> l.getName()
                                                        .startsWith("net.minecraft:server:"))
                                          .findAny()
                                          .orElse(null);

            libraries = libraries.stream()
                                 .filter(l -> !l.getName()
                                                .startsWith("net.minecraft:client") &&
                                              !l.getName()
                                                .startsWith("net.minecraft:server"))
                                 .collect(Collectors.toCollection(LinkedList::new));
            Artifact baseClientArt = clientBase.getDownloads()
                                               .getArtifact()
                                               .get();
            List<Artifact> merge = new LinkedList<>();
            merge.add(forgeVersion.client);
            baseClientArt.getMerge()
                         .ifPresent(merge::addAll);

            Library client = new Library(clientName, new LibraryDownloads(
                Optional.of(new Artifact(pathFromName(clientName),
                                         baseClientArt.getSha1(),
                                         baseClientArt.getUrl(),
                                         baseClientArt.size,
                                         Optional.of(merge)
                )),
                clientBase.getDownloads()
                          .getClassifiers()
            ), clientBase.getExtract(), clientBase.getNatives(), clientBase.getRules(), clientBase.isForClient(), clientBase.isForServer());
            libraries.add(client);
            if(serverBase != null && forgeVersion.server.isPresent()) {
                Artifact baseServerArt = clientBase.getDownloads()
                                                   .getArtifact()
                                                   .get();
                List<Artifact> serverMerge = new LinkedList<>();
                merge.add(forgeVersion.server.get());
                baseServerArt.getMerge()
                             .ifPresent(serverMerge::addAll);

                Library server = new Library(serverName,
                                             new LibraryDownloads(Optional.of(new Artifact(pathFromName(serverName),
                                                                                           baseServerArt.getSha1(),
                                                                                           baseServerArt.getUrl(),
                                                                                           baseServerArt.size,
                                                                                           Optional.of(serverMerge)
                                             )),
                                                                  serverBase.getDownloads()
                                                                            .getClassifiers()
                                             ),
                                             serverBase.getExtract(),
                                             serverBase.getNatives(),
                                             serverBase.getRules(),
                                             serverBase.isForClient(),
                                             serverBase.isForServer()
                );
                libraries.add(server);
            }
        }

        return new Pre13ForgeClientJson(id,
                                        mainClass,
                                        type,
                                        assets,
                                        releaseTime,
                                        time,
                                        assetIndex,
                                        libraries,
                                        Optional.empty(),
                                        mcVersion.arguments
        );
    }

    public static <A> Optional<A> oneOf(Optional<A> a, Optional<A> b) {
        return a.or(() -> b);
    }

    public static Library mergeLibraries(Library a, Library b) {
        var r1 = a.getRules();
        var r2 = b.getRules();
        var rules = r1.size() > r2.size() ? r1 : r2;
        var downloads = new LibraryDownloads(oneOf(a.getDownloads()
                                                    .getArtifact(),
                                                   b.getDownloads()
                                                    .getArtifact()
        ),
                                             oneOf(a.getDownloads()
                                                    .getClassifiers(),
                                                   b.getDownloads()
                                                    .getClassifiers()
                                             )
        );

        return new Library(a.getName(),
                           downloads,
                           oneOf(a.getExtract(), b.getExtract()),
                           oneOf(a.getNatives(), b.getNatives()),
                           rules,
                           a.isForClient(),
                           a.isForServer()
        );
    }
}

