package com.squedgy.mc.mod.manager.forge;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.squedgy.mc.mod.manager.http.Client;
import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.version.library.Artifact;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.squedgy.mc.mod.manager.http.Client.head;
import static com.squedgy.mc.mod.manager.version.library.Artifact.pathFromName;
import static com.squedgy.mc.mod.manager.version.minecraft.VersionManifest.Version.GENERAL_DATE_TIME_FORMAT;
import static java.util.Optional.empty;

public abstract class Forge {
    private static final String FORGE_URL_TEMPLATE = "https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_%s.html";

    public static ForgeVersion getForgeVersion(String minecraftVersion, String forgeVersion) throws
                                                                                             IOException,
                                                                                             URISyntaxException,
                                                                                             InterruptedException {
        return getForgeVersions(minecraftVersion).stream()
                                                 .filter(f -> f.version.equals(forgeVersion))
                                                 .findFirst()
                                                 .get()
                                                 .toForgeVersion();
    }

    public static List<DisplayForgeVersion> getForgeVersions(String minecraftVersion) throws IOException {
        String relativeMcVersion = minecraftVersion.replace("-", "_");
        String url = String.format(FORGE_URL_TEMPLATE, relativeMcVersion);
        Document doc = Jsoup.connect(url)
                            .get();
        return parseForgeDocument(relativeMcVersion, doc);
    }

    static List<DisplayForgeVersion> parseForgeDocument(String minecraftVersion, Document forge) {
        Elements rows = forge.select("table.download-list tbody tr");
        List<DisplayForgeVersion> versions = new LinkedList<>();

        rows.forEach(row -> {
            Element version = row.selectFirst("td.download-version"), time = removeChildren(row.selectFirst("td.download-time"));

            versions.add(new DisplayForgeVersion(minecraftVersion,
                                                 getElementVersion(version),
                                                 LocalDateTime.parse(time.text(), DateTimeFormatter.ofPattern("MM/dd/uu hh:mm a"))
            ));
        });

        return versions;
    }

    private static Element removeChildren(Element element) {
        for(var child : element.children()) {
            child.remove();
        }
        return element;
    }

    private static String getElementVersion(Element version) {
        String ver = version.text();
        if(ver.contains("Branch:")) {
            ver = ver.replaceFirst(" *Branch: *([^ ]+)", "-$1")
                     .toLowerCase();
        }
        return ver;
    }

    public static class DisplayForgeVersion {
        public final String version;
        public final String minecraftVersion;
        public final LocalDateTime releaseTime;

        public DisplayForgeVersion(String minecraftVersion, String version, LocalDateTime releaseTime) {
            this.minecraftVersion = minecraftVersion;
            this.version = version;
            this.releaseTime = releaseTime;
        }

        public ForgeVersion toForgeVersion() throws InterruptedException, IOException, URISyntaxException {
            return new ForgeVersion(minecraftVersion, version, releaseTime);
        }

        @JsonIgnore
        public String getVersion() {
            return version;
        }

        @JsonIgnore
        public String getFormattedReleaseTime() {
            return releaseTime.format(GENERAL_DATE_TIME_FORMAT);
        }

        @Override
        public int hashCode() {
            return Objects.hash(version, minecraftVersion, releaseTime);
        }

        @Override
        public boolean equals(Object o) {
            if(this == o) return true;
            if(o == null || getClass() != o.getClass()) return false;
            DisplayForgeVersion that = (DisplayForgeVersion) o;
            return Objects.equals(version, that.version) &&
                   Objects.equals(minecraftVersion, that.minecraftVersion) &&
                   Objects.equals(releaseTime, that.releaseTime);
        }
    }

    public static class ForgeVersion {
        private final static String FORGE_VERSION_URL_TEMPLATE = "https://files.minecraftforge.net/maven/net/minecraftforge/forge/%1$s-%2$s/forge-%1$s-%2$s-%3$s.%4$s";
        public final String version;
        public final LocalDateTime releaseTime;
        public final Optional<Artifact> installer, server;
        public final Artifact client;

        ForgeVersion(String minecraftVersion, String forgeVersion, LocalDateTime releaseTime) throws
                                                                                              InterruptedException,
                                                                                              IOException,
                                                                                              URISyntaxException {
            this(forgeVersion, releaseTime, findArtifact(minecraftVersion + '-' + forgeVersion,
                                                         String.format(FORGE_VERSION_URL_TEMPLATE,
                                                                       minecraftVersion,
                                                                       forgeVersion,
                                                                       "installer",
                                                                       "jar"
                                                         )
            ), findArtifact(minecraftVersion + '-' + forgeVersion,
                            String.format(FORGE_VERSION_URL_TEMPLATE, minecraftVersion, forgeVersion, "universal", "jar"),
                            String.format(FORGE_VERSION_URL_TEMPLATE, minecraftVersion, forgeVersion, "universal", "zip"),
                            String.format(FORGE_VERSION_URL_TEMPLATE, minecraftVersion, forgeVersion, "client", "zip")
            ).orElseThrow(), findArtifact(minecraftVersion + '-' + forgeVersion,
                                          String.format(FORGE_VERSION_URL_TEMPLATE, minecraftVersion, forgeVersion, "universal", "jar"),
                                          String.format(FORGE_VERSION_URL_TEMPLATE, minecraftVersion, forgeVersion, "universal", "zip"),
                                          String.format(FORGE_VERSION_URL_TEMPLATE, minecraftVersion, forgeVersion, "server", "zip")
            ));
        }

        @JsonCreator
        public ForgeVersion(String version,
                            LocalDateTime releaseTime,
                            Optional<Artifact> installer,
                            Artifact client,
                            Optional<Artifact> server) {
            this.version = version;
            this.releaseTime = releaseTime;
            this.installer = installer;
            this.client = client;
            this.server = server;
        }

        private static Optional<Artifact> findArtifact(String version, String... links) throws
                                                                                        InterruptedException,
                                                                                        IOException,
                                                                                        URISyntaxException {
            String actualLink = checkLink(links);
            if(actualLink == null) return empty();
            String classifier = actualLink.replaceAll(".*-([a-zA-Z]+)\\.jar", "$1");
            return Optional.of(new Artifact(pathFromName("net.minecraftforge:forge:" + version + ":" + classifier), actualLink));
        }

        private static String checkLink(String... links) throws InterruptedException, IOException, URISyntaxException {
            for(String link : links) {
                try {
                    Client.enforceRequest(head(link).build());
                    return link;
                } catch(RequestException e) {}
            }
            return null;
        }

        public String getVersion() {
            return version;
        }

        public LocalDateTime getReleaseTime() {
            return releaseTime;
        }

        public boolean requiresMerge(boolean client) {
            return client ? this.client.isZip() : this.server.map(Artifact::isZip)
                                                             .orElse(false);
        }

        /**
         * does this forge version have an installer?
         */
        public boolean hasInstaller() {
            return installer.isPresent();
        }

        @Override
        public int hashCode() {
            return Objects.hash(version, releaseTime);
        }

        @Override
        public boolean equals(Object o) {
            if(this == o) return true;
            if(o == null || getClass() != o.getClass()) return false;
            ForgeVersion that = (ForgeVersion) o;
            return Objects.equals(version, that.version) && Objects.equals(releaseTime, that.releaseTime);
        }

        @Override
        public String toString() {
            return "Forge" + version + "@" + releaseTime;
        }

    }
}
