package com.squedgy.mc.mod.manager.forge.install;

import com.squedgy.mc.mod.manager.utils.FormattedRuntimeException;

public class UnknownMapping extends FormattedRuntimeException {

    public UnknownMapping(String message) {
        super(message);
    }

    public UnknownMapping(String message, Object... args) {
        super(message, args);
    }

    public UnknownMapping(String message, Exception ex) {
        super(message, ex);
    }
}
