package com.squedgy.mc.mod.manager.forge;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Mapping {
    private static final Pattern parsablePattern = Pattern.compile("\\{[^.}]+(\\.[_a-zA-Z][a-zA-Z_0-9]*)*}");
    private final String client, server;

    public Mapping(String client, String server) {
        this.client = client;
        this.server = server;
    }

    public String getClient() {
        return client;
    }

    public String getServer() {
        return server;
    }

    /**
     * @param client is this parse for the client value?
     * @param values arguments that a mapping should be expecting
     *
     * @throws ArrayIndexOutOfBoundsException if a parsed index for base value is out of bounds
     * @throws IllegalArgumentException       If a mapping's field/method reference isn't found
     * @throws NullPointerException           If the a retrieved value results in null
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public String parseValue(boolean client, Map<String, Object> values) throws InvocationTargetException, IllegalAccessException {
        String toParse = client ? this.client : this.server;
        Matcher matcher = parsablePattern.matcher(toParse);

        StringBuilder actualValue = new StringBuilder();
        MatchResult[] results = matcher.results()
                                       .toArray(MatchResult[]::new);
        System.out.println("RESULTS: " + results.length);
        int start = 0;
        for(MatchResult result : results) {
            if(result.start() != start) actualValue.append(toParse, start, result.start());
            actualValue.append(valueOf(result.group(), values));
            start = result.end();
        }
        if(start != toParse.length()) {
            actualValue.append(toParse, start, toParse.length());
        }

        return actualValue.toString();
    }

    private String valueOf(String group, Map<String, Object> values) throws InvocationTargetException, IllegalAccessException {
        String actual = group.substring(1, group.length() - 1);
        String[] pieces = actual.split("\\.");
        String item = pieces[0];
        Object value = values.get(item);
        if(value == null)
            throw new NullPointerException("Failed to retrieve valid output from piece " + pieces[0] + " in: " + Arrays.toString(pieces));

        for(int i = 1; i < pieces.length; i++) {
            value = retrieveOff(value, pieces[i]);
            if(value == null) throw new NullPointerException("Failed to retrieve valid output from piece " +
                                                             pieces[i] +
                                                             " in: " +
                                                             Arrays.toString(pieces));
        }

        return value.toString();
    }

    private Object retrieveOff(Object thing, String action) throws InvocationTargetException, IllegalAccessException {
        Class<?> clazz = thing.getClass();
        Method[] methods = clazz.getMethods();
        for(Method m : methods) {
            boolean nameMatches = m.getName()
                                   .equals(action);
            boolean hasNoParameters = m.getParameterCount() == 0;
            if(nameMatches && hasNoParameters) {
                if(!m.canAccess(thing) || !Modifier.isPublic(m.getModifiers()))
                    throw new IllegalAccessException("DataMappings has no access to class " +
                                                     clazz.getSimpleName() +
                                                     "'s method " +
                                                     action);
                return m.invoke(thing);
            }
        }

        Field[] fields = clazz.getFields();
        for(Field f : fields) {
            boolean nameMatches = f.getName()
                                   .equals(action);
            if(nameMatches) {
                if(!f.canAccess(thing) || !Modifier.isPublic(f.getModifiers()))
                    throw new IllegalAccessException("DataMappings has no access to " +
                                                     clazz.getSimpleName() +
                                                     "'s instance member " +
                                                     action);
                return f.get(thing);
            }
        }

        throw new IllegalArgumentException("Passed action \"" +
                                           action +
                                           "\" is not an argumentless method or public field on " +
                                           clazz.getSimpleName());
    }

    @Override
    public String toString() {
        return "{" + "client='" + client + '\'' + ", server='" + server + '\'' + '}';
    }
}
