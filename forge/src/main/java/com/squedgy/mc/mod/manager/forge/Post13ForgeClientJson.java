package com.squedgy.mc.mod.manager.forge;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squedgy.mc.mod.manager.http.RequestException;
import com.squedgy.mc.mod.manager.version.Arguments;
import com.squedgy.mc.mod.manager.version.json.ClientJson;
import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftLibrary;
import com.squedgy.mc.mod.manager.version.minecraft.UnknownVersionException;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance.getMinecraftVersionJsonFile;
import static org.slf4j.LoggerFactory.getLogger;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Post13ForgeClientJson extends ClientJson {
    private static final Logger LOG = getLogger(Pre13ForgeClientJson.class);

    @JsonCreator
    public Post13ForgeClientJson(List<MinecraftLibrary> libraries,
                                 String id,
                                 String mainClass,
                                 String type,
                                 String releaseTime,
                                 String time,
                                 Arguments arguments,
                                 String inheritsFrom) throws
                                                      InterruptedException,
                                                      URISyntaxException,
                                                      UnknownVersionException,
                                                      RequestException,
                                                      IOException {
        this(libraries, id, mainClass, type, releaseTime, time, arguments, getMinecraftVersionJsonFile(inheritsFrom));
    }

    protected Post13ForgeClientJson(List<MinecraftLibrary> libraries,
                                    String id,
                                    String mainClass,
                                    String type,
                                    String releaseTime,
                                    String time,
                                    Arguments arguments,
                                    ClientJson inheritsFrom) {
        super(
            id,
            mainClass,
            type,
            inheritsFrom.assets,
            releaseTime,
            time,
            inheritsFrom.assetIndex,
            merge(libraries, inheritsFrom.libraries),
            inheritsFrom.logging,
            Arguments.joining(arguments, inheritsFrom.arguments)
        );
    }

    private static List<Library> merge(List<MinecraftLibrary> list, Collection<Library> include) {
        LOG.trace("merging\n{}\n{}", list, include);
        var map = new HashMap<String, Library>();
        list.forEach(lib -> {
            if(!map.containsKey(lib.getName())) map.put(lib.getName(), lib);
            else map.put(lib.getName(), Library.joining(lib, map.get(lib.getName())));
        });

        include.forEach(lib -> {
            if(!map.containsKey(lib.getName())) map.put(lib.getName(), lib);
            else map.put(lib.getName(), Library.joining(lib, map.get(lib.getName())));
        });

        LOG.trace("merged into {}", map.values());
        return new ArrayList<>(map.values());
    }
}
