package com.squedgy.mc.mod.manager.forge;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.squedgy.mc.mod.manager.http.Client;
import com.squedgy.mc.mod.manager.version.mods.ModMetadata;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static org.slf4j.LoggerFactory.getLogger;

public final class CurseForgeRetriever {
    private static Logger LOG = getLogger(CurseForgeRetriever.class);

    private CurseForgeRetriever() {}

    public static ModMetadata retrieve(String modId, String version) {
        try {
            JsonNode root = getRootNodeFromResponse(modId);
            String responseType = root.get("type")
                                      .textValue();

            if(!responseType.equals("Mods")) {
                throw new IOException("Given mod Id [" + modId + "] is not a mod. Received type [" + responseType + "].");
            }

            ArrayNode files = (ArrayNode) root.get("files");
            JsonNode firstFile = null;
            for(var file : files) {
                ArrayNode versions = (ArrayNode) file.get("versions");
                if(arrayNodeContains(
                    versions,
                    n -> n.isTextual() &&
                         n.textValue()
                          .equals(version)
                )) {
                    firstFile = file;
                    break;
                }
            }

            return parseFileNode(modId, version, firstFile);
        } catch(InterruptedException | IOException e) {
            LOG.error("Failed to retrieve mod with Id: {}", modId, e);
        } catch(ClassCastException e) {
            LOG.error("A node in the response was in an unexpected format", e);
        } catch(Exception e) {
            LOG.error("An unexpected exception occured.", e);
        }
        return null;
    }

    private static JsonNode getRootNodeFromResponse(String modId) throws IOException, InterruptedException, URISyntaxException {
        URI uri = getModUri(modId);
        LOG.info("Retrieving: {}", uri);
        HttpRequest request = HttpRequest.newBuilder(uri)
                                         .GET()
                                         .build();

        HttpResponse<InputStream> response = Client.sendRequest(request);
        InputStream responseStream = response.body();

        if(response.statusCode() == 500) {
            throw new InterruptedException("There seems to be an issue with curse forge's api currently.");
        }
        if(response.statusCode() < 200 || response.statusCode() >= 300) {
            throw new InterruptedException("Failed with status code [" + response.statusCode() + "]: {}");
        }

        LOG.debug("Response Headers: {}", response.headers());

        return objectMapper().readTree(responseStream);
    }

    private static boolean arrayNodeContains(ArrayNode node, Function<JsonNode, Boolean> fieldMatches) {
        JsonNode item = node.get(0);
        for(int i = 0; i < node.size(); ++i, item = node.get(i)) {
            if(fieldMatches.apply(item)) return true;
        }
        return false;
    }

    private static ModMetadata parseFileNode(String modId, String version, JsonNode foundFile) throws IOException {
        if(foundFile == null) throw new IOException("No file for mod with id [" + modId + "] has version [" + version + "]");

        URI downloadUri = URI.create(foundFile.get("url")
                                              .textValue() + "/file");

        HashSet<String> versions = new HashSet<>();

        ArrayNode fileVersions = (ArrayNode) foundFile.get("versions");
        fileVersions.forEach(v -> versions.add(v.textValue()));

        String fileName = foundFile.get("name")
                                   .textValue();
        String formattedName = "curseforge-" + modId + "[" + version + "].jar";

        ModMetadata modMetadata = new ModMetadata(fileName, formattedName, downloadUri, versions);

        LOG.info("Retrieved [{}].\nMetadata: {}", getModUri(modId), modMetadata);

        return modMetadata;
    }

    private static URI getModUri(String modId) {
        return URI.create("https://api.cfwidget.com/mc-mods/minecraft/" + modId);
    }

    public static ModMetadata retrieve(String modId, long modVersion, String version) {
        try {
            JsonNode root = getRootNodeFromResponse(modId);
            String responseType = root.get("type")
                                      .textValue();

            if(!responseType.equals("Mods")) {
                throw new IOException("Given mod Id [" + modId + "] is not a mod. Received type [" + responseType + "].");
            }

            ArrayNode files = (ArrayNode) root.get("files");
            AtomicReference<JsonNode> firstFile = new AtomicReference<>(null);
            files.forEach(file -> {
                if(firstFile.get() == null) {
                    long id = file.get("id")
                                  .longValue();
                    if(id == modVersion) firstFile.set(file);
                }
            });

            return parseFileNode(modId, version, firstFile.get());
        } catch(InterruptedException | IOException e) {
            LOG.error("Failed to retrieve mod with Id: {}", modId, e);
        } catch(ClassCastException e) {
            LOG.error("A node in the response was in an unexpected format", e);
        } catch(Exception e) {
            LOG.error("An unexpected exception occured.", e);
        }
        return null;
    }
}
