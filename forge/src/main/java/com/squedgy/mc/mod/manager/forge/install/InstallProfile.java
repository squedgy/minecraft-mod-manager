package com.squedgy.mc.mod.manager.forge.install;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;
import com.squedgy.mc.mod.manager.forge.DataMappings;
import com.squedgy.mc.mod.manager.forge.Mapping;
import com.squedgy.mc.mod.manager.utils.FormattedRuntimeException;
import com.squedgy.mc.mod.manager.utils.io.IO;
import com.squedgy.mc.mod.manager.utils.io.Security;
import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftInstance;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import static com.squedgy.mc.mod.manager.version.library.Artifact.pathFromName;
import static java.util.jar.Attributes.Name.MAIN_CLASS;
import static java.util.stream.Collectors.toMap;
import static org.slf4j.LoggerFactory.getLogger;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class InstallProfile {
    private static final Logger LOG = getLogger(InstallProfile.class);

    public final Map<String, ArgumentTuple> data;
    public final int spec;
    public final String version, json, path, minecraft;
    public final Collection<Library> libraries;
    public final Collection<ForgeInstallProcessor> processors;

    private Map<String, String> currentData;
    private Map<String, Object> currentValues;
    private DataMappings knownMappings;
    private Path tempDir;

    public InstallProfile(Map<String, ArgumentTuple> data,
                          int spec,
                          String version,
                          String json,
                          String path,
                          String minecraft,
                          Collection<Library> libraries,
                          Collection<ForgeInstallProcessor> processors) {
        this.data = data;
        this.spec = spec;
        this.version = version;
        this.json = json;
        this.path = path;
        this.minecraft = minecraft;
        this.libraries = libraries;
        this.processors = processors;

    }

    public void process(MinecraftInstance instance, boolean client) throws IOException {
        setData(instance, client);

        processors.forEach(proc -> {
            var output = getOutput(proc, client);
            LOG.info("Running processor {}\n  With output: {}", proc.jar, output);

            if(output.size() > 0 &&
               output.entrySet()
                     .stream()
                     .allMatch(e -> new File(e.getKey()).exists())) {
                return;
            }

            Path jar = ConfigPaths.getLibraryPath()
                                  .resolve(pathFromName(proc.jar));
            if(!jar.toFile()
                   .exists() ||
               !jar.toFile()
                   .isFile()) throw new BadProcessor("Processor \"{}\" doesn't exist!", proc.jar);

            String mainClass = null;
            try(JarFile processor = new JarFile(jar.toFile())) {
                mainClass = processor.getManifest()
                                     .getMainAttributes()
                                     .getValue(MAIN_CLASS);
            } catch(IOException ignored) {}

            if(mainClass == null || mainClass.isEmpty()) throw new BadProcessor("Processor \"{}\" doesn't have a main class!", proc.jar);

            List<URL> classPath = new LinkedList<>();
            try {
                classPath.add(jar.toUri()
                                 .toURL());
                for(String art : proc.classpath) {
                    Path p = ConfigPaths.getLibraryPath()
                                        .resolve(pathFromName(art));
                    if(!p.toFile()
                         .exists() ||
                       !p.toFile()
                         .isFile()) throw new Exception(art);
                    classPath.add(p.toUri()
                                   .toURL());
                }

            } catch(Exception e) {
                throw new BadProcessor("Processor \"{}\"'s classpath contained a bad classpath field: {}", proc.jar, e.getMessage(), e);
            }

            ClassLoader loader = new URLClassLoader(classPath.toArray(new URL[0]), getClassLoader());
            try {
                List<String> arguments = proc.args.stream()
                                                  .map(e -> mapOther(e, client))
                                                  .collect(Collectors.toList());
                LOG.info("Running processor {} with arguments:\n  {}", proc.jar, arguments);
                Class<?> cls = Class.forName(mainClass, true, loader);
                Method main = cls.getDeclaredMethod("main", String[].class);
                main.invoke(null, (Object) arguments.toArray(new String[0]));
            } catch(InvocationTargetException e) {
                LOG.error("Failed to invoke processor {}", proc.jar, e);
                throw new FormattedRuntimeException("Failed to invoke {}", proc.jar, e);
            } catch(Throwable any) {
                LOG.error("Failed to run processor {}", proc.jar, any);
                throw new FormattedRuntimeException("Processor {} threw an error ", proc.jar, any);
            }

            output.forEach((k, v) -> {
                Path result = Path.of(k);
                if(!result.toFile()
                          .exists()) {
                    throw new BadProcessor("Processor {} failed to create output {}", proc.jar, result);
                } else {
                    try {
                        String checksum = Security.forgeChecksum(result);
                        if(!checksum.equals(v)) {
                            Files.delete(result);
                            throw new BadProcessor("Processor {} output {} failed to match checksum", proc.jar, k);
                        }
                    } catch(NoSuchAlgorithmException | IOException e) {
                        LOG.error("Failed to verify output {} of processor {}.", k, proc.jar);
                    }
                }
            });
        });
    }

    private void setData(MinecraftInstance instance, boolean client) throws IOException {
        tempDir = Files.createTempDirectory(null);
        knownMappings = DataMappings.retrieve();
        currentValues = Map.of("instance",
                               instance,
                               "libraryDir",
                               ConfigPaths.getLibraryPath()
                                          .toString()
        );
        currentData = this.data.entrySet()
                               .stream()
                               .map(e -> {
                                   try {
                                       return new AbstractMap.SimpleEntry<>(e.getKey(),
                                                                            mapData(e.getValue()
                                                                                     .retrieveNeeded(client))
                                       );
                                   } catch(IOException ex) {
                                       throw new UncheckedIOException(ex);
                                   }
                               })
                               .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map<String, String> getOutput(ForgeInstallProcessor proc, boolean client) {
        BinaryOperator<String> join = (a, b) -> {
            LOG.trace("Merging {} and {}", a, b);
            return b;
        };

        return proc.output.map(out -> out.entrySet()
                                         .stream()
                                         .map(e -> new AbstractMap.SimpleEntry<>(mapOther(e.getKey(), client),
                                                                                 mapOther(e.getValue(), client)
                                         ))
                                         .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, join)))
                          .orElse(new HashMap<>());
    }

    private String mapData(String value) throws IOException {
        if(startAndEnd(value, '[', ']')) {
            return ConfigPaths.getLibraryPath()
                              .resolve(pathFromName(actualValue(value)))
                              .toString();
        } else if(startAndEnd(value, '\'', '\'')) {
            return actualValue(value);
        } else {
            String actual = value.startsWith("/") ? value.substring(1) : value;
            extractFromProfile(actual);
            return tempDir.resolve(actual)
                          .toString();
        }
    }

    private String mapOther(String value, boolean client) {
        LOG.trace("Mapping other value: \"{}\"", value);
        if(startAndEnd(value, '[', ']')) return ConfigPaths.getLibraryPath()
                                                           .resolve(pathFromName(actualValue(value)))
                                                           .toString();
        else if(startAndEnd(value, '\'', '\'')) return actualValue(value);
        else if(startAndEnd(value, '{', '}')) {
            String key = actualValue(value);
            String requested = currentData.get(key);
            if(requested != null) return requested;
            List<Mapping> mappings = knownMappings.getMappings(key);
            for(Mapping mapping : mappings) {
                try {
                    return mapping.parseValue(client, currentValues);
                } catch(IllegalAccessException | InvocationTargetException | IndexOutOfBoundsException | NullPointerException e) {
                    LOG.error("Mapping: {} failed", mapping, e);
                }
            }
            throw new UnknownMapping("Value {} has no valid mapping", value);
        } else if(value.startsWith("/")) return tempDir.resolve(value)
                                                       .toString();
        else return value;
    }

    private boolean startAndEnd(String string, char start, char end) {
        return string.charAt(0) == start && string.charAt(string.length() - 1) == end;
    }

    private String actualValue(String string) {
        return string.substring(1, string.length() - 1);
    }

    private void extractFromProfile(String path) throws IOException {
        String installerPath = pathFromName("net.minecraftforge:forge:" + forgeVersion() + ":installer");
        Path installer = ConfigPaths.getLibraryPath()
                                    .resolve(installerPath);
        if(!tempDir.resolve(path)
                   .toFile()
                   .exists()) {
            IO.getJarEntry(installer, path, input -> {
                var target = tempDir.resolve(path);
                var parent = target.getParent()
                                   .toFile();
                if(!parent.mkdirs() && !parent.exists()) throw new IOException("Failed to create directory " + parent);
                Files.copy(input, target);
            });
        }
    }

    private String forgeVersion() {
        String[] parts = path.split(":");
        return parts[parts.length - 1];
    }

    public static ClassLoader getClassLoader() {
        if(!System.getProperty("java.version")
                  .startsWith("1.")) {
            try {
                Method platform = ClassLoader.class.getDeclaredMethod("getPlatformClassLoader");
                return (ClassLoader) platform.invoke(null);
            } catch(NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                LOG.error("Failed to get platform classloader", e);
            }
        }
        return null;
    }

    public static final class BadProcessor extends FormattedRuntimeException {

        public BadProcessor(String message) {
            super(message);
        }

        public BadProcessor(String message, Object... args) {
            super(message, args);
        }

        public BadProcessor(String message, Exception ex) {
            super(message, ex);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_ABSENT)
    public static final class ForgeInstallProcessor {
        public final String jar;
        public final List<String> classpath, args;
        public final Optional<Map<String, String>> output;

        public ForgeInstallProcessor(String jar, List<String> classpath, List<String> args, Optional<Map<String, String>> outputs) {
            this.jar = jar;
            this.classpath = classpath;
            this.args = args;
            this.output = outputs;
        }

    }

}
