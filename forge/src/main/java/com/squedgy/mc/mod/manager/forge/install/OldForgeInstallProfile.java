package com.squedgy.mc.mod.manager.forge.install;

import com.squedgy.mc.mod.manager.forge.Pre13ForgeClientJson;
import com.squedgy.mc.mod.manager.version.json.ClientJson;

import java.util.List;
import java.util.Map;

public class OldForgeInstallProfile extends NewForgeInstallProfile {

    private ClientJson versionInfo;

    public OldForgeInstallProfile(ForgeInstall install, Pre13ForgeClientJson versionInfo) {
        super(Map.of(), 0, install.version, null, install.path, install.minecraft, List.of(), List.of());
        this.versionInfo = versionInfo;
    }

    public ClientJson retrieveVersionInfo() {
        return versionInfo;
    }
}
