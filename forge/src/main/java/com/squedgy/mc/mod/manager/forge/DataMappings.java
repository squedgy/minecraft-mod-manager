package com.squedgy.mc.mod.manager.forge;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;

public class DataMappings {
    public final Map<String, List<Mapping>> knownMappings;

    @JsonCreator
    private DataMappings(Map<String, List<Mapping>> knownMappings) {
        this.knownMappings = knownMappings;
    }

    public List<Mapping> getMappings(String key) {
        return knownMappings.get(key);
    }

    public static DataMappings retrieve() throws IOException {
        Path mappingsFile = ConfigPaths.getManagerDir()
                                       .resolve("mappings")
                                       .resolve("known-mappings.json");
        if(mappingsFile.toFile()
                       .exists()) {
            return objectMapper().readValue(mappingsFile.toFile(), DataMappings.class);
        }

        DataMappings def = new DataMappings(Map.ofEntries(entry(
            "MINECRAFT_JAR",
            // {libraryDir}/net/minecraft/client/1.15.2-forge-31.2.21/client-1.15.2-forge-31.2.21.jar
            new Mapping(
                "{libraryDir}/net/minecraft/client/{instance.minecraftVersion}/client-{instance.minecraftVersion}.jar",
                "{libraryDir}/net/minecraft/server/{instance.minecraftVersion}/client-{instance.minecraftVersion}.jar"
            )
        ), entry("MAPPINGS", new Mapping(
            "{libraryDir}/net/minecraft/client-mappings/{instance.minecraftVersion}/client-{instance.minecraftVersion}.jar",
            "{libraryDir}/net/minecraft/server-mappings/{instance.minecraftVersion}/client-{instance.minecraftVersion}.jar"
        ))));
        File parent = mappingsFile.getParent()
                                  .toFile();
        if(!parent.exists() && !parent.mkdirs() && !parent.exists())
            throw new IOException("Failed to create data mappings file's parent directory.");
        objectMapper().writeValue(mappingsFile.toFile(), def);
        return def;
    }

    private static Map.Entry<String, List<Mapping>> entry(String key, Mapping... mappings) {
        return new AbstractMap.SimpleEntry<>(key, List.of(mappings));
    }

}
