package com.squedgy.mc.mod.manager.forge.install;

import java.util.Optional;

public class ForgeInstall {
    public final String mirrorList, target, filePath, logo, welcome, version, path, profileName, minecraft;
    public final Optional<Boolean> stripMeta;

    public ForgeInstall(String mirrorList,
                        String target,
                        String filePath,
                        String logo,
                        String welcome,
                        String version,
                        String path,
                        String profileName,
                        String minecraft,
                        Optional<Boolean> stripMeta) {
        this.mirrorList = mirrorList;
        this.target = target;
        this.filePath = filePath;
        this.logo = logo;
        this.welcome = welcome;
        this.version = version;
        this.path = path;
        this.profileName = profileName;
        this.minecraft = minecraft;
        this.stripMeta = stripMeta;
    }
}
