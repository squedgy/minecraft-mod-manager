package com.squedgy.mc.mod.manager.forge.install;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squedgy.mc.mod.manager.version.library.LibraryDownloads;
import com.squedgy.mc.mod.manager.version.library.LibraryRule;
import com.squedgy.mc.mod.manager.version.library.OperatingSystem;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftLibrary;
import org.slf4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.slf4j.LoggerFactory.getLogger;

@JsonIgnoreProperties({"checksums", "clientreq", "serverreq", "comment"})
public class OldForgeLibrary extends MinecraftLibrary {
    private static final Logger LOG = getLogger(OldForgeLibrary.class);

    @JsonIgnore public final Optional<String> givenUrl;

    public OldForgeLibrary(String name,
                           Optional<String> url,
                           Optional<List<LibraryRule>> rules,
                           Optional<Map<OperatingSystem.OS, String>> natives,
                           Optional<Map<ExtractionAction, List<String>>> extract) {
        super(fixName(name), retrieveDownloads(fixName(name), url, natives), extract, natives, rules);
        this.givenUrl = url;
    }

    private static String fixName(String name) {
        return name.startsWith("net.minecraftforge:minecraftforge:") ?
               name.replace("net.minecraftforge:minecraftforge", "net.minecraftforge:forge") : name;
    }

    private static LibraryDownloads retrieveDownloads(String name,
                                                      Optional<String> url,
                                                      Optional<Map<OperatingSystem.OS, String>> natives) {
        LOG.trace("URL: {}", url);
        LOG.trace("NATIVES: {}", url);
        boolean hasUrl = url.isPresent(), hasNatives = natives.isPresent();
        if(hasUrl && hasNatives) return LibraryDownloads.of(name, url.get(), natives.get());
        else if(hasUrl) return LibraryDownloads.of(name, url.get());
        else if(hasNatives) return LibraryDownloads.of(name, natives.get());
        else return LibraryDownloads.of(name);
    }

}
