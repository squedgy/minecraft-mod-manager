package com.squedgy.mc.mod.manager.forge.install;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
public final class ArgumentTuple {
    private final String client, server;

    public ArgumentTuple(String client, String server) {
        this.client = client;
        this.server = server;
    }

    public String getClient() {
        return client;
    }

    public String getServer() {
        return server;
    }

    public String retrieveNeeded(boolean client) {
        return client ? this.client : server;
    }

}
