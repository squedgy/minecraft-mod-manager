package com.squedgy.mc.mod.manager.forge.install;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.squedgy.mc.mod.manager.version.json.Library;
import com.squedgy.mc.mod.manager.version.library.Artifact;
import com.squedgy.mc.mod.manager.version.library.LibraryDownloads;
import com.squedgy.mc.mod.manager.version.minecraft.MinecraftLibrary;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class NewForgeInstallProfile extends InstallProfile {
    @JsonCreator
    public NewForgeInstallProfile(Map<String, ArgumentTuple> data,
                                  int spec,
                                  String version,
                                  String json,
                                  String path,
                                  String minecraft,
                                  @JsonProperty(required = true) Collection<MinecraftLibrary> libraries,
                                  @JsonProperty(required = true) Collection<ForgeInstallProcessor> processors) {
        super(data, spec, version, json, path, minecraft, resolveLibraries(libraries), processors);
    }

    private static Collection<Library> resolveLibraries(Collection<MinecraftLibrary> libraries) {
        return libraries.stream()
                        .map(NewForgeInstallProfile::enforcePathFromName)
                        .collect(Collectors.toList());
    }

    private static Library enforcePathFromName(Library l) {
        Optional<Artifact> artifact = l.getDownloads()
                                       .getArtifact()
                                       .map(a -> enforcePathFromName(l.getName(), a));
        Optional<Map<String, Artifact>> classifiers = l.getDownloads()
                                                       .getClassifiers()
                                                       .map(map -> mapDownloads(l.getName(), map));

        return new Library(
            l.getName(),
            new LibraryDownloads(artifact, classifiers),
            l.getExtract(),
            l.getNatives(),
            l.getRules(),
            l.isForClient(),
            l.isForServer()
        );
    }

    private static Artifact enforcePathFromName(String name, Artifact l) {
        return new Artifact(Artifact.pathFromName(name), l.getSha1(), l.getUrl(), l.size, l.getMerge());
    }

    private static Map<String, Artifact> mapDownloads(String name, Map<String, Artifact> classifiers) {
        return classifiers.entrySet()
                          .stream()
                          .map(e -> new SimpleEntry<>(name + ':' + e.getKey(), enforcePathFromName(name, e.getValue())))
                          .collect(toMap(Entry::getKey, Entry::getValue));
    }
}
