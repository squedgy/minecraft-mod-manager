module com.squedgy.mc.mod.manager.forge {
    exports com.squedgy.mc.mod.manager.forge;
    exports com.squedgy.mc.mod.manager.forge.install;

    opens com.squedgy.mc.mod.manager.forge to com.fasterxml.jackson.databind;
    opens com.squedgy.mc.mod.manager.forge.install to com.fasterxml.jackson.databind;

    requires org.jsoup;
    requires com.squedgy.mc.mod.manager.configuration;
    requires transitive com.squedgy.mc.mod.manager.version;
    requires com.squedgy.mc.mod.manager.utils;
    requires com.squedgy.mc.mod.manager.http;
    requires org.slf4j;
    requires java.net.http;
    requires com.fasterxml.jackson.databind;
}