import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Run {
    private static void delete(File file) {
        if (file == null) return;
        if (file.isDirectory()) deleteDirectory(file);
        try {
            Files.delete(file.toPath());
        } catch (IOException e) {
            System.err.println("Failed to delete file " + file.getAbsolutePath());
        }
    }

    private static void deleteDirectory(File directory) {
        File[] files = directory.listFiles();
        if (files == null) return;
        for (File f : files) {
            delete(f);
        }
    }

    public static void main(String[] args) throws Exception {
        for (String arg : args) {
            System.out.println(arg);
        }
        String command = args[0];
        String name = System.getProperty("os.name");
        String[] commandSystem = name.toLowerCase()
                                     .contains("windows") ? new String[]{"cmd.exe", "/c", command} : new String[]{"sh", "-c", command};
        ProcessBuilder pb = new ProcessBuilder().command(commandSystem)
                                                .inheritIO();

        for (String arg : args) {
            if (arg.startsWith("--env=")) {
                String[] pieces = arg.split("=", 3);
                System.out.println(Arrays.toString(pieces));
                if (pieces.length > 2) {
                    pb.environment()
                      .put(pieces[1], pieces[2]);
                }
            }
        }
        Exception finalResult = null;
        try {
            Process process = pb.start();
            while (true) {
                try {
                    process.exitValue();
                    break;
                } catch (Exception e) {
                    TimeUnit.MILLISECONDS.sleep(50);
                }
            }
        } catch (InterruptedException | IOException e) {
            System.err.println("Error while running minecraft.");
            finalResult = e;
        }

        for (int i = 1; i < args.length; i++) {
            if (args[i].startsWith("--env=")) continue;
            Path delete = Paths.get(args[i]);
            if (delete.toFile()
                      .exists()) {
                delete(delete.toFile());
            }
        }

        if (finalResult != null) throw finalResult;

    }
}
