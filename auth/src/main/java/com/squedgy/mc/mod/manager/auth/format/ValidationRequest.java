package com.squedgy.mc.mod.manager.auth.format;

import static com.squedgy.mc.mod.manager.configuration.ClientToken.getClientToken;

public class ValidationRequest {
    public final String accessToken, clientToken;

    public ValidationRequest(String accessToken) {
        this.accessToken = accessToken;
        clientToken = getClientToken();
    }
}
