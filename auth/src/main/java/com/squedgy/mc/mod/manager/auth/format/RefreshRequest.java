package com.squedgy.mc.mod.manager.auth.format;

import static com.squedgy.mc.mod.manager.configuration.ClientToken.getClientToken;

public class RefreshRequest {
    public final String accessToken, clientToken;
    public final boolean requestUser = true;

    public RefreshRequest(String accessToken) {
        this.accessToken = accessToken;
        this.clientToken = getClientToken();
    }
}
