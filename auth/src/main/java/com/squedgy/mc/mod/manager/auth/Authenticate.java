package com.squedgy.mc.mod.manager.auth;

import com.squedgy.mc.mod.manager.auth.format.AuthenticationRequest;
import com.squedgy.mc.mod.manager.auth.format.AuthenticationResponse;
import com.squedgy.mc.mod.manager.auth.format.RefreshRequest;
import com.squedgy.mc.mod.manager.auth.format.ValidationRequest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.util.Objects;

import static com.squedgy.mc.mod.manager.configuration.ClientToken.getClientToken;
import static com.squedgy.mc.mod.manager.http.Client.sendRequest;
import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;

public abstract class Authenticate {

    /**
     * Attempts to authenticate the given username and password against Mojang's auth server
     *
     * @param username
     * @param password
     *
     * @return the authentication information
     *
     * @throws IOException
     * @throws URISyntaxException
     * @throws InterruptedException
     * @throws AuthenticationError
     */
    public static AuthenticationResponse authenticateUser(String username, String password) throws
                                                                                            IOException,
                                                                                            URISyntaxException,
                                                                                            InterruptedException,
                                                                                            AuthenticationError {
        var bytes = objectMapper().writeValueAsBytes(new AuthenticationRequest(username, password));
        var publisher = HttpRequest.BodyPublishers.ofByteArray(bytes);

        var request = createRequest("authenticate", publisher);
        return sendAuthRequest(request);
    }

    private static HttpRequest createRequest(String path, HttpRequest.BodyPublisher publisher) throws URISyntaxException {
        return HttpRequest.newBuilder(requestUri(path))
                          .POST(publisher)
                          .header("Content-Type", "application/json")
                          .build();
    }

    private static AuthenticationResponse sendAuthRequest(HttpRequest request) throws
                                                                               IOException,
                                                                               AuthenticationError,
                                                                               URISyntaxException,
                                                                               InterruptedException {
        var response = sendRequest(request);

        int status = response.statusCode();

        if(200 <= status && status <= 299) {
            var authResponse = objectMapper().readValue(response.body(), AuthenticationResponse.class);
            if(!Objects.equals(authResponse.clientToken, getClientToken())) {
                throw new IOException("Received an auth response with a different clientToken that expected.");
            }
            UserToken.setUserToken(authResponse.user.username, authResponse);
            return authResponse;
        }

        throw AuthenticationError.fromInputStream(response.body());
    }

    private static URI requestUri(String path) throws URISyntaxException {
        return new URI("https", "authserver.mojang.com", '/' + path, null);
    }

    /**
     * Attempt to refresh the given authentication response
     *
     * @param token
     *
     * @return a new authentication response with an updated token
     *
     * @throws IOException
     * @throws AuthenticationError
     * @throws URISyntaxException
     * @throws InterruptedException
     */
    public static AuthenticationResponse refresh(AuthenticationResponse token) throws
                                                                               IOException,
                                                                               AuthenticationError,
                                                                               URISyntaxException,
                                                                               InterruptedException {
        var bytes = objectMapper().writeValueAsBytes(new RefreshRequest(token.accessToken));
        var publisher = HttpRequest.BodyPublishers.ofByteArray(bytes);

        var request = createRequest("refresh", publisher);
        return sendAuthRequest(request);
    }

    /**
     * Checks to see if the given access token is valid
     *
     * @param accessToken
     *
     * @return whether the given access token is still valid
     *
     * @throws IOException
     * @throws URISyntaxException
     * @throws InterruptedException
     */
    public static boolean checkToken(String accessToken) throws IOException, URISyntaxException, InterruptedException {
        var bytes = objectMapper().writeValueAsBytes(new ValidationRequest(accessToken));
        var publisher = HttpRequest.BodyPublishers.ofByteArray(bytes);

        var request = createRequest("validate", publisher);

        var response = sendRequest(request);

        return response.statusCode() == 204;
    }
}
