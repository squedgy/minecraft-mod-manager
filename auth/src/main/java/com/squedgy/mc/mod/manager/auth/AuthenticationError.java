package com.squedgy.mc.mod.manager.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;
import static org.slf4j.LoggerFactory.getLogger;

public class AuthenticationError extends Exception {
    private static Logger LOG = getLogger(AuthenticationError.class);
    public final String error, cause;

    private AuthenticationError(JsonNode node) {
        this(
            node.get("error")
                .textValue(),
            node.get("errorMessage")
                .textValue(),
            node.get("cause") == null ? null : node.get("cause")
                                                   .textValue()
        );
    }

    @JsonCreator
    public AuthenticationError(String error, String errorMessage, String cause) {
        super(errorMessage);
        this.error = error;
        this.cause = cause;
    }

    public static AuthenticationError fromResponseString(String s) throws IOException {
        var mapper = objectMapper();
        JsonNode node = mapper.readTree(s);
        return new AuthenticationError((node));
    }

    public static AuthenticationError fromInputStream(InputStream is) throws IOException {
        var mapper = objectMapper();
        JsonNode node = mapper.readTree(is);
        return new AuthenticationError(node);
    }
}
