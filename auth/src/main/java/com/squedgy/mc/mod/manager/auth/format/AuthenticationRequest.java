package com.squedgy.mc.mod.manager.auth.format;

import com.fasterxml.jackson.annotation.JsonInclude;

import static com.squedgy.mc.mod.manager.configuration.ClientToken.getClientToken;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthenticationRequest {
    public final Agent agent = new Agent();
    public final String username, password, clientToken;
    public final boolean requestUser = true;

    public AuthenticationRequest(String username, String password) {
        this.username = username;
        this.password = password;
        this.clientToken = getClientToken();
    }

    private static class Agent {
        public final String name = "minecraft";
        public final byte version = 1;
    }
}
