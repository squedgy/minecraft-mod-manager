package com.squedgy.mc.mod.manager.auth.format;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Collection;
import java.util.List;

public class AuthenticationResponse {
    public final String accessToken;
    public final String clientToken;
    public final Collection<Profile> availableProfiles;
    public final Profile selectedProfile;
    public final User user;

    @JsonCreator
    public AuthenticationResponse(String accessToken,
                                  String clientToken,
                                  List<Profile> availableProfiles,
                                  Profile selectedProfile,
                                  User user) {
        this.accessToken = accessToken;
        this.clientToken = clientToken;
        this.availableProfiles = List.of(availableProfiles.toArray(new Profile[0]));
        this.selectedProfile = selectedProfile;
        this.user = user;
    }

    @Override
    public String toString() {
        return "AuthenticationResponse{" +
               "accessToken='" +
               accessToken +
               '\'' +
               ", clientToken='" +
               clientToken +
               '\'' +
               ", availableProfiles=" +
               availableProfiles +
               ", selectedProfile=" +
               selectedProfile +
               ", user=" +
               user +
               '}';
    }

    public static class Profile {
        public final String agent, id, name, userId;
        public final long createdAt;
        public final boolean legacyProfile, suspended, paid, migrated, legacy;

        @JsonCreator
        public Profile(String agent,
                       String id,
                       String name,
                       String userId,
                       long createdAt,
                       boolean legacyProfile,
                       boolean suspended,
                       boolean paid,
                       boolean migrated,
                       boolean legacy) {
            this.agent = agent;
            this.id = id;
            this.name = name;
            this.userId = userId;
            this.createdAt = createdAt;
            this.legacyProfile = legacyProfile;
            this.suspended = suspended;
            this.paid = paid;
            this.migrated = migrated;
            this.legacy = legacy;
        }

        @Override
        public String toString() {
            return "Profile{" +
                   "agent='" +
                   agent +
                   '\'' +
                   ", id='" +
                   id +
                   '\'' +
                   ", name='" +
                   name +
                   '\'' +
                   ", userId='" +
                   userId +
                   '\'' +
                   ", createdAt=" +
                   createdAt +
                   ", legacyProfile=" +
                   legacyProfile +
                   ", suspended=" +
                   suspended +
                   ", paid=" +
                   paid +
                   ", migrated=" +
                   migrated +
                   ", legacy=" +
                   legacy +
                   '}';
        }
    }

    public static class User {
        public final String id, email, username, registerIp, migratedFrom;
        public final Long migratedAt, registeredAt, passwordChangedAt, dateOfBirth;
        public final Boolean suspended, blocked, secured, migrated, emailVerified, legacyUser, verifiedByParent;
        public final Collection<Property> properties;

        @JsonCreator
        public User(String id,
                    String email,
                    String username,
                    String registerIp,
                    String migratedFrom,
                    Long migratedAt,
                    Long registeredAt,
                    Long passwordChangedAt,
                    Long dateOfBirth,
                    Boolean suspended,
                    Boolean blocked,
                    Boolean secured,
                    Boolean migrated,
                    Boolean emailVerified,
                    Boolean legacyUser,
                    Boolean verifiedByParent,
                    Collection<Property> properties) {
            this.id = id;
            this.email = email;
            this.username = username;
            this.registerIp = registerIp;
            this.migratedFrom = migratedFrom;
            this.migratedAt = migratedAt;
            this.registeredAt = registeredAt;
            this.passwordChangedAt = passwordChangedAt;
            this.dateOfBirth = dateOfBirth;
            this.suspended = suspended;
            this.blocked = blocked;
            this.secured = secured;
            this.migrated = migrated;
            this.emailVerified = emailVerified;
            this.legacyUser = legacyUser;
            this.verifiedByParent = verifiedByParent;
            this.properties = properties;
        }

        @Override
        public String toString() {
            return "User{" +
                   "id='" +
                   id +
                   '\'' +
                   ", email='" +
                   email +
                   '\'' +
                   ", username='" +
                   username +
                   '\'' +
                   ", registerIp='" +
                   registerIp +
                   '\'' +
                   ", migratedFrom='" +
                   migratedFrom +
                   '\'' +
                   ", migratedAt=" +
                   migratedAt +
                   ", registeredAt=" +
                   registeredAt +
                   ", passwordChangedAt=" +
                   passwordChangedAt +
                   ", dateOfBirth=" +
                   dateOfBirth +
                   ", suspended=" +
                   suspended +
                   ", blocked=" +
                   blocked +
                   ", secured=" +
                   secured +
                   ", migrated=" +
                   migrated +
                   ", emailVerified=" +
                   emailVerified +
                   ", legacyUser=" +
                   legacyUser +
                   ", verifiedByParent=" +
                   verifiedByParent +
                   ", properties=" +
                   properties +
                   '}';
        }
    }

    public static class Property {
        public final String name, value;

        public Property(String name, String value) {
            this.name = name;
            this.value = value;
        }

        @Override
        public String toString() {
            return "Property{" + "name='" + name + '\'' + ", value='" + value + '\'' + '}';
        }
    }
}
