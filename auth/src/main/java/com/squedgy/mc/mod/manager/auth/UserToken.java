package com.squedgy.mc.mod.manager.auth;

import com.squedgy.mc.mod.manager.auth.format.AuthenticationResponse;
import com.squedgy.mc.mod.manager.configuration.utils.ConfigPaths;

import java.io.IOException;
import java.nio.file.Path;

import static com.squedgy.mc.mod.manager.utils.io.IO.objectMapper;

public class UserToken {
    private static AuthenticationResponse token;

    public static void setUserToken(String username, AuthenticationResponse response) {
        var tokenFile = getTokenFile(username);

        if(!tokenFile.toFile()
                     .exists()) {
            try {
                tokenFile.getParent()
                         .toFile()
                         .mkdirs();
                objectMapper().writeValue(tokenFile.toFile(), response);
            } catch(IOException ignored) { }
        }

        UserToken.token = response;
    }

    private static Path getTokenFile(String username) {
        return ConfigPaths.getConfigPath()
                          .resolve(username + ".token.txt");
    }

    public static AuthenticationResponse getUserToken(String username) {
        if(token != null) return token;
        var tokenFile = getTokenFile(username);

        try {
            token = objectMapper().readValue(tokenFile.toFile(), AuthenticationResponse.class);
            return token;
        } catch(IOException e) { return null; }
    }
}
