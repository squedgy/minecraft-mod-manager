module com.squedgy.mc.mod.manager.auth {

    requires transitive java.net.http;
    requires org.slf4j;
    requires com.fasterxml.jackson.databind;
    requires com.squedgy.mc.mod.manager.configuration;
    requires com.squedgy.mc.mod.manager.utils;
    requires com.squedgy.mc.mod.manager.http;

    exports com.squedgy.mc.mod.manager.auth;
    exports com.squedgy.mc.mod.manager.auth.format;
    opens com.squedgy.mc.mod.manager.auth.format to com.fasterxml.jackson.databind;
}