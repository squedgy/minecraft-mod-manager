package com.squedgy.mc.mod.manager.auth;

import com.squedgy.utilities.formatter.ConfigInputStreamFormat;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.slf4j.LoggerFactory.getLogger;

public class AuthenticationTest {

    private static final Logger LOG = getLogger(AuthenticationTest.class);
    public static String user, pass;
    private static String token;

    @Test
    public void created_token_is_validated_by_mojangs_servers() throws
                                                                InterruptedException,
                                                                IOException,
                                                                AuthenticationError,
                                                                URISyntaxException {
        LOG.info("Token to validate: {}", token);
        if(token == null) sucessfully_logs_in_valid_user();
        assertTrue(Authenticate.checkToken(token));
    }

    @Test
    public void sucessfully_logs_in_valid_user() throws URISyntaxException, AuthenticationError, InterruptedException, IOException {
        var result = Authenticate.authenticateUser(user, pass);
        token = result.accessToken;
        LOG.info("TOKEN: {}", token);
    }

    @BeforeAll
    public static void beforeAll() {
        var file = AuthenticationTest.class.getResourceAsStream("user-pass.hidden.properties");
        Map<String, String> props = new ConfigInputStreamFormat().decode(file);

        user = props.get("username");
        pass = props.get("password");
    }

}
